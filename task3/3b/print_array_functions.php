<?php
define('MIN_VALUE', 5);
define('MAX_VALUE', 15);
define('MIN_LENGTH', 5);
define('MAX_LENGTH', 15);

function printArray($a){

    if(count($a) > 0){
        $output = "";

        foreach($a as $value){
            $output = $output.$value." ";
        }

        //return $output;
        echo $output."<br>";
    }
}

function printArrayJoin($a){

        echo implode(", ", $a)."<br>";

}

function returnArrayRecur($a){

    if(count($a) > 0){
        if (sizeof($a) == 1) {
            return $a[0];
        }
        return array_shift($a) . ", " . returnArrayRecur($a);
    }
}

function printArrayRecur($a){
    echo returnArrayRecur($a);
}
