<!doctype HTML>
<html>
<head>
</head>
<body>

<?php

define('MIN_VALUE', 0);
define('MAX_VALUE', 10);
define('MAX_LENGTH', 25);

$output = "";

for($i = 0; $i < MAX_LENGTH; $i++){
    $output .= mt_rand(MIN_VALUE, MAX_VALUE) . "\n";
}

if(file_put_contents('random_int.txt', $output)){
    echo "Operation was successful";
} else {
    echo "Something went wrong";
}

?>

</body>
</html>
