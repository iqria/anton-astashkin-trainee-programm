<?php

class AdminController extends Controller
{
    //+
    public function actionIndex($pageNumb = 1, $sortingField = 'noSorting', $sortingOrder = 'asc')
    {

        //The logic is: there will be a radiobutton within a page to select, what data will be shown: contacts or users

        if (isset($_POST['sort'])) {
            $sortingField = $_POST['sortField'];
            $sortingOrder = $_POST['sortingOrder'];
        }

        $contactsList = Admin::getDataList($pageNumb, $sortingField, $sortingOrder);

        if ($contactsList === false) {

            $this->redirectToPage(ERROR_404_PAGE_PATH);

        } else {

            $totalPages = ceil(Admin::getTableNumberOfRows() / RECORDS_PER_PAGE);

            $data = $this->createUsersTable($contactsList);

            $this->view->render('UsersDataTableAdmin.php', $data, ['totalPages' => $totalPages, 'sortingField' => $sortingField, 'sortingOrder' => $sortingOrder]);

            return true;
        }

    }
    //+
    public function actionAdd()
    {
        $submittedData = null;
        $additionalInfo = null;

        if (isset($_POST['submit'])) {

            $submittedData = [
                'login' => $_POST['login'],
                'email' => $_POST['email'],
                'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
            ];

            $additionalInfo = $this->validate($submittedData);

            if (empty($additionalInfo)) {

                Admin::addData($submittedData);

                //add redirect
                $this->redirectToPage('/admin');
            }
        }

        $additionalInfo['type'] = 'add';

        $this->view->render('FormAddEditAdmin.php', $submittedData, $additionalInfo);
        return true;
    }

    //+
    public function actionEdit($id)
    {
        if (preg_match('/[0-9]+/', $id)) {

            if (isset($_POST['submit'])) {

                $submittedData = [
                    'login' => $_POST['login'],
                    'email' => $_POST['email'],

                ];

                if ($_POST['password'] !== '') {
                    $submittedData['password'] = $_POST['password'];
                }

                $additionalInfo = $this->validate($submittedData);

                //need to preserve contact's id
                $submittedData['id'] = $id;

                if (empty($additionalInfo)) {

                    if (isset($submittedData['password'])){
                        $submittedData['password'] = password_hash($submittedData['password'], PASSWORD_DEFAULT);
                    }

                    Admin::editDataById($id, $submittedData);

                    //add redirect
                    $this->redirectToPage('/admin');

                }

            } else {
                $submittedData = Admin::getDataById($id);
            }

            $additionalInfo['type'] = 'edit';
            $this->view->render('FormAddEditAdmin.php', $submittedData, $additionalInfo);
            return true;

        } else {

            $this->redirectToPage(ERROR_404_PAGE_PATH);

        }

    }

	public function actionSearch($fieldName = 'login', $fieldValue = '', $pageNumb = 1, $sortingField = 'userId', $sortingOrder = 'asc')
	{

		if ($fieldValue != '') {

            if ($sortingField != 'userId') {
                $useSorting = 'true';
            } elseif (isset($_POST['useSorting'])){
                $useSorting = $_POST['useSorting'];
            } else {
                $useSorting = 'false';
            }

            //validation of searched column name and value
            $data = ['fieldName' => $fieldName, $fieldName => $fieldValue];
			$errors = $this->validate($data);

            if (empty($errors)) {
                //gets searched data and number of found rows
                $searchData = Admin::getUsersSearch($fieldName, $fieldValue, $pageNumb, $sortingField, $sortingOrder);

                $contactsList = $searchData['data'];
                $totalPages = ceil($searchData['rowsNumber'] / RECORDS_PER_PAGE);

                $data = $this->createUsersTable($contactsList);

                //got data to search and no error during validation
                $this->view->render('SearchAdmin.php', $data, [
                                                        'useSorting'=> $useSorting,
                                                        'searchField' => $fieldName,
                                                        'searchValue' => $fieldValue,
                                                        'totalPages' => $totalPages,
                                                        'sortingField' => $sortingField,
                                                        'sortingOrder' => $sortingOrder]);

                return true;

            } else {

                $this->view->render('SearchAdmin.php', null, [
                                                        'errors' => $errors[$fieldName],
                                                        'searchField' => $fieldName,
                                                        'searchValue' => $fieldValue,
                                                        'useSorting'=> $useSorting,
                                                        'sortingField' => $sortingField,
                                                        'sortingOrder' => $sortingOrder]);

                return true;
            }

        //empty fieldValue
		} else {

            //if it was send via POST -> actionSearch with params
			if (isset($_POST['searchFor'])) {

				if ($_POST['searchValue'] !== '') {
                    $searchValue = urlencode($_POST['searchValue']);
                    if ($_POST['useSorting'] == 'true') {
                        $this->redirectToPage("/admin/search/" . $_POST['searchField'] . "/" . $searchValue . "/" . $pageNumb . "/" . $_POST['sortingField'] . "/" . $_POST['sortingOrder']);
                    } else {
                        $this->redirectToPage("/admin/search/" . $_POST['searchField'] . "/" . $searchValue);
                    }

				}

                //no search value
				$this->view->render('SearchAdmin.php', null, [
                                                        'errors' => '<span class="error">This field cannot be empty!</span>',
                                                        'searchField' => $_POST['searchField'],
                                                        'searchValue' => '',
                                                        'useSorting' => $_POST['useSorting'],
                                                        'sortingField' => $_POST['sortingField'],
                                                        'sortingOrder' => $_POST['sortingOrder'],]);
			}

            //first visit
			$this->view->render('SearchAdmin.php', null, [
                                                    'searchField' => $fieldName,
                                                    'searchValue' => '',
                                                    'useSorting' => 'false',
                                                    'sortingField' => $sortingField,
                                                    'sortingOrder' => $sortingOrder]);

			return true;
		}


	}
    //+
    public function actionDelete($id, $deleting = false)
    {
        if (preg_match('/[0-9]+/', $id)) {

            if ($deleting) {

                Admin::deleteDataById($id);

                //add redirect
                $this->redirectToPage('/admin');

            }
            //get contacts data
            $contactsData = Admin::getDataById($id);

            if ($contactsData === false) {
                $this->redirectToPage(ERROR_404_PAGE_PATH);
            }

            $this->view->render('Delete.php', $contactsData);

            return true;

        } else {

            $this->redirectToPage(ERROR_404_PAGE_PATH);

        }

    }

    private function redirectToPage($page)
    {
        header("Location: $page");
        die();
    }

	//checks form input
	private function validate(&$data)
	{
		$errors = [];

		foreach ($data as $key=>$value) {

			$rule = $this->rules[$key];

			if ($value == null) {

				if ($rule['required']) {
					$errors[$key] = $rule['messages']['requiredError'];
				}

			} else {

				switch ($rule['type']) {

					case 'regex':

						if ($rule['parameters']['minLength'] !== null && $rule['parameters']['maxLength'] !== null) { //length is checked not with regex
							if (!(strlen($value) >= $rule['parameters']['minLength'] && strlen($value) <= $rule['parameters']['maxLength'])) {
								$errors[$key] = $rule['messages']['lengthError'];
							}
						}

						if (!preg_match($rule['parameters']['pattern'], $value)) {
							$errors[$key] = $rule['messages']['symbolsError'];
						}

						break;

					case 'regexBirthday':


						foreach ($rule['parameters']['delimitersToChange'] as $delimiter) {
							$value = str_replace($delimiter, '-', $value);
						}

						$data[$key] = $value;


						if (!preg_match($rule['parameters']['pattern'], $value)) {
							$errors[$key] = $rule['messages']['symbolsError'];
						}

						break;

					case 'filterVar':

						if (filter_var($value, $rule['parameters']['filter']) === false) {
							$errors[$key] = $rule['messages']['filterError'];
						}

						break;

					case 'imageCheck':

						if ($data['avatar']['name'] == '' || $data['avatar'] == '/img/avatars/default.jpeg' ) {

							$data['avatar'] = '/img/avatars/default.jpeg';

						} else {

							$targetFile = '/img/avatars/' . basename($data['avatar']['name']);

							$uploadOk = true;

							$size = getimagesize($data['avatar']['tmp_name']);

							if ($size === false) {

								$errors[$key] = $rule['messages']['typeError'];
								$uploadOk = false;

							} else {

								$imageFileType = pathinfo($targetFile, PATHINFO_EXTENSION);

								//check for all available types
								foreach ($rule['parameters']['types'] as $type) {
									if ($imageFileType === $type) {
										$typeIsOk = true;
										break;
									}
								}

								//if there wasnt a type match - there is an error
								if ($typeIsOk !== true) {
									$errors[$key] = $rule['messages']['typeError'];
									$uploadOk = false;
								}

							}


							//2mb limit
							if ($data['avatar']['size'] > 2 * pow(2, 20) && $uploadOk) {
								$errors[$key] = $rule['messages']['sizeError'];
								$uploadOk = false;
							}


							if ($uploadOk) {
								if (move_uploaded_file($data['avatar']['tmp_name'], ROOT . $targetFile)) {
									$data['avatar'] = $targetFile;
								} else {
									$errors[$key] = $rule['messages']['uploadError'];
								}
							}

						}

						break;
				}
			}
		}

		return $errors;
	}


    private function createUsersTable($data)
    {
        $table = [];

        $table[] = " <table class='usersTable'>
                        <tr>
                                <td>User Id</td>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Options</td>
                        </tr>";

        if ($data[0]['id'] == '-') {

            $table[]   = '<tr>
                			<td>' . $data[0]['id'] .'</td>
                			<td>' . $data[0]['login'] . '</td>
                			<td>' . $data[0]['email'] . '</td>
                			<td></td>
                		</tr>';

            $table[] = "</table>";

            return $table;
        }

        foreach ($data as $user) {

            //img src must be reviewed
            $table[]   = '<tr>
                			<td>' . $user['id'] .'</td>
                			<td>' . $user['login'] . '</td>
                			<td>' . $user['email'] . '</td>
                			<td><a href="/admin/edit/' . $user['id'] . '">edit</a> /
                			    <a href="/admin/delete/' . $user['id'] . '">delete</a></td>
                		</tr>';

        }

        $table[] = "</table>";

        return $table;
    }

}