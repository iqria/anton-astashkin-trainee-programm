<?php

function changeDataInFile($filePath, $dataToEdit, $replaceWith = '') {

    if ($replaceWith != '') {
        $replaceWith = implode('|', $replaceWith) . "\n";
    }

    $initFileString = file_get_contents($filePath);
    $initFileString = str_replace("\"", '', $initFileString);
    $initFileString = str_replace(implode('|', $dataToEdit) . "\n", $replaceWith, $initFileString);
    file_put_contents($filePath, $initFileString);
}