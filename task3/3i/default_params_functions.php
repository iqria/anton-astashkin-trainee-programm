<?php


function applyConfig($conf, $defaultConf)
{

    $array = array_merge($defaultConf, array_intersect_key($conf, $defaultConf));

    return $array;
}