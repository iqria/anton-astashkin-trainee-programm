<?php

namespace app\controllers;

use app\models\Status;
use Yii;
use yii\data\Pagination;
use yii\rest\ActiveController;
use yii\web\Controller;
use app\models\User;
use app\models\ListingOptions;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';

}
