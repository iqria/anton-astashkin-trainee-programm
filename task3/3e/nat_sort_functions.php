<?php
define('ASCENDING', 'ascending');
define('DESCENDING', 'descending');
define('MIN_VALUE', 5);
define('MAX_VALUE', 15);
define('MIN_LENGTH', 5);
define('MAX_LENGTH', 15);

function nativeSort($array, $order = ASCENDING)
{
    if($order == ASCENDING){
        asort($array);
    } else {
        arsort($array);
    }

    return $array;
}
