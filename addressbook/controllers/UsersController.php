<?php

class UsersController extends Controller
{
/*    public function actionIndex()
    {

    }
*/
    public function actionLogin()
    {
        //if there was a login attempt
        if (isset($_POST['submit'])) {

            //check what was used for authorisation: nickname or email
            if (preg_match('/@/',$_POST['login'] )) {

                $data['email'] = $_POST['login'];

            } else {

                $data['login'] = $_POST['login'];

            }

            $data['password'] = $_POST['password'];

            //validate entered data
            $errors = $this->validate($data);

            //if data CAN be valid - check it in db
            if (empty($errors)) {

                //returns
                $id = Users::checkCredentials($data);

                if ($id !== null){

                    //do stuff from now and LEAVE NOTHING BEHIND
                    $_SESSION['id'] = $id;
                    $_SESSION['loggedIn'] = true;

                    if ($id == 1) {

                        //add also namecheck 'administratorOfTHisGloriousDb'
                        $_SESSION['admin'] = true;

                    }

                    $this->redirectToPage('/');

                } else {
                    $errors['failedLogin'] = '<span>Invalid login or password!</span>';
                    $this->view->render('Login.php', $data, $errors);
                }

            } else {

                $this->view->render('Login.php', $data, $errors);
                return true;

            }
        }

        //first visit
        $this->view->render('Login.php',['login' => '']);
        return true;

    }

    public function actionRegister()
    {
        //if there was an attempt to register
        if (isset($_POST['submit'])) {

            //"nickname" is used to prevent confusion with "login"
            $data['login'] = $_POST['login'];
            $data['email'] = $_POST['email'];
            $data['password'] = $_POST['password'];


            if ($data['password'] !== $_POST['passwordConfirm']) {
                $passwordConfirmError = '</span class="error">Passwords do not match!</span>';
            }

            if ($_POST['passwordConfirm'] === '') {
                $passwordConfirmError = '</span class="error">You need to confirm Your password!</span>';
            }

            unset($data['passwordConfirm']);

            //validate entered data
            $errors = $this->validate($data);

            if (Users::checkForUniqueness($data['login']) !== null) {
                $errors['login'] = '</span class="error">This login is already used!</span>';
            }

            if (Users::checkForUniqueness($data['email']) !== null) {
                $errors['email'] = '</span class="error">This email is already used!</span>';
            }

            if (isset($passwordConfirmError)){
                $errors['passwordConfirm'] = $passwordConfirmError;
            }

            //if data is OK
            if (empty($errors)) {
                //add new user to db
                Users::registerNewUser($data);
                $this->redirectToPage('/');

            } else {

                $this->view->render('Register.php', $data, $errors);
                return true;

            }
        }

        //first visit
        $this->view->render('Register.php');
        return true;

    }

    public function actionLogout()
    {
        $_SESSION = [];
        unset($_COOKIE[session_name()]);
        session_destroy();
        $this->redirectToPage('/');
    }

    //checks form input
    private function validate($data)
    {
        $errors = [];

        foreach ($data as $key=>$value) {

            $rule = $this->rules[$key];

            if ($value == null) {

                if ($rule['required']) {
                    $errors[$key] = $rule['messages']['requiredError'];
                }

            } else {

                switch ($rule['type']) {

                    case 'regex':

                        if ($rule['parameters']['minLength'] !== null && $rule['parameters']['maxLength'] !== null) { //length is checked not with regex
                            if (!(strlen($value) >= $rule['parameters']['minLength'] && strlen($value) <= $rule['parameters']['maxLength'])) {
                                $errors[$key] = $rule['messages']['lengthError'];
                            }
                        }

                        if (!preg_match($rule['parameters']['pattern'], $value)) {
                            $errors[$key] = $rule['messages']['symbolsError'];
                        }

                        break;

                    case 'regexPassword':
                        //check length
                        if (!(strlen($value) >= $rule['parameters']['minLength'] && strlen($value) <= $rule['parameters']['maxLength'])) {
                            $errors[$key] = $rule['messages']['lengthError'];
                        }

                        foreach ($rule['parameters']['pattern'] as $patternName => $pattern) {
                            if (!preg_match($pattern, $value)) {
                                $errors[$key] = $rule['messages'][$patternName];
                                break;
                            }

                        }

                        break;

                    //for emails
                    case 'filterVar':

                        if (filter_var($value, $rule['parameters']['filter']) === false) {
                            $errors[$key] = $rule['messages']['filterError'];
                        }

                        break;
                }
            }
        }

        return $errors;
    }

    private function redirectToPage($page)
    {
        header("Location: $page");
        die();
    }

}