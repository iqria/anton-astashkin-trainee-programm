<?php

class Router
{

	private $routes;

	public function __construct()
	{

		$routesPath = ROOT . '/config/routes.php';

		$this->routes = include_once($routesPath);

	}


	
	public function run()
	{

		$uri = urldecode($this->getURI());

        //check for such QS in routes
        foreach ($this->routes as $uriPattern => $path) {

            //~delimiters used to prevent conflicts while working with directories and file names
            if (preg_match("~$uriPattern~", $uri)) {

                //if uri has additional parameters for functions - extract them
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                //decide which controller and action to use
                $segments = explode('/', $internalRoute);

                //get controller and action names
                $controllerName = ucfirst(array_shift($segments) . 'Controller');

                if ($controllerName == 'AdminController'  && !isset($_SESSION['admin'])) {
                    self::ErrorPage404();
                }

                //only UsersController's actions are allowed if one is not authenticated
                if (!(isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) && $controllerName != 'UsersController') {
                    $this->redirectToPage(LOGIN_PAGE_PATH);
                }

                $actionName = 'action' . ucfirst(array_shift($segments));

                if ($controllerName == 'UsersController' && $actionName != 'actionLogout' && isset($_SESSION['id'])) {
                    self::ErrorPage404();
                }

                //include controller file
                $controllerFilePath = ROOT . '/controllers/' . $controllerName . '.php';


                if (file_exists($controllerFilePath)) {

                    include_once($controllerFilePath);

                } else {

                    self::ErrorPage404();
                    break;

                }

                //create an object, call action

                $controllerObject = new $controllerName;

                $result = call_user_func_array([$controllerObject, $actionName], $segments);

                if ($result != null) {
                    break;
                }

            }

        }

	}

    private function redirectToPage($page = '/')
    {
        header('location:' . $page);
        die();
    }

	//returns uri string
	private function getURI()
	{
		if (!empty($_SERVER['REQUEST_URI'])) {
			return trim($_SERVER['REQUEST_URI'], '/');
		} else {
            return false;
        }
	}

    //try to make as a part of redirectToPage
	private static function ErrorPage404(){
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'error404');
	}	
}