<?php
function concat()
{
    $output = "";	
    $args = func_get_args();

    foreach ($args as $value) {
        $output = $output . $value;
    }

    return trim($output);
}

function concatFirstUpper()
{
    $output = "";	
    $args = func_get_args();

    foreach ($args as $value) {
        $output = $output . $value;
    }

    return ucfirst(strtolower(trim($output)));
}


function concatCapitals()
{
    $output = "";	
    $args = func_get_args();

    foreach ($args as $value) {
        $output = $output . $value;
    }

    return strtoupper(trim($output));
}

function concatWords()
{

    $argsConcat = "";
    $output = "";	
    $args = func_get_args();

    foreach ($args as $value) {
        $argsConcat = $argsConcat . $value;
    }

    $token = strtok($argsConcat, " ");
    
    while ($token != false) {
        $output = $output.ucfirst($token). " ";
        $token = strtok(" ");
    }

    return trim($output);

}

function concatUCWords()
{

    $output = "";	
    $args = func_get_args();

    foreach ($args as $value) {
        $output = $output . $value;
    }

    return ucwords(trim($output));

}
