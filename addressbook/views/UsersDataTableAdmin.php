<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>

    <title>Address book</title>

    <meta charset="UTF-8">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">

</head>

<body>

<div class="container">

    <div class="header">

        <div class="login">

            <a href="/users/logout">Log out</a>

        </div>

        <h1 class="logo">Address Book</h1>

        <div class="menu">

            <ul>
                <li><a href="/">Homepage</a></li>
                <li><a href="/contacts/add">Add</a></li>
                <li><a href="/contacts/search">Search</a></li>
                <li class="current_page_item"><a href="/admin">Admin (users table)</a></li>
                <li><a href="/admin/add">Add user</a></li>
                <li><a href="/admin/search">Search user</a></li>

            </ul>

        </div>


    </div>

    <div class="main">

        <div class="options">

            <form method="post" action="/admin">

                <input type="submit" name="sort" value="Sort by:">

                <select name="sortField">

                    <option value="noSorting" <?php if ($additionalInfo['sortingField'] == 'noSorting') {echo 'selected';}?>>Do not sort</option>
                    <option value="login" <?php if ($additionalInfo['sortingField'] == 'login') {echo 'selected';}?>>Login</option>
                    <option value="email" <?php if ($additionalInfo['sortingField'] == 'email') {echo 'selected';}?>>Email</option>

                </select>

                <br>

                <input type="hidden" name="sortingOrder" value="asc">
                <label for="sortingOrder">Use descending order:</label>
                <input type="checkbox" name="sortingOrder" id="sortingOrder" value="desc" <?php if ($additionalInfo['sortingOrder'] == 'desc') echo 'checked';?>>


            </form>

        </div>

        <div class="data">

            <?php
            foreach ($data as $row) {
                echo $row;
            }
            ?>

        </div>

        <div class="pages">

            <?php

            if ($additionalInfo['sortingField'] != 'noSorting') {
                $sort = "/" . $additionalInfo['sortingField'] . "/" . $additionalInfo['sortingOrder'];
            } else {
                $sort = '';
            }

            for ($pageNum = 1, $maxPages = $additionalInfo['totalPages']; $pageNum <= $maxPages; $pageNum++) {
                echo "<a href='/admin/" . $pageNum . $sort . "'>[" . $pageNum . "]</a>";
            }

            ?>

        </div>

    </div>

</div>


</body>
</html>

