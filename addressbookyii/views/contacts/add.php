<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<?= $form->field($contact, 'firstName')->label('First Name'); ?>
<?= $form->field($contact, 'lastName')->label('Last Name'); ?>
<?= $form->field($contact, 'email'); ?>
<?= $form->field($contact, 'mobilePhone')->label('Mobile phone'); ?>
<?= $form->field($contact, 'homePhone')->label('Home phone'); ?>
<?= $form->field($contact, 'workPhone')->label('Work phone'); ?>
<?= $form->field($contact, 'address'); ?>
<?= $form->field($contact, 'city'); ?>
<?= $form->field($contact, 'state'); ?>
<?= $form->field($contact, 'zip'); ?>
<?= $form->field($contact, 'birthday'); ?>
<?= $form->field($contact, 'imageFile')->fileInput(); ?>

<div class="form-group">
    <?= Html::submitButton(ucfirst($method) . ' contact', ['class' => 'btn btn-primary']) ?>

    <?= Html::button('Cancel', ['class' => 'btn btn-primary', 'onclick' => "window.location.href='/contacts/'"]) ?>
</div>

<?php ActiveForm::end(); ?>

