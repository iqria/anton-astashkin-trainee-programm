<!doctype HTML>

<html>
<head>
</head>
<body>
<?php

require_once('print_array_functions.php');

$array = [];
for($i = 0; $i < mt_rand(MIN_LENGTH, MAX_LENGTH); $i++){
    $array[] = mt_rand(MIN_VALUE, MAX_VALUE);
}

printArray($array);
printArrayJoin($array);
printArrayRecur($array);

?>
</body>
</html>
