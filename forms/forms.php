<!doctype HTML>

<html>
<head>
    <style>
        .message {
            color: red;
        }
    </style>
</head>
<body>

<?php

if($_POST['submit']){

    $badData = [];

    if($_POST['login']){

        $login = $_POST['login'];

        if(strlen($login) > 3 && strlen($login) < 25){
            $loginPattern = "/^[a-zA-Z0-9'_]+$/";

            if(!preg_match($loginPattern, $login)){
                $badData['login'] = $login;
                $loginError = 'Please, use only letters (a-z, A-Z), digits (0-9) and apostrophe or underscore!';
            }

        } else {
            $loginError = 'Invalid login length! Use 4 to 24 characters.';
            $badData['login'] = $login;
        }

    } else {
        $loginError = 'Empty login!';
        $badData['login'] = $_POST['login'];
    }

    if($_POST['password']){

        $password = $_POST['password'];
        if(strlen($password) > 5 && strlen($password) < 33){
            $passwordPattern = "/^[a-zA-Z0-9_:'*!£$@.,!?+-]+$/";

            if(!preg_match($passwordPattern, $password)){
                $badData['password'] = $password;
                $passwordError = 'Please, use only letters (a-z, A-Z), digits (0-9) or this special symbols (_ : \\ \' * ! £ $ @ . , ! ? + -)!';
            }

        } else {
            $passwordError = 'Invalid password length! Use 6 to 32 characters.';
            $badData['password'] = $password;
        }

    } else {
        $passwordError = 'Empty password!';
        $badData['password'] = $_POST['password'];
    }


    if($_POST['hidden']){

        $hidden = $_POST['hidden'];
        $hiddenPattern = "/^[a-zA-Z0-9_:'*!£$@.,!?+-]+$/";

        if(!preg_match($hiddenPattern, $hidden)){
            $badData['hidden'] = $hidden;
        }
    }


    if($_POST['textarea']){

        $textArea = $_POST['textarea'];
        $textAreaPattern = "/^[a-zA-Z0-9 _:'*!£$@.,!?+-]+$/s";

        if(!preg_match($textAreaPattern, $textArea)){
            $badData['textarea'] = $textArea;
            $textAreaError = 'Invalid data!';
        }
    }


    if($_POST['radio'] == 'a' || $_POST['radio'] == 'b'){
        $radio = $_POST['radio'];
    } else {
        $badData['radio'] = true;
    }


    if($_POST['check1'] == "true" || $_POST['check1'] == "false"){
        $check1 = $_POST['check1'];
    } else {
        $badData['check1'] = true;
    }


    if($_POST['check2'] == "true" || $_POST['check2'] == "false"){
        $check2 = $_POST['check2'];
    } else {
        $badData['check2'] = true;
    }


    if($_POST['select']){
        $select = $_POST['select'];
        $selectPattern = "/^[1-4]$/";

        if(!preg_match($selectPattern, $select)){
            $badData['select'] = $select;
            $selectError = 'Invalid data!';
        } else {
            $select = $_POST['select'];
        }
    }


    if($_POST['selectList']){
        $selectPattern = "/^[1-4]$/";
        $selectList = $_POST['selectList'];

        foreach($_POST['selectList'] as $value){
            if(!preg_match($selectPattern, $value)){
                $badData['selectList'] = true;
                break;
            }
        }
    }


    if(count($badData) != 0){
        $errors = true;

        echo "<br>Bad data:<br>";

        foreach($badData as $k=>$v){
            echo $k . " " . $v ."<br>";
        }

        echo "<br>";
    } else {
        $errors = false;
    }
}

?>




<form action="forms.php" method="post">


    <label>Login:</label>
    <input type="text" name="login" value="<?php if($errors) echo $login;?>">

    <span class="message"><?=$loginError?></span>

    <br>

    <label>Password:</label>
    <input type="password" name="password">

    <span class="message"><?=$passwordError?></span>

    <br>

    <input type="hidden" name="hidden" value="secret">
    hidden field here

    <br>
    <br>

    <textarea name="textarea"><?php if($errors){echo (($textArea) ?  $textArea :  'Enter text here...');} else { echo 'Enter text here...';};?></textarea>
    <span class="message" ><?=$textAreaError?></span>

    <br>
    <br>

    <?php //A is checked by default
    if(!$errors || ($radio != 'a' && $radio != 'b') || $radio == 'a'){
        $achecked = "checked";
    } else {
        $bchecked = "checked";
    }
    ?>

    <input type="radio" name="radio" value="a" <?=$achecked?>> A
    <input type="radio" name="radio" value="b" <?=$bchecked?>> B


    <br>
    <br>

    <input type="hidden" name="check1" value="false">
    <input type="checkbox" name="check1" value="true" <?php if($check1 == "true" && $errors){echo "checked";}?>> 1
    <input type="hidden" name="check2" value="false">
    <input type="checkbox" name="check2" value="true" <?php if($check2 == "true" && $errors){echo "checked";}?>> 2


    <br>
    <br>

    <select name="select">
        <?php
        if($errors){
            switch ($select){
                case 1:
                    $select1 = "selected";
                    break;
                case 2:
                    $select2 = "selected";
                    break;
                case 3:
                    $select3 = "selected";
                    break;
                default:
                    $select4 = "selected";
                    break;
            }
        }
        ?>
        <option value="1" <?=$select1?>>1</option>
        <option value="2" <?=$select2?>>2</option>
        <option value="3" <?=$select3?>>3</option>
        <option value="4" <?=$select4?>>4</option>
    </select>


    <br>
    <br>

    <select name="selectList[]" multiple>

        <?php
        if($errors){
            $selectedOptions = [];

            for($i = 0; $i < count($selectList); $i++) {
                $selectedOptions[intval($selectList[$i])] = "selected";
            }
        }
        ?>

        <option value="1" <?=$selectedOptions[1]?>>1</option>
        <option value="2" <?=$selectedOptions[2]?>>2</option>
        <option value="3" <?=$selectedOptions[3]?>>3</option>
        <option value="4" <?=$selectedOptions[4]?>>4</option>
    </select>

    <br>
    <br>

    <input type="submit" name="submit" value="Done!"><br>

</form>
<?php
if(count($badData) == 0){
    echo "<br>Submitted!<br>";
}

?>












<br>
<br>
Retrieved data:
<br>
<br>

<form>

    <input type="text" value="<?=$login?>">
    login

    <br>

    <input type="text" value="<?=$password?>">
    password

    <br>

    <input type="text" value="<?=$hidden?>">
    hidden field here

    <br>
    <br>

    <textarea name="textarea"><?=$textArea?></textarea>

    <br>
    <br>

    <?php //A is checked by default
    if(($radio != 'a' && $radio != 'b') || $radio == 'a'){

        $achecked = "checked";

    } else {
        $bchecked = "checked";
    }
    ?>

    <input type="radio" name="radio" value="a" <?=$achecked?>> A
    <input type="radio" name="radio" value="b" <?=$bchecked?>> B

    <br>
    <br>

    <input type="hidden" name="check1" value="false">
    <input type="checkbox" name="check1" value="true" <?php if($check1 == "true"){echo "checked";}?>> 1
    <input type="hidden" name="check2" value="false">
    <input type="checkbox" name="check2" value="true" <?php if($check2 == "true"){echo "checked";}?>> 2

    <br>
    <br>

    <select name="select">
        <?php
        if(!$errors){
            switch ($select){
                case 1:
                    $select1 = "selected";
                    break;
                case 2:
                    $select2 = "selected";
                    break;
                case 3:
                    $select3 = "selected";
                    break;
                default:
                    $select4 = "selected";
                    break;
            }
        }
        ?>
        <option value="1" <?=$select1?>>1</option>
        <option value="2" <?=$select2?>>2</option>
        <option value="3" <?=$select3?>>3</option>
        <option value="4" <?=$select4?>>4</option>
    </select>


    <br>
    <br>

    <select name="selectList[]" multiple>

        <?php
        if(!$errors){
            $selectedOptions = [];

            for($i = 0; $i < count($selectList); $i++) {
                $selectedOptions[intval($selectList[$i])] = "selected";
            }
        }
        ?>

        <option value="1" <?=$selectedOptions[1]?>>1</option>
        <option value="2" <?=$selectedOptions[2]?>>2</option>
        <option value="3" <?=$selectedOptions[3]?>>3</option>
        <option value="4" <?=$selectedOptions[4]?>>4</option>
    </select>

</form>


</body>
</html>
