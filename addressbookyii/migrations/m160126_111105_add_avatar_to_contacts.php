<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_111105_add_avatar_to_contacts extends Migration
{
    public function up()
    {
        $this->addColumn('contacts', 'avatar', $this->string()->defaultValue('/img/avatar/default.png'));
    }

    public function down()
    {
        $this->dropColumn('contacts', 'avatar');
    }
}
