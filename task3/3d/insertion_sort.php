<!doctype HTML>
<html>
<head>
</head>
<body>

<?php
require_once('insertion_sort_functions.php');

$array = [];
for($i = 0; $i < mt_rand(MIN_LENGTH, MAX_LENGTH); $i++){
    $array[] = mt_rand(MIN_VALUE, MAX_VALUE);
}

echo "Array elements: ".implode(', ', $array)."</br>Sorted array (ascending): ".implode(", ",insertion($array, ASCENDING))."<br><br>";

echo "Array elements: ".implode(', ', $array)."</br>Sorted array (descending):".implode(", ",insertion($array, DESCENDING))."<br><br>";

?>

</body>
</html>
