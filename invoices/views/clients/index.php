<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */


use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'My Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a('Add Client', ['/clients/add-client'])?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'format' => 'text',
                'label' => 'Name',
                'value' => function ($data) {
                    return $data->firstName . " " . $data->lastName;
                },
            ],
            'phone',
            'email',
            'organization',
            [
                'format' => 'text',
                'label' => 'Last Invoice',
                'value' => function ($data) {
                    if ($data->lastInvoice == '0000-00-00') {
                        return 'No invoices yet.';
                    } else {
                        return $data->lastInvoice;
                    }
                },
            ],
            [
                'format' => 'text',
                'label' => 'Total Spent Count',
                'value' => function ($data) {
                    return "$ " . $data->totalSpentCount;
                },
            ],
            [
                'format' => 'raw',
                //'label' => 'edit/delete options',
                'value' => function ($data) {
                    $links = Html::a(Html::img('/img/system/edit.png'), "/clients/edit/$data->clientId") . ' ';
                    /*
                     * button changes depending on current status
                    if (//checkStatus) {*/
                        $links .= Html::a(Html::img('/img/system/delete.png'), "/clients/delete/$data->clientId");
                    /*} else {
                        $links .= Html::a(Html::img('/img/system/inactive.png'), "/admin/change-status/$data->userId");
                    }
                    */
                    return $links;
                }
            ],
        ],
    ]) ?>

</div>
