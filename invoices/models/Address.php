<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "address".
 *
 * @property string $addressId
 * @property string $addressLine1
 * @property string $addressLine2
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $zip
 */
class Address extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['addressLine1', 'addressLine2', 'country', 'city', 'zip'], 'required'],
            [['addressLine1', 'addressLine2', 'country', 'state', 'city', 'zip'], 'match', 'pattern' => '/^[a-zA-Z0-9 \.\,\#\-\'\"\(\)\:]{2,100}$/']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'addressId' => 'Address ID',
            'addressLine1' => 'Address Line 1',
            'addressLine2' => 'Address Line 2',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'zip' => 'Zip',
        ];
    }

    public static function checkAddressExists($addressModel)
    {
        //if exact same address already exists -> give its id
        if ($address = Address::findOne([
            'addressLine1' => $addressModel->addressLine1,
            'addressLine2' => $addressModel->addressLine2,
            'country' => $addressModel->country,
            'city' => $addressModel->city,
            'state' => $addressModel->state,
            'zip' => $addressModel->zip
        ])) {
            return $address->addressId;
        }
    }
}
