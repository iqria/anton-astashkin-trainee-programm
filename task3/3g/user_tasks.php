<!doctype HTML>
<html>
<head>
    <style>
    table {
        border-collapse: collapse;

    }

    table, th, td {
        border: 1px solid black;
        padding: 5px;
    }
    </style>
</head>
<body>

<?php
require_once('user_tasks_functions.php');

if ($_POST['submit']) {

    if ($_POST['maxTime']) {
        $maxTime = intval($_POST['maxTime']);
    } else {
        $maxTime = 1;
    }

    if ($_POST['numTasks']) {
        $numTasks = $_POST['numTasks'];
    } else {
        $numTasks = 0;
    }

    $tasks = generateTasks($numTasks, $maxTime);
}

?>
    <form action="<?=$_SERVER['SCRIPT_NAME']?>"  method="post">
        Number of tasks:
        <input type="text" name="numTasks" value="<?=$numTasks?>">
        <br>
        Maximum time for 1 task:
        <input type="text" name="maxTime" value="<?=$maxTime?>">
        <br>
        <input type="submit" name="submit">
        <br>
        <br>
    </form>

<?php
sortTasks($tasks);
execute($tasks);
?>

</body>
</html>
