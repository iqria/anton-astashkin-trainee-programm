<!doctype HTML>
<html>
<head>
</head>
<body>

<?php
require_once('asoc_array_replacments_functions.php');

$input = 'Hello, world! I am a PHP developer!';

$substitutions = [
    'Hello' => 'Good-bye',
    'world' => 'java',
    'PHP' => 'Javascript'
];

echo replaceAll($substitutions, $input);

?>


</body>
</html>
