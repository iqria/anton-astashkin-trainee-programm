<?php

namespace app\controllers;

use app\models\Status;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use app\models\User;
use app\models\ListingOptions;
use yii\web\NotFoundHttpException;
use yii\helpers\VarDumper;

class AdminController extends Controller
{



    public function actionIndex()
    {
        if (Yii::$app->user->can('changeUsers')) {

            $searchData = new ListingOptions();

            $searchData->load(Yii::$app->request->post());

            $query = User::find();

            //if searching for some values
            if ($searchData->searchValue != '') {
                switch ($searchData->searchFieldUsers) {
                    //allows to search by status field using both names and ids of statuses
                    case 'statusId' :

                        if (strtolower($searchData->searchValue) == 'active') {
                            $modifiedValue = Status::STATUS_ACTIVE;
                        } elseif (strtolower($searchData->searchValue) == 'inactive') {
                            $modifiedValue = Status::STATUS_INACTIVE;
                        } elseif (strtolower($searchData->searchValue) == 'pending') {
                            $modifiedValue = Status::STATUS_PENDING;
                        }

                        if (isset($modifiedValue)) {
                            $query->where([$searchData->searchFieldUsers => $modifiedValue]);

                        //remove else to prevent searching by statusId
                        } else {
                            $query->where([$searchData->searchFieldUsers => $searchData->searchValue]);
                        }

                        break;

                    //case 'role' :

                        //$query->joinWith('auth_assignment', '`auth_assignment`.`user_id` = `user`.`userId`')->where(['user_id' => 'userId']);

                      //  break;

                    default :
                        $query->where([$searchData->searchFieldUsers => $searchData->searchValue]);
                        break;

                }

            }

            //if ordering is set
            if ($searchData->sortOrder == 'ascending') {

                $query->orderBy([$searchData->sortFieldUsers => SORT_ASC]);

            } elseif ($searchData->sortOrder == 'descending') {

                $query->orderBy([$searchData->sortFieldUsers => SORT_DESC]);

            }

            $pagination = new Pagination([
                'defaultPageSize' => 10,
                'totalCount' => $query->count(),
            ]);

            $users = $query->offset($pagination->offset)->limit($pagination->limit)->all();


            return $this->render('index', ['users' => $users, 'pagination' => $pagination, 'searchData' => $searchData]);

        } else {

            throw new NotFoundHttpException('Page not found.');

        }

    }

    public function actionAddUser()
    {
        if (Yii::$app->user->can('changeUsers')) {

            $user = new User();

            if ($user->load(Yii::$app->request->post())  && $user->validate()) {

                $user->save();

                $userRoleName = Yii::$app->authManager->getRole('user');
                Yii::$app->authManager->assign($userRoleName, $user->userId);

                $this->redirect('/admin/');
            }

            return $this->render('add', ['user' => $user, 'method' => 'add'] );

        } else {

            throw new NotFoundHttpException('Page not found.');

        }

    }

    public function actionEditUser($userId = -1)
    {
        if (Yii::$app->user->can('changeUsers')) {

            if (($user = $this->getUserById($userId)) !== null) {
                //prevents from showing existing hash from db
                unset($user->password);

                if ($user->load(Yii::$app->request->post()) && $user->validate()) {
                    $user->save();
                    $this->redirect('/admin/');
                }

                return $this->render('add', ['user' => $user, 'method' => 'edit'] );

            } else {

                throw new NotFoundHttpException('Not found this user!');

            }

        } else {

            throw new NotFoundHttpException('Page not found.');

        }
    }


    public function actionDeleteUser($userId = -1)
    {
        if (Yii::$app->user->can('changeUsers')) {

            if (($user = $this->getUserById($userId)) !== null) {

                if (Yii::$app->request->post()) {
                    $user->delete();
                    $this->redirect('/admin/');
                }

                return $this->render('delete', ['user' => $user, 'action' => 'delete']);

            } else {

                throw new NotFoundHttpException('Not found this user!');

            }

        } else {

            throw new NotFoundHttpException('Page not found.');

        }

    }

    public function actionChangeStatus($userId = -1, $status)
    {
        if (Yii::$app->user->can('changeUsers')) {

            if (($user = $this->getUserById($userId)) !== null) {

                if (Yii::$app->request->post()) {

                    switch ($status) {

                        case 'deactivate':
                            $user->changeStatus(Status::STATUS_INACTIVE);
                            break;

                        case 'activate':
                            $user->changeStatus(Status::STATUS_ACTIVE);
                            break;

                        case 'pending':
                            $user->changeStatus(Status::STATUS_PENDING);
                            break;

                    }

                    $user->update(false);

                    $this->redirect('/admin/');
                }

                return $this->render('delete', ['user' => $user, 'action' => $status]);

            } else {

                throw new NotFoundHttpException('Not found this user!');

            }

        } else {

            throw new NotFoundHttpException('Page not found.');

        }

    }



    private function getUserById($userId)
    {
        return User::findOne(['userId' => $userId]);
    }


}
