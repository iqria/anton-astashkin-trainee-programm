<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\ListingOptions;
use app\models\AuthAssignment;

?>

<h1>Users</h1>


<?php $form = ActiveForm::begin(); ?>

<?= Html::submitButton('Search for:', ['class' => 'btn btn-primary']) ?>

<?= $form->field($searchData, 'searchFieldUsers')->dropDownList(ListingOptions::getUsersFieldsList())?>

<?= $form->field($searchData, 'searchValue')?>

<?= $form->field($searchData, 'sortFieldUsers')->dropDownList(ListingOptions::getUsersFieldsList())?>

<?php
//ascending will be set by default
if ($searchData->sortOrder == null) {
    $searchData->sortOrder = 'ascending';
}
?>

<?= $form->field($searchData, 'sortOrder')->radioList(ListingOptions::getSortOrderList())?>

<?php ActiveForm::end();?>



<table class="contactsTable">
    <tr>
        <td>User Id</td>
        <td>Username</td>
        <td>Email</td>
        <td>Role</td>
        <td>Status</td>
        <td>Options</td>
        <td>Change status</td>
    </tr>

    <?php
        if (count($users) != 0):

            foreach ($users as $user):

            ?>

                <tr>
                    <td><?= Html::encode($user->userId);?></td>
                    <td><?= Html::encode($user->username);?></td>
                    <td><?= Html::encode($user->email)?></td>
                    <td><?= Html::encode(AuthAssignment::getItemNameByUserId($user->userId))?></td>
                    <td><?= Html::encode(Status::getStatusName($user->statusId))?></td>
                    <td>
                        <a href="/admin/edit-user/<?=$user->userId?>">Edit</a>
                        <a href="/admin/delete-user/<?=$user->userId?>">Delete</a>
                    </td>
                    <td>
                        <a href="/admin/change-status/<?=$user->userId?>/activate">Activate</a>
                        <a href="/admin/change-status/<?=$user->userId?>/deactivate">Deactivate</a>
                    </td>
                </tr>

            <?php
            endforeach;

            echo LinkPager::widget(['pagination' => $pagination]);

        else:

    ?>

    <tr>
        <!-- mb needs to be gone since how would one login, if users table is empty? -->
        <td colspan="6" class="emptyContactsWarning">There are no users yet!</td>
    </tr>

    <?php
        endif;
    ?>

</table>

