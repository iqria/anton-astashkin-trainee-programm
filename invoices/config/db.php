<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=invoices',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
