<!doctype HTML>
<html>
<head>
</head>
<body>

<?php
require_once('random_elem_array_functions.php');

$array = generateArray();
print_r($array);

echo "<br>Random element using shuffle: ".getRandElemShuffle($array);
echo "<br>Random element using sort + random index: ".getRandElemSort($array);
echo "<br>Random element using array_rand: ".getRandElemArrayRand($array);

?>


</body>
</html>
