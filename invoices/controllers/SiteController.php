<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new User(['scenario' => User::SCENARIO_REGISTER]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $imageFile = UploadedFile::getInstance($model, 'imageFile');

            if ($imageFile != null) {

                //has as filename to check later if such file was already uploaded
                $avatarFilePath = User::AVATAR_FILE_PATH . hash_file('md5', $imageFile->tempName) . "." . $imageFile->extension;

                $imageFile->saveAs($avatarFilePath);
                $model->avatar = $avatarFilePath;

            } else {

                $model->avatar = User::DEFAULT_AVATAR_FILE_PATH;

            }

            //autologin
            $identity = new LoginForm($model->email, $model->password);

            $model->save();

            $userRoleName = Yii::$app->authManager->getRole('user');
            Yii::$app->authManager->assign($userRoleName, $model->userId);

            $identity->login();

            return $this->redirect('/');
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionPasswordRestore()
    {
        //enterEmail -> get link to the password restore window
    }

}
