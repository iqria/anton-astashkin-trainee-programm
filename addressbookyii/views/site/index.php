<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">

        <h1>Welcome!</h1>

        <p class="lead">This is an address book application!</p>

        <p><a class="btn btn-xs btn-primary" href="/contacts">Browse Your contacts -></a></p>

    </div>

    <div class="body-content">

        <div class="row">
            <p>
                Here is some general information about this address book. Oh, btw, it is created by Anton.

            </p>
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Vestibulum sit amet neque ut sapien consequat dignissim nec sed odio.
                Fusce fermentum enim justo, ut eleifend odio maximus in. Nulla accumsan scelerisque quam in dapibus.
                Suspendisse potenti. Cras iaculis varius metus a varius.
                Integer non maximus nisi. Integer vestibulum augue eu mauris fermentum, a euismod lorem condimentum.
                Morbi eget varius nulla, sit amet convallis libero. Sed venenatis vestibulum enim nec maximus.
                Quisque finibus hendrerit quam, eu congue arcu pharetra vitae. Etiam tempus efficitur ante, id elementum ante vehicula a.
                Aliquam semper ac risus sit amet congue. Suspendisse bibendum, tortor eget rutrum finibus, nunc massa facilisis dui, pellentesque euismod eros erat ut libero.
                Ut ultrices, sem at ornare elementum, tortor magna vestibulum erat, eget laoreet magna urna vitae nunc. Maecenas dapibus odio a est consectetur blandit ac eget orci.
                Duis vitae nisl sapien.
            </p>
        </div>

    </div>
</div>
