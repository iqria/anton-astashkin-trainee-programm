<?php
define('LTR', 'ltr');
define('RTL', 'rtl');

function printVertically($input, $direction = LTR)
{	
    $string = trim($input); 
    $length = strlen($string);

    echo "Start $direction:</br>";    

    if(strtolower($direction) == LTR) {
	
        $start = 0;
        $dir = 1;

    } elseif(strtolower($direction) == RTL) {
        
        $start = $length - 1;
        $dir = -1;

    } else {
        $length = -1;
        echo "We've got a problem with a method type.</br>";
    }

    for($i = 0; $i < $length; $i++){
        echo $string{$start + $i * $dir}."</br>";
    }

    echo "End</br></br>";
}
