<?php

namespace app\controllers;

use yii;
use app\models\User;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\UnauthorizedHttpException;

class UserController extends Controller
{
    public function actionIndex($userId = null)
    {

        if (Yii::$app->user->can('user')) {


            if ($userId == null) {
                $userId = Yii::$app->user->id;
            } else {
                if (!Yii::$app->user->can('admin')) {
                    //mb need to throw not found
                    throw new UnauthorizedHttpException('Log is as admin!');
                }
            }

            $model = User::findById($userId);

            $model->scenario = User::SCENARIO_EDIT;

            unset($model->password);

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                    $imageFile = UploadedFile::getInstance($model, 'imageFile');

                    if ($imageFile != null) {

                        //has as filename to check later if such file was already uploaded
                        $avatarFilePath = User::AVATAR_FILE_PATH . hash_file('md5', $imageFile->tempName) . "." . $imageFile->extension;

                        $imageFile->saveAs($avatarFilePath);
                        $model->avatar = $avatarFilePath;

                    }

                $model->save();

            }


            return $this->render('index', ['model' => $model]);

        } else {

            throw new UnauthorizedHttpException('Please, log in first!');

        }
    }

    public function actionChangePassword()
    {
        if (Yii::$app->user->can('user')) {

            //changepass

        } else {

            throw new UnauthorizedHttpException('Please, log in first!');

        }
    }
}