<!doctype HTML>

<html>
<head>
    <style>
        .error {
            color: red;
            font-size: 0.8em;
        }
    </style>
</head>
<body>

<?php
define('DATA_FILE_PATH', 'data/dataFile.csv');
define('AVATAR_DIR', 'avatars/');

$rules = require_once('validationRules.php');
require_once('validation.php');

$file = fopen(DATA_FILE_PATH, 'a+');


if ($_POST['submit']) {

    $data = [
        'avatar' => $_FILES['avatar'],
        'fullName' => $_POST['fullName'],
        'email' => $_POST['email'],
        'mobPhone' => $_POST['mobPhone'],
        'homePhone' => $_POST['homePhone'],
        'workPhone' => $_POST['workPhone'],
        'address' => $_POST['address'],
        'city' => $_POST['city'],
        'state' => $_POST['state'],
        'zip' => $_POST['zip'],
        'birthday' => $_POST['birthday'],
    ];

    $errors = validate($data, $rules);
    $numErrors = count($errors);

    if ($numErrors == 0) {

        fseek($file, 0);
        $unique = true;

        //check if data is not already in the file
        while (!feof($file)) {
            if (implode('|', $data) == implode('|', fgetcsv($file, null, '|'))) {
                $unique = false;
                $numErrors = 1;
                break;
            }
        }

        if ($unique) {

            fseek($file, 0, SEEK_END);
            fputcsv($file, $data, '|');
            var_dump($data);

            header("Location: index.php");
            die();

        } else {

            echo '<span class="error">Data entered is already in the file!</span>';

        }

    } else {

        echo '<span class="error">Invalid data!</span>';

    }
}

fclose($file);
?>


<form method="post" action="<?= $_SERVER['SCRIPT_NAME'] ?>" enctype="multipart/form-data">

    <label for="fullName">Full Name <span class="required">*</span></label>
    <input type="text" name="fullName" id="fullName"
           value="<?php echo ($numErrors == 0) ? '' : ($data['fullName']) ?>">
    <?php
    if ($errors['fullName']) {
        echo $errors['fullName'];
    }
    ?>
    <br>

    <label for="email">Email <span class="required">*</span></label>
    <input type="text" name="email" id="email"
           value="<?php echo ($numErrors == 0) ? '' : ($data['email']) ?>">
    <?php
    if ($errors['email']) {
        echo $errors['email'];
    }
    ?>
    <br>

    <label for="mobPhone">Mobile phone</label>
    <input type="text" name="mobPhone" id="mobPhone"
           value="<?php echo ($numErrors == 0) ? '' : ($data['mobPhone']) ?>">
    <?php
    if ($errors['mobPhone']) {
        echo $errors['mobPhone'];
    }
    ?>
    <br>

    <label for="homePhone">Home phone</label>
    <input type="text" name="homePhone" id="homePhone"
           value="<?php echo ($numErrors == 0) ? '' : ($data['homePhone']) ?>">
    <?php
    if ($errors['homePhone']) {
        echo $errors['homePhone'];
    }
    ?>
    <br>

    <label for="workPhone">Work phone</label>
    <input type="text" name="workPhone" id="workPhone"
           value="<?php echo ($numErrors == 0) ? '' : ($data['workPhone']) ?>">
    <?php
    if ($errors['workPhone']) {
        echo $errors['workPhone'];
    }
    ?>
    <br>

    <label for="address">Address <span class="required">*</span></label>
    <input type="text" name="address" id="address"
           value="<?php echo ($numErrors == 0) ? '' : ($data['address']) ?>">
    <?php
    if ($errors['address']) {
        echo $errors['address'];
    }
    ?>
    <br>

    <label for="city">City <span class="required">*</span></label>
    <input type="text" name="city" id="city"
           value="<?php echo ($numErrors == 0) ? '' : ($data['city']) ?>">
    <?php
    if ($errors['city']) {
        echo $errors['city'];
    }
    ?>
    <br>

    <label for="state">State <span class="required">*</span></label>
    <input type="text" name="state" id="state"
           value="<?php echo ($numErrors == 0) ? '' : ($data['state']) ?>">
    <?php
    if ($errors['state']) {
        echo $errors['state'];
    }
    ?>
    <br>

    <label for="zip">Zip code <span class="required">*</span></label>
    <input type="text" name="zip" id="zip"
           value="<?php echo ($numErrors == 0) ? '' : ($data['zip']) ?>">
    <?php
    if ($errors['zip']) {
        echo $errors['zip'];
    }
    ?>
    <br>

    <label for="birthday">Birthday (yyyy-mm-dd) <span class="required">*</span></label>
    <input type="text" name="birthday" id="birthday"
           value="<?php echo ($numErrors == 0) ? '' : ($data['birthday']) ?>">
    <?php
    if ($errors['birthday']) {
        echo $errors['birthday'];
    }
    ?>
    <br>

    <label for="avatar">Avatar</label>
    <input type="file" name="avatar" id="avatar">
    <?php
    if ($errors['avatar']) {
        echo $errors['avatar'];
    }
    ?>
    <br>

    <p>
        * - required fields!
    </p>

    <input type="submit" name="submit" value="Add">

    <input type="button" value="Cancel" onclick="window.location.href='index.php'">

</form>


</body>
</html>
