<!doctype HTML>

<html>
<head>
</head>
<body>

<a href="forms/forms.php">Forms</a></br>

</br>

<a href="task1/1a/arraysum.php">1a) Array sum</a></br>
<a href="task1/1b/tabulate.php">1b) Tabulate</a></br>
<a href="task1/1c/multidim_structure.php">1c) Multidimensional structure</a></br>
<a href="task1/1d/print_recur_structure.php">1d) Print out recursive structure</a></br>
<a href="task1/1e/sum_leaves.php">1e) Sum "leaves"</a></br>
<a href="task1/1f/arguments_sum.php">1f) Function arguments sum</a></br>
<a href="task1/mines/mines.php">1g) Mines</a></br>
<a href="task1/1h/Errors.php">1h.1) Errors with array values multiplication</a></br>
<a href="task1/1h/global_keyword.php">1h.2) Using "global" keyword</a></br>

</br>

<a href="task2/2a/vertical_string.php">2a) Vertical string</a></br>
<a href="task2/2b/args_concat.php">2b) Function arguments concatenation</a></br>
<a href="task2/2c/args_concat_improved.php">2c) Function arguments concatenation (improved)</a></br>
<a href="task2/2d/search.php">2d) Search in text (needs improvments)</a></br>
<a href="task2/2d/search_replace.php">2e) Search in text (using str_replace)</a></br>
<a href="task2/2f/search_spaces.php">2f) Search in text (spaces skipped)</a></br>
<a href="task2/2g/search_nlbr.php">2g) Search in text (\n to &lt;br&gt;)</a></br>

</br>

<a href="task3/3a/random_numbers.php">3a) Random numbers in an array</a></br>
<a href="task3/3b/print_array.php">3b) Printing array</a></br>
<a href="task3/3c/bubble_sort.php">3c) Bubble sort</a></br>
<a href="task3/3d/insertion_sort.php">3d) Insertion sort</a></br>
<a href="task3/3e/nat_sort.php">3e) Native sorting functions</a></br>
<a href="task3/3f/array_of_asoc_arrays_sort.php">3f.1) Sorting array of associative arrays</a></br>
<a href="task3/3f/array_of_asoc_arrays_sort_custom.php">3f.2) Sorting array of associative arrays (Using custom bubble sort)</a></br>
<a href="task3/3g/user_tasks.php">3g) Table of user tasks</a></br>
<a href="task3/3h/acos_array_replacements.php">3h) Replacement word in text using associative array</a></br>
<a href="task3/3i/default_params.php">3i) Megring arrays</a></br>
<a href="task3/3j/random_elem_array.php">3j) Random element from array</a></br>
<a href="task3/3k/dashfy.php">3k) Dashfy</a></br>

</br>

<a href="files/a/files.php">a) Files and file uploads in php</a></br>
<a href="files/b/wav.php">b) Wav</a></br>
<a href="files/c/generating_file.php">c) Generating and sorting data in file</a></br>



</body>
</html>
