<?php
session_start();
//front controller

//configuration
ini_set('display_errors', 1);
error_reporting(E_ALL);

//connecting system files
define('ROOT', dirname(__FILE__));
define('FULL_TABLE_NAME_CONTACTS', 'addressbook.contacts');
define('FULL_TABLE_NAME_USERS', 'addressbook.users');
define('ERROR_404_PAGE_PATH', '/Error404');
define('LOGIN_PAGE_PATH', '/users/login');
define('RECORDS_PER_PAGE', 20);

require_once(ROOT . '/models/Contacts.php');
require_once(ROOT . '/models/Users.php');
require_once(ROOT . '/models/Admin.php');

require_once(ROOT . '/controllers/Controller.php');
require_once(ROOT . '/controllers/UsersController.php');
require_once(ROOT . '/controllers/AdminController.php');

require_once(ROOT . '/components/Router.php');
require_once(ROOT . '/components/Db.php');
require_once(ROOT . '/views/View.php');



//Call Router
$router = new Router();
$router->run();
