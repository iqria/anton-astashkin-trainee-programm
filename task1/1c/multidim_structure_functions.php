
<?php

function calculate($data)
{
    $output = [];
    foreach ($data as $year=>$yearValues) {
        $countYear = $year; //store year

        foreach ($yearValues as $month=>$monthValues) {
            $countMonth = $month;

            foreach ($monthValues as $day=>$dayValues) {

                foreach ($dayValues as $project) {
                    sumHours($project, $countYear, $countMonth, $output);
                }

            }
        }

    }

    return $output;
}

function sumHours($task, $countYear, $countMonth, &$output)
{
    foreach ($task as $k=>$v) {
        if (preg_match("/^Persone/", $k)) {

            switch ($k) {
                case 'Persone1': {
                    $output['Persone1'][$countYear][$countMonth] += $v;
                    break;
                }
                case 'Persone2': {
                    $output['Persone2'][$countYear][$countMonth] += $v;
                    break;
                }
                case 'Persone3': {
                    $output['Persone3'][$countYear][$countMonth] += $v;
                    break;
                }
            }

        } else {
            sumHours($v, $countYear, $countMonth, $output);
        }
    }
}

