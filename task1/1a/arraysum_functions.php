
<?php

define('ITER', 'iteration');
define('RECUR', 'recursive');
define('MIN_VALUE', 0);
define('MAX_VALUE', 10);
define('MIN_LENGTH', 0);
define('MAX_LENGTH', 10);

function straight_sum($array)
{
    $total = 0;

    foreach($array as $elem) {
        $total += $elem;
    }

    return $total;
}

function recursive_sum($val)
{

    if (sizeof($val) == 1) {
        return $val[0];
    }
    return array_shift($val) + recursive_sum($val);
}

function sum($array, $variant = ITER)
{
    if (strtolower($variant) == ITER) {
		return straight_sum($array);
	} elseif (strtolower($variant) == RECUR) {
		return recursive_sum($array);
	} else {
		return "Unrecognized operation.</br>";
	}
}

