<?php



class Error404Controller extends Controller
{
    public function actionIndex()
    {
        $this->view->render('Error404.php');
        return true;
    }
}