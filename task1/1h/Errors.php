<!doctype HTML>
<html>
<head>
</head>
<body>

<?php
error_reporting(0);
require_once('Errors_functions.php');

$a = [];
for ($i = 0; $i < 10; $i++) {
    $a[] = mt_rand(0, 100);
}

$b = $a; //$a will be modified, so a copy is made


//shows original array
echo "Initial array:<br />";
echo '[' . implode(',', $a) . ']'."<br /><br />";


//echos multiplied array
multiplyItemsNoReturn(10, $a);


//returns multiplied array
$a = multiplyItems(10, $a);

echo '[' . implode(',', $a) . ']'."<br />";


//returns multiplied array
multiplyItemsReference(10, $b);

echo '[' . implode(',', $b) . ']'."<br />";

?>

</body>
</html>
