<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'My Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <!--
        <p>Please fill out the following fields to register:</p>
    -->
    <img src="<?='/' . $model->avatar?>" class="avatar">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data'
        ],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'imageFile')->fileInput()->label('Change Avatar') ?>

    <?= $form->field($model, 'firstName') ?>

    <?= $form->field($model, 'lastName') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'phone') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'passwordConfirm')->passwordInput() ?>



    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= ''//Html::button('Cancel', ['class' => 'btn btn-primary', 'onclick' => "window.location.href=document.referrer"]) ?>

            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'edit-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
