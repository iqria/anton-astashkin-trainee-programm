<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use app\models\Status;

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a('New Invoice', ['/invoices/create'])?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
/*
        'rowOptions' => function ($model) {
            if ($model->statusId == Status::STATUS_NEW) {
                return ['class' => 'warning'];
            } elseif ($model->statusId == Status::STATUS_SHIPPED) {
                return ['class' => 'success'];
            }

        },
*/

        'columns' => [

            [
                'format' => 'text',
                'label' => 'Invoice #',
                'value' => 'number',
            ],

            [
                'format' => 'text',
                'label' => 'Client Name',
                'value' => function ($model) {
                    return $model->client->firstName . " " . $model->client->lastName;
                },
            ],

            [
                'format' => 'text',
                'label' => 'Date',
                'value' => 'date',
            ],

            [
                'format' => 'text',
                'label' => 'Status',
                'value' => function ($model) {
                    return $model->status->statusName;
                },
            ],
            [
                'format' => 'text',
                'label' => 'Total',
                'value' => function ($model) {
                    return $model->total . " $";
                },
            ],
            [
                'format' => 'raw',
                'label' => '',
                'value' => function ($model) {
                    $links = Html::a(Html::img('/img/system/edit.png'), "/invoices/edit/" . $model->invoiceId) . ' ';
                    $links .= Html::a(Html::img('/img/system/delete.png'), "/invoices/delete/" . $model->invoiceId) . ' ';

                    return $links;
                }
            ],
        ],
    ]) ?>


</div>
