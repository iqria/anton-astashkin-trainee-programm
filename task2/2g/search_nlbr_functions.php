<?php

define('DIR_LTR', 'ltr');
define('DIR_RTL', 'rtl');
define('CASE_INSENS', 'insens');
define('CASE_SENS', 'sens');

function position($input, $search, $case, $dir)
{

  if ($dir == DIR_RTL) {

    if ($case == CASE_SENS) {
      return strrpos($input, $search);
    } else {
      return strripos($input, $search);
    }

  } else {

    if ($case == CASE_SENS) {
      return strpos($input, $search);
    } else {
      return stripos($input, $search);
    }
  }
}

function search($input, $search, $case, $dir, $occurrences)
{
  if($occurrences == 0){
    return $input;
  }

  $length = strlen($search);
  $pos = position($input, $search, $case, $dir);


  if ($pos !== false) {

    $occurrences--;

    if ($dir == DIR_RTL) {

      $output = search(substr($input, 0, $pos), $search, $case, $dir, $occurrences) . "<span class=\"highlighted\">" . substr($input, $pos - $length + 1, $length) . "</span>" . substr($input, $pos + $length);

    } else {

      $output = substr($input, 0, $pos) . "<span class=\"highlighted\">" . substr($input, $pos, $length) . "</span>" . search(substr($input, $pos + $length), $search, $case, $dir, $occurrences);

    }


  } else {

    return $input;

  }

  return $output;

}
