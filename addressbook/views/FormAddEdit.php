<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>

    <title>Address book</title>

    <meta charset="UTF-8">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">

</head>

<body>

<div class="container">

    <div class="header">

        <div class="login">

            <a href="/users/logout">Log out</a>

        </div>

        <h1 class="logo">Address Book</h1>

        <div class="menu">
            <ul>
                <li><a href="/">Homepage</a></li>
                <li <?php if ($additionalInfo['type'] == 'add') {echo ' class="current_page_item"';}?>><a href="/contacts/add">Add</a></li>
                <li><a href="/contacts/search">Search</a></li>

                <?php
                if (isset($_SESSION['admin'])) {
                    echo   '<li><a href="/admin">Admin (users table)</a></li>
                                    <li><a href="/admin/add">Add user</a></li>
                                    <li><a href="/admin/search">Search user</a></li>';
                }
                ?>
            </ul>
        </div>


    </div>

    <div class="main">

        <div class="data">

            <form method="post" action="/contacts/<?= ($additionalInfo['type'] == 'add') ? 'add' : 'edit/' . $data['id'];?>" enctype="multipart/form-data">

                <label for="firstName">First Name <span class="required">*</span></label>
                <br>
                <input type="text" name="firstName" id="firstName"
                       value="<?php
                       if (isset($data['firstName'])) {
                           echo $data['firstName'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['firstName'])) {
                    echo $additionalInfo['firstName'];
                }
                ?>
                <br>

                <label for="lastName">Last Name <span class="required">*</span></label>
                <br>
                <input type="text" name="lastName" id="lastName"
                       value="<?php
                       if (isset($data['lastName'])) {
                           echo $data['lastName'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['lastName'])) {
                    echo $additionalInfo['lastName'];
                }
                ?>
                <br>


                <label for="email">Email <span class="required">*</span></label>
                <br>
                <input type="text" name="email" id="email"
                       value="<?php
                       if (isset($data['email'])) {
                           echo $data['email'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['email'])) {
                    echo $additionalInfo['email'];
                }
                ?>
                <br>


                <label for="mobPhone">Mobile phone</label>
                <br>
                <input type="text" name="mobPhone" id="mobPhone"
                       value="<?php
                       if (isset($data['mobPhone'])) {
                           echo $data['mobPhone'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['mobPhone'])) {
                    echo $additionalInfo['mobPhone'];
                }
                ?>
                <br>


                <label for="homePhone">Home phone</label>
                <br>
                <input type="text" name="homePhone" id="homePhone"
                       value="<?php
                       if (isset($data['homePhone'])) {
                           echo $data['homePhone'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['homePhone'])) {
                    echo $additionalInfo['homePhone'];
                }
                ?>
                <br>


                <label for="workPhone">Work phone</label>
                <br>
                <input type="text" name="workPhone" id="workPhone"
                       value="<?php
                       if (isset($data['workPhone'])) {
                           echo $data['workPhone'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['workPhone'])) {
                    echo $additionalInfo['workPhone'];
                }
                ?>
                <br>


                <label for="address">Address <span class="required">*</span></label>
                <br>
                <input type="text" name="address" id="address"
                       value="<?php
                       if (isset($data['address'])) {
                           echo $data['address'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['address'])) {
                    echo $additionalInfo['address'];
                }
                ?>
                <br>


                <label for="city">City <span class="required">*</span></label>
                <br>
                <input type="text" name="city" id="city"
                       value="<?php
                       if (isset($data['city'])) {
                           echo $data['city'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['city'])) {
                    echo $additionalInfo['city'];
                }
                ?>
                <br>


                <label for="state">State <span class="required">*</span></label>
                <br>
                <input type="text" name="state" id="state"
                       value="<?php
                       if (isset($data['state'])) {
                           echo $data['state'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['state'])) {
                    echo $additionalInfo['state'];
                }
                ?>
                <br>


                <label for="zip">Zip code <span class="required">*</span></label>
                <br>
                <input type="text" name="zip" id="zip"
                       value="<?php
                       if (isset($data['zip'])) {
                           echo $data['zip'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['zip'])) {
                    echo $additionalInfo['zip'];
                }
                ?>
                <br>


                <label for="birthday">Birthday (yyyy-mm-dd) <span class="required">*</span></label>
                <br>
                <input type="text" name="birthday" id="birthday"
                       value="<?php
                       if (isset($data['birthday'])) {
                           echo $data['birthday'];
                       }
                       ?>">
                <?php
                if (isset($additionalInfo['birthday'])) {
                    echo $additionalInfo['birthday'];
                }
                ?>
                <br>


                <label for="avatar">Avatar</label>
                <br>
                <input type="file" name="avatar" id="avatar">
                <?php
                if (isset($additionalInfo['avatar'])) {
                    echo $additionalInfo['avatar'];
                }
                ?>
                <br>


                <p>
                    * - required fields!
                </p>

                <input type="submit" name="submit" value="<?= ($additionalInfo['type'] == 'add') ? 'Add' : 'Edit';?>">

                <input type="button" value="Cancel" onclick="window.location.href='/'">

            </form>


        </div>

    </div>





</div>


</body>
</html>
