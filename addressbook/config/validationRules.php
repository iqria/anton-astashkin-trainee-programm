<?php

return [

    'fieldName' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[A-Za-z]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'error' => 'error'
        ]
    ],

    'firstName' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[A-Z][-A-Za-z\' .]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters and apostrophe for first name! Name must start with a capital letter!</span>',
            'lengthError' => '<span class="error">Invalid name length! Please, use not more than 50 characters and enter both Your first and last name!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'lastName' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[-A-Za-z\' .]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters and apostrophe for last name!</span>',
            'lengthError' => '<span class="error">Invalid name length! Please, use not more than 50 characters and enter both Your first and last name!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'email' => [
        'type' => 'filterVar',
        'required' => true,

        'parameters' => [
            'filter' => FILTER_VALIDATE_EMAIL,
            'minLength' => null,
            'maxLength' => null
        ],

        'messages' => [
            'requiredError' => '<span class="error">This field is required!</span>',
            'filterError' => '<span class="error">Please, enter a valid email!</span>'
        ]
    ],


    'mobPhone' => [
        'type' => 'regex',
        'required' => false,

        'parameters' => [
            'pattern' => '/^[0-9-() ]{6,16}$/',
            'minLength' => null,
            'maxLength' => null
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only digits and - ( ) characters!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>'
        ],

    ],


    'homePhone' => [
        'type' => 'regex',
        'required' => false,

        'parameters' => [
            'pattern' => '/^[0-9-() ]{6,16}$/',
            'minLength' => null,
            'maxLength' => null
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only digits and - ( ) characters!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>'
        ],

    ],


    'workPhone' => [
        'type' => 'regex',
        'required' => false,

        'parameters' => [
            'pattern' => '/^[0-9-() ]{6,16}$/',
            'minLength' => null,
            'maxLength' => null
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only digits and - ( ) characters!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>'
        ],

    ],


    'address' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[A-Za-z0-9 .,#-]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters and apostrophe!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'city' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[-A-Za-z\' ]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters and apostrophe!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],



    'state' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[-A-Za-z\' ()]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters, space and these symbols: \' ( )!</span>',
            'lengthError' => '<span class="error">Invalid length! Please, use not more than 50 characters!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'zip' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[0-9A-Z -]+$/',
            'minLength' => 4,
            'maxLength' => 16
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Invalid zip code!</span>',
            'lengthError' => '<span class="error">Invalid length! Please, use 4 to 16 characters!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'birthday' => [
        'type' => 'regexBirthday',
        'required' => true,

        'parameters' => [
            'pattern' => '/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/',
            'delimitersToChange' => [
                '-',
                ' ',
                '\\',
                '/',
                '.',
            ],
            'minLength' => null,
            'maxLength' => null
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Invalid data! Please, use yyyy-mm-dd format!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],

    'avatar' => [
        'type' => 'imageCheck',
        'required' => false,
        'parameters' => [
            'types' => [
                'jpg',
                'jpeg',
                'png',
                'gif',

            ],
            'minLength' => null,
            'maxLength' => null
        ],

        'messages' => [
            'uploadError' => '<span class="error">Sorry, Your file was not uploaded due to an error.</span>',
            'typeError' => '<span class="error">File is not an image.</span>',
            'extError' => '<span class="error">Sorry, only JPG, JPEG, PNG & GIF files are allowed.</span>',
            'sizeError' => '<span class="error">Sorry, Your file is too large.</span>'
        ]
    ],


    'login' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            //the one for names, not starting with a number
            //'pattern' => '/^[a-zA-Z_][A-Za-z0-9_]+$/',
            'pattern' => '/^[A-Za-z0-9_]+$/',
            'minLength' => 6,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters, numbers and underscore!</span>',
            'lengthError' => '<span class="error">Invalid login length! Please, use 6 to 50 characters!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'password' => [
        'type' => 'regexPassword',
        'required' => true,

        'parameters' => [
            'pattern' => [
                //check for character usage
                'general' => '/^[!?@$%&*()_\-\#A-Za-z0-9]+$/',
                //check for at least 1 capital letter
                'capital' => '/[A-Z]/',
                //check for at least 1 digit
                'digit' => '/[0-9]/',
                //check for at least1 spec char
                'specChar' => '/[!?@$%&*()_\-\#]/',
            ],
            'minLength' => 6,
            'maxLength' => 50
        ],

        'messages' => [
            'general' => '<span class="error">Please, use only letters, numbers and this special symbols: !, ?, @, $, %, &, *, (, ), _, -, # !</span>',
            'capital' => '<span class="error">Please, use at least 1 capital letter!</span>',
            'digit' => '<span class="error">Please, use at least 1 digit!</span>',
            'specChar' => '<span class="error">Please,  use at least 1 special character (!, ?, @, $, %, &, *, (, ), _, -, #, \, /)!</span>',
            'lengthError' => '<span class="error">Invalid password length! Please, use 6 to 50 characters!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],
];