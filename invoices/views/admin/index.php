<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */


use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use app\models\Status;
use app\models\Invoices;

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <!--
        <p>Please fill out the following fields to register:</p>
    -->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

        'rowOptions' => function ($model) {
            if ($model->statusId == Status::STATUS_ACTIVE) {
                return ['class' => 'success'];
            } elseif ($model->statusId == Status::STATUS_INACTIVE) {
                return ['class' => 'danger'];
            } else {
                return ['class' => 'warning'];
            }

        },

        'columns' => [
            [
                'format' => 'text',
                'label' => 'Name',
                'value' => function ($model) {
                    return $model->firstName . " " . $model->lastName;
                },
            ],
            'phone',
            'email',
            [
                'format' => 'text',
                'label' => 'Role',
                'value' => function ($model) {
                    return $model->role->item_name;
                },
            ],
            [
                'format' => 'text',
                'label' => 'Total Invoice Count',
                'value' => function ($model) {
                    return Invoices::countUsersInvoices($model->userId);
                }
            ],
            [
                'format' => 'raw',
                'label' => 'edit/deactivate options',
                'value' => function ($model) {
                    $links = Html::a(Html::img('/img/system/edit.png'), "/user/index/$model->userId") . ' ';

                    //button changes depending on current status
                    if ($model->statusId == Status::STATUS_ACTIVE) {

                        $links .= Html::a(Html::img('/img/system/active.png'), "/admin/change-status/$model->userId");

                    } else {

                        $links .= Html::a(Html::img('/img/system/inactive.png'), "/admin/change-status/$model->userId");

                    }

                    return $links;
                }
            ],
        ],
    ]) ?>

    <?=LinkPager::widget(['pagination' => $pagination]);?>

</div>
