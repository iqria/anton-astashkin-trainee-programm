<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\datepicker\DatePicker;

$this->title = 'Create Invoice';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please, fill out the following fields to add a new client:</p>

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
        ],
    ]); ?>

<div class=" col-lg-8 withBorder">

    <?= $form->field($client, 'firstName')->textInput(['class' => '']) ?>

    <?= $form->field($client, 'lastName')->textInput(['class' => '']) ?>

    <?= $form->field($client, 'phone')->label('Phone Number')->textInput(['class' => '']) ?>

    <?= $form->field($client, 'email')->label('E-Mail Address')->textInput(['class' => '']) ?>

    <?= $form->field($client, 'organization')->textInput(['class' => '']) ?>

    <?= $form->field($address, 'addressLine1')->textInput(['class' => '']) ?>

    <?= $form->field($address, 'addressLine2')->textInput(['class' => '']) ?>

    <?= $form->field($address, 'city')->textInput(['class' => '']) ?>

    <?= $form->field($address, 'country')->textInput(['class' => '']) ?>

    <?= $form->field($address, 'state')->textInput(['class' => '']) ?>

    <?= $form->field($address, 'zip')->textInput(['class' => '']) ?>
</div>

    <div class="col-lg-4 withBorder">

        <?= $form->field($invoice, 'number') ?>

        <?= $form->field($invoice, 'date')->widget(
            DatePicker::className(), [
                'inline' => false,
                'clientOptions' => [
                    'autoclose' => true,
                    //'format' => 'dd-mm-yyyy'
                    'format' => 'yyyy-mm-dd'
                ]
            ]
        ) ?>


        <?='User ID<br><span class="userId">' . $user->firstName . " " . $user->lastName . '</span>'?>

    </div>

    <div class="col-lg-12">

        <table id="new-invoice">
            <tr>
                <td>Name</td>
                <td>Description</td>
                <td>Price</td>
                <td>Qnt</td>
                <td>Tax</td>
                <td>Line Total</td>
            </tr>
        <?php
        foreach ($invoiceLines as $i => $invoiceLine): ?>
        <tr>
            <td><?= $form->field($invoiceLine, "[$i]name")->textInput(['class' => '']);?></td>
            <td><?= $form->field($invoiceLine, "[$i]description")->textInput(['class' => '']);?></td>
            <td><?= $form->field($invoiceLine, "[$i]price")->textInput(['class' => '']);?></td>
            <td><?= $form->field($invoiceLine, "[$i]quantity")->textInput(['class' => '']);?></td>
            <td><?= $form->field($invoiceLine, "[$i]tax")->textInput(['class' => '']);?></td>
            <td><?= $form->field($invoiceLine, "[$i]total")->textInput(['class' => ''])->textInput(['readonly' => true]);?></td>
        </tr>
        <?php endforeach; ?>

            <tr>
                <td colspan="3" class=""><?= Html::button('+ Add line', ['class' => 'btn btn-primary', 'onclick' => "window.location.href='/invoices/edit/" . $invoice->invoiceId . '/' . ($extraLines + 1) . "'"]) ?></td>
                <td colspan="2">
                    <?= $form->field($invoice, 'total')->textInput(['class' => ''])->textInput(['readonly' => true]);?>
                </td>
                <td>counts total</td>
            </tr>

            <tr>
                <td colspan="3"></td>
                <td colspan="2">Balance</td>
                <td></td>
            </tr>
            <table>
    </div>





    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>

            <?= Html::button('Cancel', ['class' => 'btn btn-primary', 'onclick' => "window.location.href='/clients/index'"]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    /*
    function insRow()
    {
        var table = document.getElementById('new-invoice');

        var newRow = 1;

        table.appendChild(newRow);
    }
    */
</script>
