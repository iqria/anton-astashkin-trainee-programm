<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>

    <title>Address book</title>

    <meta charset="UTF-8">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">

</head>

<body>

<div class="container">

    <div class="header">

        <div class="login">

            <a href="/users/logout">Log out</a>

        </div>

        <h1 class="logo">Address Book</h1>

        <div class="menu">
            <ul>
                <li><a href="/">Homepage</a></li>
                <li><a href="/contacts/add">Add</a></li>
                <li><a href="/contacts/search">Search</a></li>

                <?php
                if (isset($_SESSION['admin'])) {
                    echo   '<li><a href="/admin">Admin (users table)</a></li>
                                    <li><a href="/admin/add">Add user</a></li>
                                    <li><a href="/admin/search">Search user</a></li>';
                }
                ?>
            </ul>
        </div>


    </div>

    <div class="main">

        <div class="data">

            <p>
                <?php
                foreach ($data as $key=>$value) {
                    if ($key == 'id') {
                        continue;
                    }
                    echo "$key : $value <br>";
                }
                ?>
            </p>

            <form>
                <input type="button" value="Delete" onclick="window.location.href='<?=$_SERVER['REQUEST_URI']?>/true'">
                <input type="button" value="Cancel" onclick="window.location.href='/'">
            </form>

        </div>


    </div>





</div>


</body>
</html>
