<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>

    <title>Address book</title>

    <meta charset="UTF-8">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">

</head>

<body>

<div class="container">

    <div class="header">

        <div class="login">

            <a href="/users/logout">Log out</a>

        </div>

        <h1 class="logo">Address Book</h1>

        <div class="menu">
            <ul>
                <li class="current_page_item"><a href="/">Homepage</a></li>
                <li><a href="/contacts/add">Add</a></li>
                <li><a href="/contacts/search">Search</a></li>

                <?php
                if (isset($_SESSION['admin'])) {
                    echo   '<li><a href="/admin">Admin (users table)</a></li>
                            <li><a href="/admin/add">Add user</a></li>
                            <li><a href="/admin/search">Search user</a></li>';
                }
                ?>
            </ul>
        </div>


    </div>

    <div class="main">

        <div class="options">

            <form method="post" action="/">

                <input type="submit" name="sort" value="Sort by:">

                <select name="sortField">

                    <option value="noSorting" <?php if ($additionalInfo['sortingField'] == 'noSorting') {echo 'selected';}?>>Do not sort</option>
                    <option value="firstName" <?php if ($additionalInfo['sortingField'] == 'firstName') {echo 'selected';}?>>First Name</option>
                    <option value="lastName" <?php if ($additionalInfo['sortingField'] == 'lastName') {echo 'selected';}?>>Last Name</option>
                    <option value="email" <?php if ($additionalInfo['sortingField'] == 'email') {echo 'selected';}?>>Email</option>
                    <option value="mobPhone" <?php if ($additionalInfo['sortingField'] == 'mobPhone') {echo 'selected';}?>>Mobile Phone</option>
                    <option value="homePhone" <?php if ($additionalInfo['sortingField'] == 'homePhone') {echo 'selected';}?>>Home Phone</option>
                    <option value="workPhone" <?php if ($additionalInfo['sortingField'] == 'workPhone') {echo 'selected';}?>>Work Phone</option>
                    <option value="state" <?php if ($additionalInfo['sortingField'] == 'state') {echo 'selected';}?>>State</option>
                    <option value="zip" <?php if ($additionalInfo['sortingField'] == 'zip') {echo 'selected';}?>>Zip code</option>

                </select>

                <br>

                <input type="hidden" name="sortingOrder" value="asc">
                <label for="sortingOrder">Use descending order:</label>
                <input type="checkbox" name="sortingOrder" id="sortingOrder" value="desc" <?php if ($additionalInfo['sortingOrder'] == 'desc') echo 'checked';?>>


            </form>

        </div>

        <div class="data">

            <?php
            foreach ($data as $row) {
                echo $row;
            }
            ?>

        </div>

        <div class="pages">

        <?php

        if ($additionalInfo['sortingField'] != 'noSorting') {
            $sort = "/" . $additionalInfo['sortingField'] . "/" . $additionalInfo['sortingOrder'];
        } else {
            $sort = '';
        }

        if (isset($_SESSION['admin']) && $_SESSION['id'] == 1) {
            $controller = 'admin';
        } else {
            $controller = 'contacts';
        }

        for ($pageNum = 1, $maxPages = $additionalInfo['totalPages']; $pageNum <= $maxPages; $pageNum++) {
            echo "<a href='/$controller/" . $pageNum . $sort . "'>[" . $pageNum . "]</a>";
        }

        ?>

        </div>

    </div>

</div>


</body>
</html>
