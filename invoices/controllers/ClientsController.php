<?php

namespace app\controllers;

use app\models\Status;
use Yii;
use app\models\Address;
use app\models\Clients;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class ClientsController extends Controller
{
    public function actionIndex()
    {
        if (Yii::$app->user->can('user')) {

            $query = Clients::find()->where(['statusId' => Status::STATUS_ACTIVE]);

            $provider = new ActiveDataProvider([
                'sort' => false,
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('index', ['dataProvider' => $provider, 'pagination' => $provider->getPagination()]);

        } else {

            throw new ForbiddenHttpException('Please, log in or sign up first!');

        }

    }

    public function actionAddClient()
    {

        if (Yii::$app->user->can('user')) {

            $client = new Clients();
            $address = new Address();

            if ($client->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()) && $client->validate() && $address->validate()) {

                if ($id = Address::checkAddressExists($address)) {

                    $address = Address::findOne(['addressId' => $id]);

                } else {

                    $address->save();
                }

                $client->addressId = $address->getPrimaryKey();
                $client->save();

                return $this->redirect('/clients/index');
            }

            return $this->render('add', ['client' => $client, 'address' => $address]);

        } else {

            throw new ForbiddenHttpException('Please, log in or sign up first!');

        }

    }

    public function actionEdit($clientId = -1)
    {
        if (Yii::$app->user->can('user')) {

            if ($client = Clients::findOne(['clientId' => $clientId])) {

                $address = Address::findOne(['addressId' => $client->addressId]);

                if ($client->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()) && $client->validate() && $address->validate()) {

                    if ($id = Address::checkAddressExists($address)) {

                        $address = Address::findOne(['addressId' => $id]);

                    } else {

                        $address->save();
                    }

                    $client->addressId = $address->getPrimaryKey();
                    $client->save();

                    return $this->redirect('/clients/index');
                }

                return $this->render('edit', ['client' => $client, 'address' => $address]);

            } else {
                throw new NotFoundHttpException('Client was not found.');
            }

        } else {

            throw new ForbiddenHttpException('Please, log in or sign up first!');

        }
    }

    public function actionDelete($clientId = -1)
    {
        if (Yii::$app->user->can('user')) {

            if ($client = Clients::findOne(['clientId' => $clientId])) {

                if (Yii::$app->request->post()) {


                    $client->statusId = Status::STATUS_INACTIVE;
                    $client->save();

                    return $this->redirect('/clients/index');
                }

                return $this->render('delete', ['client' => $client]);

            } else {
                throw new NotFoundHttpException('Contact was not found.');
            }

        } else {

            throw new ForbiddenHttpException('Please, log in or sign up first!');

        }
    }

    //

}