<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Edit client';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please, fill out the following fields to edit client:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'register-form',
        'options' => [
            'class' => 'form-horizontal',
            //'enctype' => 'multipart/form-data'
        ],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <p>
        Client Information
    </p>

    <?= $form->field($client, 'firstName') ?>

    <?= $form->field($client, 'lastName') ?>

    <?= $form->field($client, 'email') ?>

    <?= $form->field($client, 'phone') ?>

    <?= $form->field($client, 'organization') ?>

    <p>
        Client address
    </p>

    <?= $form->field($address, 'addressLine1') ?>

    <?= $form->field($address, 'addressLine2') ?>

    <?= $form->field($address, 'city') ?>

    <?= $form->field($address, 'country') ?>

    <?= $form->field($address, 'state') ?>

    <?= $form->field($address, 'zip') ?>



    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Edit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

            <?= Html::button('Cancel', ['class' => 'btn btn-primary', 'onclick' => "window.location.href='/clients/index'"]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
