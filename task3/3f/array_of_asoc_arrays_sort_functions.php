<?php
define('ASCENDING', 'ascending');
define('DESCENDING', 'descending');
define('MIN_VALUE', 5);
define('MAX_VALUE', 15);
define('MIN_LENGTH', 5);
define('MAX_LENGTH', 15);

function generate(){

    $array = [];
    for($i = 0; $i < mt_rand(MIN_LENGTH, MAX_LENGTH); $i++){
        $array[$i] = ['priority'=>mt_rand(MIN_VALUE, MAX_VALUE)];
    }

    return $array;
}

function standardSort($array, $order = ASCENDING)
{
    if($order == ASCENDING){
        asort($array);
    } else {
        arsort($array);
    }

    return $array;
}
