<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;

?>

<h1>User to <?=$action?>:</h1>
<table class="usersTable">
    <tr>
        <td>User ID</td>
        <td><?= Html::encode($user->userId);?></td>
    </tr>
    <tr>
        <td>Username</td>
        <td><?= Html::encode($user->username);?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?= Html::encode($user->email)?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td><?= Html::encode(Status::getStatusName($user->statusId))?></td>
    </tr>

</table>

<br>

<div class="form-group">
    <?php $form = ActiveForm::begin(); ?>
    <?= Html::submitButton(ucfirst($action), ['class' => 'btn btn-primary']) ?>

    <?= Html::button('Cancel', ['class' => 'btn btn-primary', 'onclick' => "window.location.href='/admin/'"]) ?>
    <?php ActiveForm::end(); ?>
</div>
