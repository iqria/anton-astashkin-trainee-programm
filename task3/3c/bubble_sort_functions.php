<?php
define('ASCENDING', 'ascending');
define('DESCENDING', 'descending');
define('MIN_VALUE', 5);
define('MAX_VALUE', 15);
define('MIN_LENGTH', 5);
define('MAX_LENGTH', 15);

function bubble($a, $order = ASCENDING)
{

    if ($order == ASCENDING) {
        $offset1 = 0;
        $offset2 = 1;
    } else {
        $offset1 = 1;
        $offset2 = 0;
    }

    $length = count($a);

    if ($length > 1) {

        do {
            $swaps = 0;
            for ($i = 0; $i < $length - 1; $i++) {
                if ($a[$i + $offset1] > $a[$i + $offset2]) {
                    $temp = $a[$i];
                    $a[$i] = $a[$i + 1];
                    $a[$i + 1] = $temp;
                    $swaps++;
                }
            }
        } while ($swaps != 0);


    }
    return $a;

}
