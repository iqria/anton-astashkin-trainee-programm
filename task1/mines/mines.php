<!doctype HTML>

<html>
<head>
    <style>
        table {
            display: inline-block;
            border-collapse: collapse;
        }
        td {
            width: 20px;
            height: 20px;
        }

        table, th, td {
            border: 1px solid black;
        }

    </style>
</head>
<body>

<?php

require_once('minesFunctions.php');


if($_POST['submit']){

    if($_POST['rows']){

        if (is_int(intval($_POST['rows']))  && $_POST['rows'] > 0 && $_POST['rows'] < 101) {
            $rows = intval($_POST['rows']);
        } else {
            $rows = 5;
        }

    } else {
        $rows = 0;
    }

    if($_POST['columns']){

        if (is_int(intval($_POST['columns'])) && $_POST['columns'] > 0 && $_POST['columns'] < 101) {
            $columns = intval($_POST['columns']);
        } else {
            $columns = 5;
        }

    } else {
        $columns = 0;
    }

    if($_POST['mines']){

        if (is_int(intval($_POST['mines'])) && $_POST['mines'] >= 0) {
            $mines = intval($_POST['mines']);
        } else {
            $mines = 0;
        }

    } else {
        $mines = 0;
    }

    $array = generateArray($mines, $rows, $columns);
    drawMines($array);

}

?>

<form action="mines.php" method="post">

    <input type="text" name="rows" value="<?=$rows;?>">
    Number of rows (max 100)
    <br>
    <input type="text" name="columns" value="<?=$columns;?>">
    Number of columns (max 100)
    <br>
    <input type="text" name="mines" value="<?=$mines;?>">
    Number of mines (MUST be less than total number of cells)
    <br>

    <input type="submit" name="submit" value="Done!"><br>

</form>

</body>
</html>
