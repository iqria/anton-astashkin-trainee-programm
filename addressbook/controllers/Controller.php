<?php

class Controller
{
    public $view;

    protected $rules;

    function __construct()
    {
        $this->view = new View();

        $this->rules = require_once(ROOT . '/config/validationRules.php');

    }
}