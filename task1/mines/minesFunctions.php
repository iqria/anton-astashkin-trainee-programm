<?php
function generateArray($mines, $x, $y)
{

    $field = [];

    for ($i = 0; $i < $x + 2; $i++) { //создаем сетку с рамкой в 1 клетку вокруг
	    $columns = [];
	    $columns = array_pad($columns, $y + 2, 0);
        $field[] = $columns;
    }
    
    $max = $x * $y - 1;

    if ($mines > $max) {
        $mines = 0;
    }

    for ($placed = 0; $placed < $mines; $placed++) { //placing mines
        
         while (true) {//repeat until we place a mine
            $position = mt_rand(0 , $max);

            $posX = intval($position / $y + 1);
            $posY = $position % $y + 1;
            
            if($field[$posX][$posY] == 0){
                $field[$posX][$posY] = -1; //mine placed
                break;
            }
         }
    }
    
    for ($i = 1; $i < ($x + 1); $i++) {//calculate neighbours
        for ($j = 1; $j < ($y + 1); $j++) {
            if ($field[$i][$j] != -1) {
            $minesAround = 0;
                for ($dx = -1; $dx < 2; $dx++) {
                    for ($dy = -1; $dy < 2; $dy++) {
                        if ($field[$i + $dx][$j + $dy] == -1) {
                            $minesAround++;
                        }
                    }    
                }
            $field[$i][$j] = $minesAround;
            }
        }
    }

    return $field;

}

function drawMines($array)
{

    $arrayRows = count($array);
    $arrayCols = count($array[0]);

    echo "<table>";

    for ($i = 1; $i < $arrayRows - 1; $i++) {

        echo "<tr>";

        for ($j = 1; $j < $arrayCols - 1; $j++) {
            if ($array[$i][$j] == -1) {
                echo "<td>M</td>";
            } elseif ($array[$i][$j] == 0) {
                echo "<td></td>";
            } else {
                echo "<td>" . $array[$i][$j] . "</td>";
            }

        }

        echo "</tr>";
    }

    echo "</table>";
}
