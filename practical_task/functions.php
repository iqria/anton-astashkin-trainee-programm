<?php

define('TD_OPEN_TAG_LENGTH', 4);
define('TD_CLOSE_TAG_LENGTH', 5);
define('ASCENDING', 'ascending');
define('DESCENDING', 'descending');
define('DATA_FILE_PATH','data/dataFile.csv');
define('DATA_FIELD_INDEXES', serialize(['avatar' => 0,
    'fullName' => 1,
    'email' => 2,
    'mobPhone' => 3,
    'homePhone' => 4,
    'workPhone' => 5,
    'address' => 6,
    'city' => 7,
    'state' => 8,
    'zip' => 9,
    'birthday' => 10])
);


function drawTable($file, $selected = false)
{
    fseek($file, 0);

    $output = [];
    $output[] = " <table class='usersTable'>
                <tr>
                        <td>№</td>
                        <td>Avatar</td>
                        <td>Full name</td>
                        <td>Email</td>
                        <td>Mobile phone</td>
                        <td>Home phone</td>
                        <td>Work phone</td>
                        <td>Address</td>
                        <td>City</td>
                        <td>State</td>
                        <td>Zip code</td>
                        <td>Birthday</td>
                        <td>Options</td>
                </tr>";

    $userNumb = 1;

    if ($selected == false) {

        drawTableData($file, $output, $userNumb);

    } else {

        drawTableData($file, $output, $userNumb, $selected);

    }


    $output[] = "</table>";
    return $output;
}



function drawTableData($file, &$output, $userNumb = 1, $selected = false)
{
    while (!feof($file)) {

        //gets data of a single user
        $singleEntry = fgetcsv($file, 0,'|');
        //skip empty lines
        $num = count($singleEntry);

        if ($num > 1){

            if ($selected != false) {

                $searchField = key($selected);
                if ($singleEntry[$searchField] == $selected[$searchField]) {
                    drawTableRow($singleEntry, $num, $userNumb, $output);
                }

            } else {

                drawTableRow($singleEntry, $num, $userNumb, $output);

            }


            $userNumb++;
        }
    }

}

function drawTableRow($singleEntry, $num, $userNumb, &$output)
{
    $row = "<tr><td>$userNumb</td>";

    for ($i = 0; $i < $num; $i++) {
        if ($i == 0) {
            $avatarPath = $singleEntry[$i];
            $row .= "<td><img class='avatar' src='$avatarPath'></td>";
        } else {
            $row .= "<td>" . $singleEntry[$i] . "</td>";
        }

    }

    $row .= "<td><a href='editUser.php?user=$userNumb'>edit</a>/" .
        "<a href='editUserAvatar.php?user=$userNumb'>change avatar</a>/" .
        "<a href='deleteUser.php?user=$userNumb'>delete</a></td></tr>";

    $output[] = $row;
}






function sortRecords(&$data, $field, $order = ASCENDING) //data - array, field - name of needed data field
{
    if ($order == ASCENDING) {
        $compareResult = 1;
    } else {
        $compareResult = -1;
    }

    //get number of searched field
    $fieldNumb = unserialize(DATA_FIELD_INDEXES)[$field] + 1; //need to skip first table field (user number in table)

    $numOfRows = count($data); //1st and last rows must not be processed

    //$numOfRows - 2 because we need to preserve space for 1 more line while bubblesorting
    // and do not process last row (</table>)
    do {
        $swaps = 0;

        for ($i = 1; $i < $numOfRows - 2; $i++) {
            $row1 = $data[$i];
            $row2 = $data[$i + 1];

            if (strcmp(searchFieldInRow($row1, $fieldNumb), searchFieldInRow($row2, $fieldNumb)) == $compareResult) {
                $data[$i] = $row2;
                $data[$i + 1] = $row1;
                $swaps++;
            }
        }

        $numOfRows--;
    } while ($swaps != 0);


}

function searchFieldInRow($row, $fieldNumb)
{
    //4 - length of <td>, 5 - length of </td>
    while ($fieldNumb > 0) {
        $fieldNumb--;
        $pos = strpos($row, '</td>');
        $row = substr($row, $pos + TD_CLOSE_TAG_LENGTH);
    }

    $pos = strpos($row, '</td>');
    return substr($row, TD_OPEN_TAG_LENGTH, $pos - TD_OPEN_TAG_LENGTH);
}







//functions for sorting in file
function sortRecordsInFile($field)
{

    $fieldNumb = unserialize(DATA_FIELD_INDEXES)[$field];

    $data = file(DATA_FILE_PATH);

    file_put_contents(DATA_FILE_PATH, implode("", bubbleFile($data, $fieldNumb)));

}

function bubbleFile($data, $fieldNumb)
{

    $num = count($data);

    do {
        $swaps = 0;

        for($i = 0; $i < $num - 1; $i++) {

            $record1 = explode('|', $data[$i]);
            $record2 = explode('|', $data[$i + 1]);

            if (strcmp($record1[$fieldNumb], $record2[$fieldNumb]) == 1) {

                $data[$i] = implode('|', $record2);
                $data[$i + 1] = implode('|', $record1);

                $swaps++;
            }
        }

        $num--;

    } while ($swaps != 0);

    return $data;
}

//creates array[fieldNumber, data to search] and calls drawTable function with this parameter
function searchByField($file, $field, $value)
{

    return drawTable($file, [unserialize(DATA_FIELD_INDEXES)[$field] => $value]);

}