<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $changeContacts = $auth->createPermission('changeContacts');
        $changeContacts->description = 'User can change contacts data';

        $viewAllContacts = $auth->createPermission('viewAllContacts');
        $viewAllContacts->description = 'Admin can view all contacts';

        $changeUsers = $auth->createPermission('changeUsers');
        $changeUsers->description = 'Admin can change users data';

        $auth->add($changeContacts);
        $auth->add($viewAllContacts);
        $auth->add($changeUsers);


        $user = $auth->createRole('user');
        $admin = $auth->createRole('admin');

        $auth->add($user);
        $auth->add($admin);

        $auth->addChild($user, $changeContacts);
        $auth->addChild($admin, $viewAllContacts);
        $auth->addChild($admin, $changeUsers);
        $auth->addChild($admin, $user);

    }
}