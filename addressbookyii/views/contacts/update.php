<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Contacts */

$this->title = 'Update Contacts: ' . ' ' . $model->contactId;
$this->params['breadcrumbs'][] = ['label' => 'Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->contactId, 'url' => ['view', 'id' => $model->contactId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contacts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'firstName')->label('First Name'); ?>
    <?= $form->field($model, 'lastName')->label('Last Name'); ?>
    <?= $form->field($model, 'email'); ?>
    <?= $form->field($model, 'mobilePhone')->label('Mobile phone'); ?>
    <?= $form->field($model, 'homePhone')->label('Home phone'); ?>
    <?= $form->field($model, 'workPhone')->label('Work phone'); ?>
    <?= $form->field($model, 'address'); ?>
    <?= $form->field($model, 'city'); ?>
    <?= $form->field($model, 'state'); ?>
    <?= $form->field($model, 'zip'); ?>
    <?= $form->field($model, 'birthday'); ?>
    <?= $form->field($model, 'imageFile')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton('Update contact', ['class' => 'btn btn-primary']) ?>

        <?= Html::button('Cancel', ['class' => 'btn btn-primary', 'onclick' => "window.location.href='/contacts/'"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
