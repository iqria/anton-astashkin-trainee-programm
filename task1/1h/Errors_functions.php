
<?php

function multiplyItems($mult, $a)
{
    for ($i = 0; $i < sizeof($a); $i++) {
        $a[$i] *= $mult;
    }

    return $a;
}

function multiplyItemsNoReturn($mult, $a)
{
    for ($i = 0; $i < sizeof($a); $i++) {
        $a[$i] *= $mult;
    }

    echo "[".implode(',', $a)."]</br>";
}

function multiplyItemsReference($mult, &$a)
{
    for ($i = 0; $i < sizeof($a); $i++) {
        $a[$i] *= $mult;
    }
}
