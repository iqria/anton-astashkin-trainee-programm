<?php
define('CHUNK_DESCRIPTOR_NAME', 'RIFF');
define('CHUNK_SIZE_FIELD_LENGTH', 4);

define('FMT_SUBCHUNK_NAME', 'fmt');
define('SUBCHUNK_1_SIZE_FIELD_LENGTH', 4);
define('AUDIO_FORMAT_FIELD_LENGTH', 2);
define('NUM_CHANNELS_FIELD_LENGTH', 2);
define('SAMPLE_RATE_FIELD_LENGTH', 4);
define('BYTE_RATE_FIELD_LENGTH', 4);
define('BLOCK_ALIGN_FIELD_LENGTH', 2);
define('BITS_PER_SAMPLE_FIELD_LENGTH', 2);

define('DATA_SUBCHUNK_NAME', 'data');
define('DATA_SUBCHUNK_2_FIELD_LENGTH', 4);


function makeHex($string)
{
    for ($i = 0; $i < strlen($string); $i++) {
        $hex[] = sprintf("%02X", ord($string[$i]));
    }

    return implode(" ", $hex);
}


function getRIFFInfo($fileData)
{

    $posChunk = stripos($fileData, CHUNK_DESCRIPTOR_NAME);
    return "RIFF:<br>Chunksize " . makeHex(substr($fileData, $posChunk + strlen(CHUNK_DESCRIPTOR_NAME), CHUNK_SIZE_FIELD_LENGTH)) . "<br><br>";
}



function getFMTInfo($fileData){

    $posFmt = stripos($fileData, FMT_SUBCHUNK_NAME);
    $output = "";
    $output .=  "fmt_:<br>subchunk1size: " . makeHex(substr($fileData, $posFmt + 4, SUBCHUNK_1_SIZE_FIELD_LENGTH)) . "<br>"; $posFmt += 4 + SUBCHUNK_1_SIZE_FIELD_LENGTH;
    $output .=  "audio format: " . makeHex(substr($fileData, $posFmt, AUDIO_FORMAT_FIELD_LENGTH)) . "<br>"; $posFmt += AUDIO_FORMAT_FIELD_LENGTH;
    $output .=  "number of channels: " . makeHex(substr($fileData, $posFmt, NUM_CHANNELS_FIELD_LENGTH)) . "<br>"; $posFmt += NUM_CHANNELS_FIELD_LENGTH;
    $output .=  "sample rate: " . makeHex(substr($fileData, $posFmt, SAMPLE_RATE_FIELD_LENGTH)) . "<br>"; $posFmt += SAMPLE_RATE_FIELD_LENGTH;
    $output .=  "byte rate: " . makeHex(substr($fileData, $posFmt, BYTE_RATE_FIELD_LENGTH)) . "<br>"; $posFmt += BYTE_RATE_FIELD_LENGTH;
    $output .=  "block align: " . makeHex(substr($fileData, $posFmt, BLOCK_ALIGN_FIELD_LENGTH)) . "<br>"; $posFmt += BLOCK_ALIGN_FIELD_LENGTH;
    $output .=  "bits per sample: " . makeHex(substr($fileData, $posFmt, BITS_PER_SAMPLE_FIELD_LENGTH)) . "<br><br>"; $posFmt += BITS_PER_SAMPLE_FIELD_LENGTH;

    return $output;
}

function getDATAInfo($fileData)
{

    $posData = stripos($fileData, DATA_SUBCHUNK_NAME);
    return "data:<br>subchunk2size " . makeHex(substr($fileData, $posData + strlen(DATA_SUBCHUNK_NAME), DATA_SUBCHUNK_2_FIELD_LENGTH)) . "<br><br>";
}
