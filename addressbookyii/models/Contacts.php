<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "contacts".
 *
 * @property string $contactId
 * @property string $userId
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $mobilePhone
 * @property string $homePhone
 * @property string $workPhone
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $birthday
 * @property string $avatar
 */
class Contacts extends ActiveRecord
{
    const SORTING_ORDER_ASC = 'ascending';
    const SORTING_ORDER_DESC = 'descending';

    const AVATAR_FILE_PATH = 'img/avatar/';

    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'address', 'city', 'state', 'zip', 'birthday', 'email'], 'required'],

            [['email'], 'unique'],

            [['firstName', 'lastName', 'address', 'city', 'state'], 'string', 'max' => 255],

            [['mobilePhone', 'homePhone', 'workPhone', 'zip', 'birthday'], 'string', 'max' => 32],
            //check for valid chars in first/last name
            [['firstName', 'lastName'], 'match', 'pattern' => '/^[A-Za-z \']+$/', 'message' => 'Please, use only letters, spaces and apostrophe!'],
            //check for numbers, -, (, ) only in phones
            [['mobilePhone', 'homePhone', 'workPhone'], 'match', 'pattern' => '/^[0-9-\(\)]+$/', 'message' => 'Please, use only digits, dashes and parenthesis!'],
            //divide into smaller groups
            [['address', 'city', 'state'], 'match', 'pattern' => '/^[A-Za-z0-9 \'\-.]+$/', 'message' => 'Please, use only letters, digits, spaces, dash and apostrophe!'],

            ['email', 'email'],

            ['imageFile', 'file', 'extensions' => 'jpg, jpeg, png', 'maxSize' => pow(2, 21)], //2mb limit

            ['avatar', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contactId' => 'Contact ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'mobilePhone' => 'Mobile Phone',
            'homePhone' => 'Home Phone',
            'workPhone' => 'Work Phone',
            'address' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip',
            'birthday' => 'Birthday',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->userId = \Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }

    public function getImageUrl()
    {
        return \Yii::$app->request->BaseUrl.'/' . $this->avatar;
    }
}
