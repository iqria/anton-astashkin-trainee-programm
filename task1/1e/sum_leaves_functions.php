
<?php

function sumLeaves($data)
{

    if(!(is_array($data))){

        return $data;

    } else {

        $sum = 0;

        foreach($data as $element){
           $sum += sumLeaves($element);
        }

        return $sum;
    }
}
