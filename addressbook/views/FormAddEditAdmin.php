<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>

    <title>Address book</title>

    <meta charset="UTF-8">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">

</head>

<body>

<div class="container">

    <div class="header">

        <div class="login">

            <a href="/users/logout">Log out</a>

        </div>

        <h1 class="logo">Address Book</h1>

        <div class="menu">
            <ul>
                <li><a href="/">Homepage</a></li>
                <li><a href="/contacts/add">Add</a></li>
                <li><a href="/contacts/search">Search</a></li>

                <?php
                if (isset($_SESSION['admin'])) {
                    if ($additionalInfo['type'] == 'add'){
                        $class = 'class="current_page_item"';
                    } else {
                        $class ='';
                    }

                    echo   '<li><a href="/admin">Admin (users table)</a></li>
                            <li' . $class . '><a href="/admin/add">Add user</a></li>
                            <li><a href="/admin/search">Search user</a></li>';
                }
                ?>
            </ul>
        </div>


    </div>

    <div class="main">

        <div class="options">


        </div>

        <div class="data">

            <form method="post" action="/admin/<?= ($additionalInfo['type'] == 'add') ? 'add' : 'edit/' . $data['id'];?>">

                <label for="login">Login</label>
                <br>
                <input type="text" name="login" id="login"
                       value="<?php
                       if (isset($data['login'])) {
                           echo $data['login'];
                       }
                       ?>">

                <?php
                //show errors in login if any
                if (isset($additionalInfo['login'])) {
                    echo $additionalInfo['login'];
                }
                ?>
                <br>
                <br>


                <label for="email">Email</label>
                <br>
                <input type="text" name="email" id="email"
                       value="<?php
                       if (isset($data['email'])) {
                           echo $data['email'];
                       }
                       ?>">

                <?php
                //show errors in login if any
                if (isset($additionalInfo['email'])) {
                    echo $additionalInfo['email'];
                }
                ?>
                <br>
                <br>

                <label for="password">Password</label>
                <br>
                <input type="text" name="password" id="password">
                <?php
                //show errors in login if any
                if (isset($additionalInfo['password'])) {
                    echo $additionalInfo['password'];
                }
                ?>
                <br>
                <br>

                <input type="submit" name="submit" value="<?= ($additionalInfo['type'] == 'add') ? 'Add' : 'Edit';?>">

                <input type="button" value="Cancel" onclick="window.location.href='/'">

            </form>

        </div>

    </div>

</div>


</body>
</html>
