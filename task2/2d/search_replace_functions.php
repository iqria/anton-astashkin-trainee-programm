<?php

define('DIR_LTR', 'ltr');
define('DIR_RTL', 'rtl');
define('CASE_INSENS', 'insens');
define('CASE_SENS', 'sens');

function position($input, $search, $case, $dir)
{

    if ($dir == DIR_RTL) {

        if ($case == CASE_SENS) {
            return strrpos($input, $search);
        } else {
            return strripos($input, $search);
        }

    } else {

        if ($case == CASE_SENS) {
            return strpos($input, $search);
        } else {
            return stripos($input, $search);
        }
    }
}

function replace($input, $search, $case)
{

    if($case == CASE_SENS){
        $output = str_replace($search, "<span class='highlighted'>" . $search . "</span>", $input);
    } else {
        $output = str_ireplace($search, "<span class='highlighted'>" . $search . "</span>", $input);
    }

    return $output;
}

function search($input, $search, $case, $dir, $occurrences)
{
    if($occurrences == 0){
        return $input;
    }

    $length = strlen($search);
    $pos = position($input, $search, $case, $dir);


    if ($pos !== false) {

        $occurrences--;

        if ($dir == DIR_LTR) {

            $output = replace(substr($input, 0, $pos + $length), substr($input, $pos, $length), $case) . search(substr($input, $pos + $length), $search, $case, $dir, $occurrences);

        } else {

            $output = search(substr($input, 0, $pos), $search, $case, $dir, $occurrences) . replace(substr($input, $pos), substr($input, $pos, $length), $case) ;

        }


    } else {

        return $input;

    }

    return $output;

}



