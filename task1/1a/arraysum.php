<!doctype HTML>

<html>
<head>
</head>
<body>

<?php

require_once('arraysum_functions.php');

$array = [];
for($i = 0; $i < mt_rand(MIN_LENGTH, MAX_LENGTH); $i++){
    $array[] = mt_rand(MIN_VALUE, MAX_VALUE);
}


echo "array elements: ".implode(', ', $array)."</br>straight sum: ".sum($array)."</br></br>";
echo "array elements: ".implode(', ', $array)."</br>recursive sum: ".sum($array, 'recursive')."</br></br>";
echo "array elements: ".implode(', ', $array)."</br>False type sum: ".sum($array, 'sadasd')."</br></br>";

?>

</body>
</html>
