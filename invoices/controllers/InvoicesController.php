<?php

namespace app\controllers;

use Yii;
use app\models\Status;
use app\models\Clients;
use app\models\InvoiceLines;
use app\models\Invoices;
use app\models\Address;
use app\models\User;
use yii\web\Controller;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\UnauthorizedHttpException;

class InvoicesController extends Controller
{
    public function actionIndex()
    {
        if (Yii::$app->user->can('user')) {

            $query = Invoices::find()->joinWith(['client', 'status'])->where([
                'and',
                ['invoices.userId' => \Yii::$app->user->id],
                ['or',
                    ['invoices.statusId' => Status::STATUS_NEW],
                    ['invoices.statusId' => Status::STATUS_SHIPPED]
                ],

            ]);

            $provider = new ActiveDataProvider([
                'sort' => false,
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('index', ['dataProvider' => $provider]);


        } else {

            throw new UnauthorizedHttpException('Please, log in first!');

        }
    }


    public function actionCreate($lines = 1)
    {
        if (Yii::$app->user->can('user')) {

            if (!isset($lines) || $lines < 1) {
                $lines = 1;
            }

            $client = new Clients();
            $address = new Address();
            $invoice = new Invoices();

            $invoice->number = Invoices::getSmallestUnusedId();

            $invoiceLines = [];

            for ($i = 0; $i < $lines; $i++){
                $invoiceLines[] = new InvoiceLines();
            }

            if ($client->load(Yii::$app->request->post()) &&
                $address->load(Yii::$app->request->post()) &&
                $invoice->load(Yii::$app->request->post())) {

                //check if address is already in db
                if ($id = Address::checkAddressExists($address)) {

                    $address = Address::findOne(['addressId' => $id]);
                //save if not
                } else {

                    $address->save();

                }

                //check if client is already in db
                if ($id = Clients::checkClientExists($client)) {

                    $client = Clients::findOne(['clientId' => $id]);
                //save if not
                } else {

                    $client->addressId = $address->getPrimaryKey();
                    $client->save();

                }

                //add client, address and user information to invoice
                $invoice->clientId = $client->getPrimaryKey();
                $invoice->addressId = $address->getPrimaryKey();
                $invoice->userId = Yii::$app->user->id;
                $invoice->total = 0;
                $invoice->save();

                //get all invoice lines
                if (Model::loadMultiple($invoiceLines, Yii::$app->request->post())) {

                    foreach ($invoiceLines as $invoiceLine) {
                        $invoiceLine->invoiceId = $invoice->getPrimaryKey();
                    }
                }

                //count total for each line, sum to invoice total
                foreach ($invoiceLines as $invoiceLine) {
                    $invoiceLine->total = $invoiceLine->price * $invoiceLine->quantity * (1 + $invoiceLine->tax);
                    $invoice->total += $invoiceLine->total;
                    $invoiceLine->save();
                }

                $invoice->save();

                //if invoice is last for this user - update lastInvoice field
                if ($client->lastInvoice < $invoice->date) {
                    $client->lastInvoice = $invoice->date;
                }

                $client->totalSpentCount += $invoice->total;
                $client->save();

                return $this->redirect('/invoices/index');
            }

            $user = User::findOne(['userId' => Yii::$app->user->id]);

            return $this->render('create', ['client' => $client,
                                            'address' => $address,
                                            'invoice' => $invoice,
                                            'invoiceLines' => $invoiceLines,
                                            //'invoiceLines' => $invoiceLines,
                                            'user' => $user,
                                            'lines' => $lines
            ]);

        } else {

            throw new UnauthorizedHttpException('Please, log in first!');

        }

    }


    public function actionEdit($invoiceId = -1, $extraLines = 0)
    {
        if (Yii::$app->user->can('user')) {

            $invoice = Invoices::findOne(['invoiceId' => $invoiceId]);
            $client = Clients::findOne(['clientId' => $invoice->clientId]);
            $address = Address::findOne(['addressId' => $invoice->addressId]);

            $invoiceLines = InvoiceLines::find()->where(['invoiceId' => $invoiceId]);
            $lines = $invoiceLines->count();
            $invoiceLines = $invoiceLines->all();

            for ($i = 0; $i < $extraLines; $i++) {
                $invoiceLines[] = new InvoiceLines();
            }


            if ($client->load(Yii::$app->request->post()) &&
                $address->load(Yii::$app->request->post()) &&
                $invoice->load(Yii::$app->request->post())) {

                //check if address is already in db
                if ($id = Address::checkAddressExists($address)) {

                    $address = Address::findOne(['addressId' => $id]);
                    //save if not
                } else {

                    $address->save();

                }

                //check if client is already in db
                if ($id = Clients::checkClientExists($client)) {

                    $client = Clients::findOne(['clientId' => $id]);
                    //save if not
                } else {

                    $client->addressId = $address->getPrimaryKey();
                    $client->save();

                }

                //add client, address and user information to invoice
                $invoice->clientId = $client->getPrimaryKey();
                $invoice->addressId = $address->getPrimaryKey();
                $invoice->userId = Yii::$app->user->id;

                //get all invoice lines
                if (Model::loadMultiple($invoiceLines, Yii::$app->request->post())) {

                    foreach ($invoiceLines as $invoiceLine) {
                        $invoiceLine->invoiceId = $invoice->getPrimaryKey();
                    }
                }
                $invoice->total = 0;
                //count total for each line, sum to invoice total
                foreach ($invoiceLines as $invoiceLine) {
                    $invoiceLine->total = $invoiceLine->price * $invoiceLine->quantity * (1 + $invoiceLine->tax);
                    $invoice->total += $invoiceLine->total;
                    $invoiceLine->save();
                }

                $invoice->save();

                //if invoice is last for this user - update lastInvoice field
                if ($client->lastInvoice < $invoice->date) {
                    $client->lastInvoice = $invoice->date;
                }

                $client->totalSpentCount += $invoice->total;
                $client->save();

                return $this->redirect('/invoices/index');
            }

            $user = User::findOne(['userId' => Yii::$app->user->id]);

            return $this->render('edit', ['client' => $client,
                'address' => $address,
                'invoice' => $invoice,
                'invoiceLines' => $invoiceLines,
                'user' => $user,
                'lines' => $lines,
                'extraLines' => $extraLines,
            ]);

        } else {

            throw new UnauthorizedHttpException('Please, log in first!');

        }

    }
}