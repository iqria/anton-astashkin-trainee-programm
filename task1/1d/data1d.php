<?php
$data1d = [
    [
        'text' => 'Home',
        'link' => 'home.html'
    ],
    [
        'text' => 'Projects',
        'link' => 'projects.html',
        'items' => [
            [
                'text' => 'Project1',
                'link' => 'project1.html',
            ],
            [
                'text' => 'Project2',
                'link' => 'project2.html',
                'items' => [
                    [
                        'text' => 'Subproject 2',
                        'link' => 'subproject2.html',
                    ],
                    [
                        'text' => 'Subproject 3',
                        'link' => 'subproject3.html',
                    ],
                    [
                        'text' => 'Subproject 4',
                        'link' => 'subproject4.html',
                    ],
                ]
            ]
        ]
    ],
];
