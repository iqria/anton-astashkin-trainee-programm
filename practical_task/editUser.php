<!doctype HTML>

<html>
<head>
    <style>
        .error {
            color: red;
            font-size: 0.8em;
        }
    </style>
</head>
<body>

<?php
define('DATA_FILE_PATH', 'data/dataFile.csv');

$rules = require_once('validationRules.php');
require_once('validation.php');
require_once('functionsEdit.php');

$file = fopen(DATA_FILE_PATH, 'r+');

//getting to needed user
$userNumb = $_GET['user'];

//userNumb must be preserved
{
    $userNumbLoop = $userNumb;
    while ($userNumbLoop > 1) {
        fgets($file);
        $userNumbLoop--;
    }
}

//grab users data
$dataToEdit = fgetcsv($file, null, '|');

if ($_POST['submit']) {

    if ($_FILES['avatar']) {
        $avatar = $_FILES['avatar'];
    } else {
        $avatar = $dataToEdit[0];
    }

    $data = [
        'avatar' => $avatar,
        'fullName' => $_POST['fullName'],
        'email' => $_POST['email'],
        'mobPhone' => $_POST['mobPhone'],
        'homePhone' => $_POST['homePhone'],
        'workPhone' => $_POST['workPhone'],
        'address' => $_POST['address'],
        'city' => $_POST['city'],
        'state' => $_POST['state'],
        'zip' => $_POST['zip'],
        'birthday' => $_POST['birthday'],
    ];

    $errors = validate($data, $rules);
    $numErrors = count($errors);

    if ($numErrors == 0) {

        fseek($file, 0);
        $unique = true;


        //check if data is not already in the file
        //need to skip avatar comparison
        while (!feof($file)) {
            if (implode('|', $data) == implode('|', fgetcsv($file, null, '|'))) {
                $unique = false;
                $numErrors = 1;
                break;
            }
        }

        if ($unique) {

            changeDataInFile(DATA_FILE_PATH, $dataToEdit, $data);
            header("Location: index.php");
            die();

        } else {

            echo '<span class="error">Data entered is already in the file!</span>';

        }

    } else {

        echo '<span class="error">Invalid data!</span>';

    }

} else {
    $data = [
        'avatar' => $dataToEdit[0],
        'fullName' => $dataToEdit[1],
        'email' => $dataToEdit[2],
        'mobPhone' => $dataToEdit[3],
        'homePhone' => $dataToEdit[4],
        'workPhone' => $dataToEdit[5],
        'address' => $dataToEdit[6],
        'city' => $dataToEdit[7],
        'state' => $dataToEdit[8],
        'zip' => $dataToEdit[9],
        'birthday' => $dataToEdit[10],
    ];
}

fclose($file);
?>

<form method="post" action="<?= $_SERVER['SCRIPT_NAME'] . "?user=$userNumb" ?>">

    <label for="fullName">Full Name <span class="required">*</span></label>
    <input type="text" name="fullName" id="fullName"
           value="<?= $data['fullName'] ?>">
    <?php
    if ($errors['fullName']) {
        echo $errors['fullName'];
    }
    ?>
    <br>

    <label for="email">Email <span class="required">*</span></label>
    <input type="text" name="email" id="email"
           value="<?= $data['email'] ?>">
    <?php
    if ($errors['email']) {
        echo $errors['email'];
    }
    ?>
    <br>

    <label for="mobPhone">Mobile phone</label>
    <input type="text" name="mobPhone" id="mobPhone"
           value="<?= $data['mobPhone'] ?>">
    <?php
    if ($errors['mobPhone']) {
        echo $errors['mobPhone'];
    }
    ?>
    <br>

    <label for="homePhone">Home phone</label>
    <input type="text" name="homePhone" id="homePhone"
           value="<?= $data['homePhone'] ?>">
    <?php
    if ($errors['homePhone']) {
        echo $errors['homePhone'];
    }
    ?>
    <br>

    <label for="workPhone">Work phone</label>
    <input type="text" name="workPhone" id="workPhone"
           value="<?= $data['workPhone'] ?>">
    <?php
    if ($errors['workPhone']) {
        echo $errors['workPhone'];
    }
    ?>
    <br>

    <label for="address">Address <span class="required">*</span></label>
    <input type="text" name="address" id="address"
           value="<?= $data['address'] ?>">
    <?php
    if ($errors['address']) {
        echo $errors['address'];
    }
    ?>
    <br>

    <label for="city">City <span class="required">*</span></label>
    <input type="text" name="city" id="city"
           value="<?= $data['city'] ?>">
    <?php
    if ($errors['city']) {
        echo $errors['city'];
    }
    ?>
    <br>

    <label for="state">State <span class="required">*</span></label>
    <input type="text" name="state" id="state"
           value="<?= $data['state'] ?>">
    <?php
    if ($errors['state']) {
        echo $errors['state'];
    }
    ?>
    <br>

    <label for="zip">Zip code <span class="required">*</span></label>
    <input type="text" name="zip" id="zip"
           value="<?= $data['zip'] ?>">
    <?php
    if ($errors['zip']) {
        echo $errors['zip'];
    }
    ?>
    <br>

    <label for="birthday">Birthday (yyyy-mm-dd) <span class="required">*</span></label>
    <input type="text" name="birthday" id="birthday"
           value="<?= $data['birthday'] ?>">
    <?php
    if ($errors['birthday']) {
        echo $errors['birthday'];
    }
    ?>
    <br>


    <p>
        * - required fields!
    </p>

    <input type="submit" name="submit" value="Edit">

    <input type="button" value="Cancel" onclick="window.location.href='index.php'">

</form>


</body>
</html>