<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;

?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($user, 'username'); ?>

<?= $form->field($user, 'email'); ?>

<?= $form->field($user, 'statusId')->dropDownList(Status::getStatusList()); ?>

<?= $form->field($user, 'password')->passwordInput() ?>

<?= $form->field($user, 'passwordConfirm')->passwordInput() ?>

<div class="form-group">
    <?= Html::submitButton(ucfirst($method) . ' contact', ['class' => 'btn btn-primary']) ?>

    <?= Html::button('Cancel', ['class' => 'btn btn-primary', 'onclick' => "window.location.href='/admin/'"]) ?>
</div>

<?php ActiveForm::end(); ?>

