<?php

namespace app\controllers;

use app\models\ListingOptions;
use app\models\Contacts;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ContactsController extends Controller
{

    public function actionIndex()
    {

        //checks if a user can see contacts
        if (Yii::$app->user->can('changeContacts')) {

            $searchData = new ListingOptions();

            $searchData->load(Yii::$app->request->get());
            
            //checks if a user is admin or just have rights to see all contacts
            if (Yii::$app->user->can('viewAllContacts') && $searchData->viewAll) {

                $query = Contacts::find();

            } else {

                $query = Contacts::find()->where(['userId' => Yii::$app->user->id]);

            }

            if ($searchData->searchValue != '' && $searchData->searchField != 'contactId') {
                $query->where(['like', $searchData->searchField, $searchData->searchValue]);
            }


            if ($searchData->sortOrder == 'ascending') {

                $query->orderBy([$searchData->sortField => SORT_ASC]);

            } elseif ($searchData->sortOrder == 'descending') {

                $query->orderBy([$searchData->sortField => SORT_DESC]);

            }


            $pagination = new Pagination([
                'defaultPageSize' => 10,
                'totalCount' => $query->count(),
            ]);

            $contacts = $query->offset($pagination->offset)->limit($pagination->limit)->all();

            return $this->render('index', ['contacts' => $contacts, 'pagination' => $pagination, 'searchData' => $searchData]);

        } /*else {

            throw new NotFoundHttpException('Please, log in first!');

        }
*/
    }

    public function actionAdd()
    {
        if (Yii::$app->user->can('changeContacts')) {

            $contact = new Contacts();

            if ($contact->load(Yii::$app->request->post()) && $contact->validate()) {

                $imageFile = UploadedFile::getInstance($contact, 'imageFile');

                if ($imageFile != null) {

                    //has as filename to check later if such file was already uploaded
                    $avatarFilePath = Contacts::AVATAR_FILE_PATH . hash_file('md5', $imageFile->tempName) . "." . $imageFile->extension;

                    $imageFile->saveAs($avatarFilePath);
                    $contact->avatar = $avatarFilePath;

                }

                $contact->userId = Yii::$app->user->id;

                $contact->save();
                $this->redirect('/contacts/');

            }

            return $this->render('add', ['contact' => $contact, 'method' => 'add'] );

        } else {

            throw new NotFoundHttpException('Please, log in first!');

        }

    }

    public function actionEdit($contactId = -1)
    {
        if (Yii::$app->user->can('changeContacts')) {

            if (($contact = $this->getContactById($contactId)) !== null) {

                if ($contact->load(Yii::$app->request->post()) && $contact->validate()) {

                    $imageFile = UploadedFile::getInstance($contact, 'imageFile');
                    
                    if ($imageFile != null) {

                        //has as filename to check later if such file was already uploaded
                        $avatarFilePath = Contacts::AVATAR_FILE_PATH . hash_file('md5', $imageFile->tempName) . "." . $imageFile->extension;

                        $imageFile->saveAs($avatarFilePath);
                        $contact->avatar = $avatarFilePath;

                    }

                    $contact->save();
                    $this->redirect('/contacts/');
                }

                return $this->render('add', ['contact' => $contact, 'method' => 'edit'] );

            } else {

                throw new NotFoundHttpException('Not found this contact!');

            }

        } else {

            throw new NotFoundHttpException('Please, log in first!');

        }
    }


    public function actionDelete($contactId = -1)
    {
        if (Yii::$app->user->can('changeContacts')) {

            if (($contact = $this->getContactById($contactId)) !== null) {

                if (Yii::$app->request->post()) {
                    $contact->delete();
                    $this->redirect('/contacts/');
                }

                return $this->render('delete', ['contact' => $contact]);

            } else {

                throw new NotFoundHttpException('Not found this contact!');

            }

        } else {

            throw new NotFoundHttpException('Please, log in first!');

        }

    }



    private function getContactById($contactId)
    {
        return Contacts::findOne([
            'contactId' => $contactId,
            //admin has viewAllContacts permission and can get any contact to edit/delete
            (Yii::$app->user->can('viewAllContacts')) ? : 'userId' => Yii::$app->user->id
        ]);
    }

}
