
<?php
define('MIN_VALUE', 0);
define('MAX_VALUE', 10);
define('MIN_LENGTH', 0);
define('MAX_LENGTH', 10);

function sumArgs()
{
    /*
    $args = func_get_args();
    $total = 0;

    foreach($args as $value){
        $total += intval($value);
    }

    return $total;
     */

    return array_sum(func_get_args());
}
