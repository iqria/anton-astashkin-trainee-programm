<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $userId
 * @property integer $avatar
 * @property string $firstName
 * @property string $lastName
 * @property string $phone
 * @property string $email
 * @property string $authKey
 * @property string $password
 * @property string $statusId
 *
 * @property Status $statusId0
 */
class User extends ActiveRecord implements IdentityInterface
{

    public $imageFile;

    public $passwordConfirm = false;

    const AVATAR_FILE_PATH = 'img/avatar/';
    const DEFAULT_AVATAR_FILE_PATH = 'img/avatar/default.png';

    const SCENARIO_LOGIN = 'login';
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_EDIT = 'edit';
    const SCENARIO_CHANGE_STATUS = 'changeStatus';

    public static function tableName()
    {
        return 'users';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_LOGIN] = ['email', 'password'];
        $scenarios[self::SCENARIO_REGISTER] = ['firstName', 'lastName', 'email', 'phone', 'password', 'passwordConfirm', 'avatar', 'imageFile'];
        $scenarios[self::SCENARIO_EDIT] = ['firstName', 'lastName', 'email', 'phone', 'password', 'passwordConfirm', 'avatar', 'imageFile'];
        $scenarios[self::SCENARIO_CHANGE_STATUS] = ['statusId'];
        return $scenarios;
    }


    public function rules()
    {

        return  [

            [['firstName', 'lastName', 'email', 'password', 'passwordConfirm'], 'required', 'on' => 'register'],
/*
            ['passwordConfirm', 'required', 'when' => function($model) {
                return $model->password != '';
            }],
*/
            ['email', 'unique', 'message' => 'Sorry, this email already exists!', 'on' => ['register', 'edit']],

            [['firstName', 'lastName'], 'match', 'pattern' => '/^[a-zA-Z]{3,32}$/', 'message' => 'Please, use only 3 to 32 letters!'],

            ['email', 'email'],

            ['phone', 'match', 'pattern' => '/^[0-9]/', 'message' => 'Invalid phone number!'],

            ['phone', 'match', 'pattern' => '/^[0-9\-\(\)]{6,32}$/', 'message' => 'Please, use only digits, dashes and parenthesis!'],

            ['statusId', 'match', 'pattern' => '/(10|20)/'],

            ['password', 'match', 'pattern' => '/^[!?@$%&*()_\-\#A-Za-z0-9]+$/', 'message' => 'Please, use only letters, digits and this special symbols: !, ?, @, $, %, &, *, (, ), _, -, # !'],

            ['password', 'match', 'pattern' => '/[a-zA-Z]/', 'message' => 'Please, use at least 1 letter!'],

            ['password', 'match', 'pattern' => '/[0-9]/', 'message' => 'Please, use at least 1 digit!'],

            ['password', 'match', 'pattern' => '/[!?@$%&*()_\-\#]/', 'message' => 'Please, use at least 1 special character (!, ?, @, $, %, &, *, (, ), _, -, #, \, /)!'],

            //need to fix a problem when a valid password was entered, passwordConfirm matches it, and than password was altered
            //passwordConfirm remains glowing green
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password'],

            ['authKey', 'safe'],

            ['imageFile', 'file', 'extensions' => 'jpg, jpeg, png', 'maxSize' => pow(2, 21)], //2mb limit

            ['avatar', 'string', 'max' => 255],

        ];

    }




    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'email' => 'E-mail',
        ];
    }



    //identity interface
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->userId;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    //--------------------------------------------


    public static function findById($userId)
    {
        return static::findOne(['userId' => $userId]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    //use with login scenario
    public static function getPasswordHashByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function validatePassword($password)
    {
        return password_verify($password, $this->password);
    }





    public function changeStatus($newStatus)
    {
        $this->statusId = $newStatus;
    }



    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {
                $this->authKey = \Yii::$app->security->generateRandomString();
                if (!isset($this->statusId)) {
                    $this->statusId = 10;
                    //$this->statusId = Status::STATUS_ACTIVE;
                }
            }

            //if not editing profile's password -> hash it
            if (!(($this->scenario == User::SCENARIO_EDIT || $this->scenario == User::SCENARIO_CHANGE_STATUS) && $this->password == '')) {
                $this->password = password_hash($this->password, PASSWORD_DEFAULT);
            } else {
                unset($this->password);
            }

            return true;
        }

        return false;

    }

    public function getRole()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'userId']);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['statusId' => 'statusId']);
    }

}
