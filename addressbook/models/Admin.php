<?php


class Admin
{
    //seems working
    public static function getDataList($pageNumb, $sortingField = 'noSorting', $sortingOrder)
    {
        $db = Db::getConnection();

        if ($sortingField !== 'noSorting') {
            $sort = " ORDER BY $sortingField $sortingOrder";
        } else {
            $sort = "";
        }

        $query = "SELECT * FROM " . FULL_TABLE_NAME_USERS . $sort . " LIMIT " . ($pageNumb - 1) * RECORDS_PER_PAGE . ", " . RECORDS_PER_PAGE;
        $result = $db->query($query);

        return Admin::fetchResultUsers($result);

    }

    //+
    public static function getDataById($id)
    {
        $db = Db::getConnection();

        $result = $db->query("SELECT * FROM " . FULL_TABLE_NAME_USERS . " WHERE userID = $id;");

        if ($result !== false) {

            return Admin::fetchResultUsers($result)[0];

        } else {

            return false;
        }

    }

    //+
    public static function addData($data)
    {

        $db = Db::getConnection();

        $query = "INSERT INTO " . FULL_TABLE_NAME_USERS;

        $query .= " VALUES (NULL, ";

        foreach ($data as $column=>$value) {
            $query .= "'$value', ";
        }

        //need to cut last coma
        $query = substr($query, 0, strrpos($query, ','));
        $query .= ");";

        $db->query($query);

    }

    //+
    public static function getUsersSearch($fieldName, $fieldValue, $pageNumb, $sortingField, $sortingOrder)
    {
        if ($sortingField !== null) {
            $sort = " ORDER BY $sortingField $sortingOrder";
        } else {
            $sort = "";
        }

        $db = Db::getConnection();
        $result = $db->query("SELECT * FROM " . FULL_TABLE_NAME_USERS . " WHERE $fieldName='$fieldValue' $sort LIMIT " . ($pageNumb - 1) * RECORDS_PER_PAGE . ", " . RECORDS_PER_PAGE . ";");
        $contactsList = Admin::fetchResultUsers($result);

        return ['data' => $contactsList, 'rowsNumber'  => count($contactsList)];
    }

    //+
    public static function editDataById($id, $data)
    {

        unset($data['id']);

        $query = "UPDATE " . FULL_TABLE_NAME_USERS . " SET ";

        foreach ($data as $column=>$value) {
            $query .= "$column ='$value', ";
        }

        //need to cut last coma
        $query = substr($query, 0, strrpos($query, ','));

        $query .= " WHERE userId = $id;";

        $db = Db::getConnection();
        $db->query($query);
    }
    //+
    public static function deleteDataById($id)
    {
        $db = Db::getConnection();

        $db->query("DELETE FROM " . FULL_TABLE_NAME_USERS . " WHERE userId = $id;");

    }

    //+
    public static function getTableNumberOfRows()
    {
        $db = Db::getConnection();

        $numb = $db->query("SELECT COUNT(*) FROM " . FULL_TABLE_NAME_USERS . ";");

        return ($numb->fetch()[0]);
    }

	public static function fetchResultUsers($result)
	{

        $dataList = [];

        $i = 0;

        if ($row = $result->fetch()) {

            do {

                $dataList[$i]['id'] = $row['userId'];
                $dataList[$i]['login'] = $row['login'];
                $dataList[$i]['email'] = $row['email'];
                $i++;

            } while ($row = $result->fetch());

        } else {

            $dataList[$i]['id'] =  '-';
            $dataList[$i]['login'] = '-';
            $dataList[$i]['email'] =  '-';

        }

		return $dataList;
	}
}
