
<?php

function printRow($x)
{

    if (($x / 0.5) %2  == 0) {
        $highlighting = 'class="highlighted"'; 
    } else {
        $highlighting = '';
    }

    echo "<tr  $highlighting> ";

    if ($x < 1) {
        echo "<td>" . $x * $x . "</td>";
    } else {
        echo "<td>" . sqrt($x) . "</td>";
    }

    echo "</tr>";

}

function tabulate_while()
{
    echo "<table><tr><td>while</td></tr>";

    $x = -10;
    while ($x <= 10) {

        printRow($x);
        $x += 0.5;

    }

    echo "</table>";
	
}

function tabulate_for()
{
    echo "<table><tr><td>for</td></tr>";
  
    for ($x = -10; $x <= 10; $x += 0.5) {

        printRow($x);

    }

    echo "</table>";
	
}
