<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "invoiceLines".
 *
 * @property string $invoiceLineId
 * @property string $invoiceId
 * @property string $name
 * @property string $description
 * @property double $price
 * @property integer $quantity
 * @property double $tax
 * @property double $total
 */
class InvoiceLines extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoiceLines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //after adding js funciton to automatically update total - it becomes required
            [['invoiceId', 'name', 'description', 'price', 'quantity', 'tax', /*'total'*/], 'required', 'message' => ''],
            [['invoiceId', 'quantity'], 'integer', 'message' => ''],
            [['price', 'tax', 'total'], 'number', 'message' => ''],
            [['name', 'description'], 'string', 'max' => 255, 'message' => ''],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoiceLineId' => '',
            'invoiceId' => '',
            'name' => '',
            'description' => '',
            'price' => '',
            'quantity' => '',
            'tax' => '',
            'total' => '',
        ];
    }

    public function getInvoice()
    {
        return $this->hasOne(Invoices::className(), ['invoiceId' => 'invoiceId']);
    }
}
