<?php

function generateFileContent($file)
{
    $length = floor(pow(2, 5) * 3.3);

    for ($i = 0; $i < $length; $i++) {
        $string = mt_rand(0, 9) . mt_rand(0, 9) . "\n";
        fwrite($file, $string);
    }
}


function getNumberOfLines($file)
{
    $linesNumber = 0;

    fseek($file, 0, SEEK_SET);

    while (!feof($file)) {
        fgets($file);
        $linesNumber++;
    }

    return $linesNumber;
}




function bubbleFile($file)
{
    $num = getNumberOfLines($file);

        do {
            $swaps = 0;

            fseek($file, 0, SEEK_SET);

            for($i = 0; $i < $num - 2; $i++) {

                $apos = ftell($file);
                $a = fgets($file);
                $bpos = ftell($file);
                $b = fgets($file);
                fseek($file, $bpos);

                if (intval(trim($a)) > intval(trim($b))) {
                    fseek($file, $bpos);
                    fwrite($file, $a);
                    fseek($file, $apos);
                    fwrite($file, $b);
                    $swaps++;
                }
            }

            $num--;

        } while ($swaps != 0);

}
