<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>

    <title>Address book</title>

    <meta charset="UTF-8">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">

</head>

<body>

<div class="container">

    <div class="header">

        <div class="login">

            <a href="/users/login">Login</a>
            <a href="/users/register">Register</a>

        </div>

        <h1 class="logo">Address Book</h1>

        <div class="menu">

            <ul>
                <li><a href="/">Homepage</a></li>
                <li><a href="/contacts/add">Add</a></li>
                <li><a href="/contacts/search">Search</a></li>

                <?php
                if (isset($_SESSION['admin'])) {
                    echo   '<li><a href="/admin">Admin (users table)</a></li>
                                    <li><a href="/admin/add">Add user</a></li>
                                    <li><a href="/admin/search">Search user</a></li>';
                }
                ?>
            </ul>

        </div>

    </div>


    <div class="main">

        <div class="options">

        </div>


        <div class="data">

            <form method="post" action="/users/register">


                <label for="login">Login</label>
                <br>
                <input type="text" name="login" id="login"
                       value="<?php
                       if (isset($data['login'])) {
                           echo $data['login'];
                       }
                       ?>">

                <?php
                //show errors in login if any
                if (isset($additionalInfo['login'])) {
                    echo $additionalInfo['login'];
                }
                ?>
                <br>
                <br>


                <label for="email">Email</label>
                <br>
                <input type="text" name="email" id="email"
                       value="<?php
                       if (isset($data['email'])) {
                           echo $data['email'];
                       }
                       ?>">

                <?php
                //show errors in login if any
                if (isset($additionalInfo['email'])) {
                    echo $additionalInfo['email'];
                }
                ?>
                <br>
                <br>


                <label for="password">Password</label>
                <br>
                <input type="password" name="password" id="password">
                <?php
                //show errors in login if any
                if (isset($additionalInfo['password'])) {
                    echo $additionalInfo['password'];
                }
                ?>
                <br>
                <br>

                <label for="passwordConfirm">Confirm password</label>
                <br>
                <input type="password" name="passwordConfirm" id="passwordConfirm">
                <?php
                //show errors in login if any
                if (isset($additionalInfo['passwordConfirm'])) {
                    echo $additionalInfo['passwordConfirm'];
                }
                ?>
                <br>
                <br>

                <input type="submit" name="submit" value="Register">

                <input type="button" value="Return" onclick="window.location.href='/'">

            </form>


        </div>


    </div>





</div>


</body>
</html>
