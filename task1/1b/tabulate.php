<!doctype HTML>

<html>
<head>
<style>

table {
    display: inline-block;
    border-collapse: collapse;
}

table, th, td {
    border: 1px solid black;
}

.highlighted {
    background-color: yellow;
}

</style>
</head>
<body>

<?php
require_once('tabulate_functions.php');

tabulate_while();
tabulate_for();

?>

</body>
</html>
