<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\ListingOptions;

?>

<h1>Contacts</h1>

<?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::to(['contacts/index']),]); ?>

<?php

if (Yii::$app->user->can('viewAllContacts')) {

    echo $form->field($searchData, 'viewAll')->checkbox($searchData->viewAll == 1 ? ['checked' => 'checked'] : []);

}

?>

<?= Html::submitButton('Search for:', ['class' => 'btn btn-primary']) ?>

<?= $form->field($searchData, 'searchField')->dropDownList(ListingOptions::getContactsFieldsList())?>

<?= $form->field($searchData, 'searchValue')?>

<?= $form->field($searchData, 'sortField')->dropDownList(ListingOptions::getContactsFieldsList())?>

<?php
    //ascending will be set by default
    if ($searchData->sortOrder == null) {
        $searchData->sortOrder = 'ascending';
    }
?>

<?= $form->field($searchData, 'sortOrder')->radioList(ListingOptions::getSortOrderList())?>


<?php ActiveForm::end();?>

<table class="contactsTable">
    <tr>
        <td>Avatar</td>
        <td>Name</td>
        <td>Email</td>
        <td>Mobile Phone</td>
        <td>Home Phone</td>
        <td>Work Phone</td>
        <td>Address</td>
        <td>City</td>
        <td>State</td>
        <td>Zip</td>
        <td>Birthday</td>
        <td>Options</td>
    </tr>

    <?php
        if (count($contacts) != 0):

            foreach ($contacts as $contact):
            ?>

                <tr>
                    <td>
                        <img src="<?= Yii::$app->request->baseUrl . '/'  . $contact->avatar ?>" class=" img-avatar" >
                    </td>
                    <td><?= Html::encode("$contact->firstName $contact->lastName");?></td>
                    <td><?= Html::encode($contact->email)?></td>
                    <td><?= Html::encode($contact->mobilePhone);?></td>
                    <td><?= Html::encode($contact->homePhone);?></td>
                    <td><?= Html::encode($contact->workPhone);?></td>
                    <td><?= Html::encode($contact->address);?></td>
                    <td><?= Html::encode($contact->city);?></td>
                    <td><?= Html::encode($contact->state);?></td>
                    <td><?= Html::encode($contact->zip);?></td>
                    <td><?= Html::encode($contact->birthday);?></td>
                    <td>
                        <a href="/contacts/edit/<?=$contact->contactId?>">Edit</a>
                        <a href="/contacts/delete/<?=$contact->contactId?>">Delete</a>
                    </td>
                </tr>

            <?php
            endforeach;


        else:

    ?>

    <tr>
        <td colspan="12" class="emptyContactsWarning">You don't have any contacts yet.</td>
    </tr>

    <?php
        endif;
    ?>

</table>

<?= LinkPager::widget(['pagination' => $pagination]); ?>

