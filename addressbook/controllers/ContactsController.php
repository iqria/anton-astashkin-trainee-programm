<?php


class ContactsController extends Controller
{

	public function actionIndex($pageNumb = 1, $sortingField = 'noSorting', $sortingOrder = 'asc')
	{

        if (isset($_POST['sort'])) {
            $sortingField = $_POST['sortField'];
            $sortingOrder = $_POST['sortingOrder'];
        }

		$contactsList = Contacts::getContactsList($pageNumb, $sortingField, $sortingOrder);

        if ($contactsList === false) {

            $this->redirectToPage(ERROR_404_PAGE_PATH);

        } else {

            $totalPages = ceil(Contacts::getNumberOfRows() / RECORDS_PER_PAGE);

            $data = $this->createContactsTable($contactsList);

            $this->view->render('DataTable.php', $data, ['totalPages' => $totalPages, 'sortingField' => $sortingField, 'sortingOrder' => $sortingOrder]);

            return true;
        }

	}

    public function actionAdd()
    {
        $submittedData = null;
        $additionalInfo = null;

        if (isset($_POST['submit'])) {

            $submittedData = [
                'firstName' => $_POST['firstName'],
                'lastName' => $_POST['lastName'],
                'email' => $_POST['email'],
                'mobPhone' => $_POST['mobPhone'],
                'homePhone' => $_POST['homePhone'],
                'workPhone' => $_POST['workPhone'],
                'address' => $_POST['address'],
                'city' => $_POST['city'],
                'state' => $_POST['state'],
                'zip' => $_POST['zip'],
                'birthday' => $_POST['birthday'],
				'avatar' => $_FILES['avatar'],
            ];

            $additionalInfo = $this->validate($submittedData);

            if (empty($additionalInfo)) {

                Contacts::addContact($submittedData);

                //add redirect
                $this->redirectToPage('/');
            }
        }

        $additionalInfo['type'] = 'add';
        $this->view->render('FormAddEdit.php', $submittedData, $additionalInfo);
        return true;
    }

    public function actionEdit($id)
    {
        if (preg_match('/[0-9]+/', $id)) {

            if (isset($_POST['submit'])) {

                $contactData = [
                    'firstName' => $_POST['firstName'],
                    'lastName' => $_POST['lastName'],
                    'email' => $_POST['email'],
                    'mobPhone' => $_POST['mobPhone'],
                    'homePhone' => $_POST['homePhone'],
                    'workPhone' => $_POST['workPhone'],
                    'address' => $_POST['address'],
                    'city' => $_POST['city'],
                    'state' => $_POST['state'],
                    'zip' => $_POST['zip'],
                    'birthday' => $_POST['birthday'],
                    'avatar' => $_FILES['avatar'],
                ];

                $additionalInfo = $this->validate($contactData);

                //need to preserve contact's id
                $contactData['id'] = $id;

                if (empty($additionalInfo)) {
                    Contacts::editContactById($id, $contactData);

                    //add redirect
                    $this->redirectToPage('/');
                }

            } else {
                $contactData = Contacts::getContactById($id);
            }

            $additionalInfo['type'] = 'edit';
            $this->view->render('FormAddEdit.php', $contactData, $additionalInfo);
            return true;

        } else {

            $this->redirectToPage(ERROR_404_PAGE_PATH);

        }

    }

	public function actionSearch($fieldName = 'firstName', $fieldValue = '', $pageNumb = 1, $sortingField = 'contactId', $sortingOrder = 'asc')
	{

		if ($fieldValue != '') {

            if ($sortingField != 'contactId') {
                $useSorting = 'true';
            } elseif (isset($_POST['useSorting'])){
                $useSorting = $_POST['useSorting'];
            } else {
                $useSorting = 'false';
            }

            //validation of searched column name and value
            $data = ['fieldName' => $fieldName, $fieldName => $fieldValue];
			$errors = $this->validate($data);

            if (empty($errors)) {
                //gets searched data and number of found rows
                $searchData = Contacts::getContactsSearch($fieldName, $fieldValue, $pageNumb, $sortingField, $sortingOrder);

                $contactsList = $searchData['data'];
                $totalPages = ceil($searchData['rowsNumber'] / RECORDS_PER_PAGE);

                $data = $this->createContactsTable($contactsList);

                //got data to search and no error during validation
                $this->view->render('Search.php', $data, [
                                                        'useSorting'=> $useSorting,
                                                        'searchField' => $fieldName,
                                                        'searchValue' => $fieldValue,
                                                        'totalPages' => $totalPages,
                                                        'sortingField' => $sortingField,
                                                        'sortingOrder' => $sortingOrder]);

                return true;

            } else {

                $this->view->render('Search.php', null, [
                                                        'errors' => $errors[$fieldName],
                                                        'searchField' => $fieldName,
                                                        'searchValue' => $fieldValue,
                                                        'useSorting'=> $useSorting,
                                                        'sortingField' => $sortingField,
                                                        'sortingOrder' => $sortingOrder]);

                return true;
            }

        //empty fieldValue
		} else {

            //if it was send via POST -> actionSearch with params
			if (isset($_POST['searchFor'])) {

				if ($_POST['searchValue'] !== '') {
                    $searchValue = urlencode($_POST['searchValue']);
                    if ($_POST['useSorting'] == 'true') {
                        $this->redirectToPage("/contacts/search/" . $_POST['searchField'] . "/" . $searchValue . "/" . $pageNumb . "/" . $_POST['sortingField'] . "/" . $_POST['sortingOrder']);
                    } else {
                        $this->redirectToPage("/contacts/search/" . $_POST['searchField'] . "/" . $searchValue);
                    }

				}

                //no search value
				$this->view->render('Search.php', null, [
                                                        'errors' => '<span class="error">This field cannot be empty!</span>',
                                                        'searchField' => $_POST['searchField'],
                                                        'searchValue' => '',
                                                        'useSorting' => $_POST['useSorting'],
                                                        'sortingField' => $_POST['sortingField'],
                                                        'sortingOrder' => $_POST['sortingOrder'],]);
			}

            //first visit
			$this->view->render('Search.php', null, [
                                                    'searchField' => $fieldName,
                                                    'searchValue' => '',
                                                    'useSorting' => 'false',
                                                    'sortingField' => $sortingField,
                                                    'sortingOrder' => $sortingOrder]);

			return true;
		}


	}








    public function actionDelete($id, $deleting = false)
    {
        if (preg_match('/[0-9]+/', $id)) {

            if ($deleting) {

                Contacts::deleteContactById($id);

                //add redirect
                $this->redirectToPage('/');

            }
            //get contacts data
            $contactsData = Contacts::getContactById($id);

            if ($contactsData === false) {
                $this->redirectToPage(ERROR_404_PAGE_PATH);
            }

            $this->view->render('Delete.php', $contactsData);

            return true;

        } else {

            $this->redirectToPage(ERROR_404_PAGE_PATH);

        }

    }

    private function redirectToPage($page)
    {
        header("Location: $page");
        die();
    }

	//checks form input
	private function validate(&$data)
	{
		$errors = [];

		foreach ($data as $key=>$value) {

			$rule = $this->rules[$key];

			if ($value == null) {

				if ($rule['required']) {
					$errors[$key] = $rule['messages']['requiredError'];
				}

			} else {

				switch ($rule['type']) {

					case 'regex':

						if ($rule['parameters']['minLength'] !== null && $rule['parameters']['maxLength'] !== null) { //length is checked not with regex
							if (!(strlen($value) >= $rule['parameters']['minLength'] && strlen($value) <= $rule['parameters']['maxLength'])) {
								$errors[$key] = $rule['messages']['lengthError'];
							}
						}

						if (!preg_match($rule['parameters']['pattern'], $value)) {
							$errors[$key] = $rule['messages']['symbolsError'];
						}

						break;

					case 'regexBirthday':


						foreach ($rule['parameters']['delimitersToChange'] as $delimiter) {
							$value = str_replace($delimiter, '-', $value);
						}

						$data[$key] = $value;


						if (!preg_match($rule['parameters']['pattern'], $value)) {
							$errors[$key] = $rule['messages']['symbolsError'];
						}

						break;

					case 'filterVar':

						if (filter_var($value, $rule['parameters']['filter']) === false) {
							$errors[$key] = $rule['messages']['filterError'];
						}

						break;

					case 'imageCheck':

						if ($data['avatar']['name'] == '' || $data['avatar'] == '/img/avatars/default.jpeg' ) {

							$data['avatar'] = '/img/avatars/default.jpeg';

						} else {

							$targetFile = '/img/avatars/' . basename($data['avatar']['name']);

							$uploadOk = true;

							$size = getimagesize($data['avatar']['tmp_name']);

							if ($size === false) {

								$errors[$key] = $rule['messages']['typeError'];
								$uploadOk = false;

							} else {

								$imageFileType = pathinfo($targetFile, PATHINFO_EXTENSION);

								//check for all available types
								foreach ($rule['parameters']['types'] as $type) {
									if ($imageFileType === $type) {
										$typeIsOk = true;
										break;
									}
								}

								//if there wasnt a type match - there is an error
								if ($typeIsOk !== true) {
									$errors[$key] = $rule['messages']['typeError'];
									$uploadOk = false;
								}

							}


							//2mb limit
							if ($data['avatar']['size'] > 2 * pow(2, 20) && $uploadOk) {
								$errors[$key] = $rule['messages']['sizeError'];
								$uploadOk = false;
							}


							if ($uploadOk) {
								if (move_uploaded_file($data['avatar']['tmp_name'], ROOT . $targetFile)) {
									$data['avatar'] = $targetFile;
								} else {
									$errors[$key] = $rule['messages']['uploadError'];
								}
							}

						}

						break;
				}
			}
		}

		return $errors;
	}

	private function createContactsTable($data)
	{
		$table = [];

		$table[] = " <table class='usersTable'>
                        <tr>
                                <td>Avatar</td>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Mobile phone</td>
                                <td>Home phone</td>
                                <td>Work phone</td>
                                <td>Address</td>
                                <td>City</td>
                                <td>State</td>
                                <td>Zip code</td>
                                <td>Birthday</td>
                                <td>Options</td>
                        </tr>";

		foreach ($data as $contact) {

			//img src must be reviewed
			$table[] = '<tr>
                			<td><img class="avatar" src="' . $contact['avatar'] .'"></td>
                			<td>' . $contact['name'] . '</td>
                			<td>' . $contact['email'] . '</td>
                			<td>' . $contact['mobPhone'] . '</td>
                			<td>' . $contact['homePhone'] . '</td>
                			<td>' . $contact['workPhone'] . '</td>
                			<td>' . $contact['address'] . '</td>
                			<td>' . $contact['city'] . '</td>
                			<td>' . $contact['state'] . '</td>
                			<td>' . $contact['zip'] . '</td>
                			<td>' . $contact['birthday'] . '</td>
                			<td>    <a href="/contacts/edit/' . $contact['id'] . '">edit</a> / <a href="/contacts/delete/' . $contact['id'] . '">delete</a></td>
                		</tr>';

		}

		$table[] = "</table>";

		return $table;
	}

}