<!doctype HTML>

<html>
<head>
</head>
<body>

<?php
require_once('args_concat_improved_functions.php');


echo "Arguments concatenation: ".concat("a", "b", "sdasd"," sad ","2134ASD", "dd dsd sad asd as d")."</br>";

echo "Arguments concatenation: ".concatFirstUpper("a", "b", "sdasd"," sad ","2134ASD", "dd dsd sad asd as d")."</br>";

echo "All upper case: " . concatCapitals("a", "b", "sdasd"," sad ","2134ASD", "dd dsd sad asd as d")."</br>";

echo "All words with capital: " . concatWords("a", "b", "sdasd"," sad ","2134ASD", "dd dsd sad asd as d")."</br>";

echo "All words with capital (ucwords): " . concatUCWords("a", "b", "sdasd"," sad ","2134ASD", "dd dsd sad asd as d")."</br>";

?>

</body>
</html>
