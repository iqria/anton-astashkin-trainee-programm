<?php

return [

    'fullName' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[A-Z][-A-Za-z\' .]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters and apostrophe for first and last name! Names must start with a capital letter!</span>',
            'lengthError' => '<span class="error">Invalid name length! Please, use not more than 50 characters and enter both Your first and last name!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'email' => [
        'type' => 'filter_var',
        'required' => true,

        'parameters' => [
            'filter' => FILTER_VALIDATE_EMAIL
        ],

        'messages' => [
            'filterError' => '<span class="error">Please, enter a valid email!</span>'
        ]
    ],


    'mobPhone' => [
        'type' => 'regex',

        'parameters' => [
            'pattern' => '/^[0-9-() ]{6,16}$/'
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only digits and - ( ) characters!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>'
        ],

    ],


    'homePhone' => [
        'type' => 'regex',

        'parameters' => [
            'pattern' => '/^[0-9-() ]{6,16}$/'
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only digits and - ( ) characters!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>'
        ],

    ],


    'workPhone' => [
        'type' => 'regex',

        'parameters' => [
            'pattern' => '/^[0-9-() ]{6,16}$/'
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only digits and - ( ) characters!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>'
        ],

    ],


    'address' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[A-Za-z0-9 .,#-]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters and apostrophe!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'city' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[-A-Za-z\' ]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters and apostrophe!</span>',
            'lengthError' => '<span class="error">Invalid length!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],



    'state' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[-A-Za-z\' ()]+$/',
            'minLength' => 3,
            'maxLength' => 50
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Please, use only letters, space and these symbols: \' ( )!</span>',
            'lengthError' => '<span class="error">Invalid length! Please, use not more than 50 characters!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'zip' => [
        'type' => 'regex',
        'required' => true,

        'parameters' => [
            'pattern' => '/^[0-9A-Z -]{5,16}$/',
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Invalid zip code!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],


    'birthday' => [
        'type' => 'regexBirthday',
        'required' => true,

        'parameters' => [
            'pattern' => '/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/',
            'delimitersToChange' => [
                '-',
                ' ',
                '\\',
                '/',
                '.',
            ]
        ],

        'messages' => [
            'symbolsError' => '<span class="error">Invalid data! Please, use yyyy-mm-dd format!</span>',
            'requiredError' => '<span class="error">This field is required!</span>',
        ]
    ],

    'avatar' => [
        'type' => 'imageCheck',
        'parameters' => [
            'jpg',
            'jpeg',
            'png',
            'gif',
        ],

        'messages' => [
            'uploadError' => '<span class="error">Sorry, Your file was not uploaded due to an error.</span>',
            'typeError' => '<span class="error">File is not an image.</span>',
            'extError' => '<span class="error">Sorry, only JPG, JPEG, PNG & GIF files are allowed.</span>',
            'sizeError' => '<span class="error">Sorry, Your file is too large.</span>'
        ]
    ]
];