<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $accessClients = $auth->createPermission('accessClients');
        $accessClients->description = 'User can access his/her clients data';

        $editUser = $auth->createPermission('editUser');
        $editUser->description = 'User can edit own profile';


        $accessUsers = $auth->createPermission('accessAllUsers');
        $accessUsers->description = 'Admin can access users data';



        $auth->add($accessClients);
        $auth->add($editUser);
        $auth->add($accessUsers);


        $user = $auth->createRole('user');
        $admin = $auth->createRole('admin');


        $auth->add($user);
        $auth->add($admin);


        $auth->addChild($user, $accessClients);
        $auth->addChild($user, $editUser);

        $auth->addChild($admin, $accessUsers);
        $auth->addChild($admin, $user);

    }
}