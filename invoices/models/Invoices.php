<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "invoices".
 *
 * @property string $invoiceId
 * @property string $userId
 * @property string $number
 * @property string $clientId
 * @property string $date
 * @property string $statusId
 * @property string $addressId
 * @property double $total
 */
class Invoices extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'date'], 'required'],

            [['date'], 'safe'],

            [['number'], 'string', 'max' => 32],

            ['number', 'unique', 'message' => 'Invoice with such number already exists!'],

            ['number', 'match', 'pattern' => '/^\d+$/'],

            //for now
            ['total', 'number', 'integerOnly' => false, 'min' => 0],

            [['userId', 'clientId', 'statusId', 'addressId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoiceId' => 'Invoice ID',
            'number' => 'Number',
            'clientId' => 'Client ID',
            'date' => 'Date',
            'statusId' => 'Status ID',
            'addressId' => 'Address ID',
            'total' => 'Total',
        ];
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['statusId' => 'statusId']);
    }

    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['clientId' => 'clientId']);
    }

    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['addressId' => 'addressId']);
    }


    public static function countUsersInvoices($userId)
    {
        return Invoices::find()->where(['userId' => $userId])->count();
    }


    public static function getSmallestUnusedId()
    {
        $i = 0;

        do {

            $i++;
            $invoice = Invoices::findOne(['number' => $i]);

        } while ($invoice != null);

        return $i;
    }



    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {
                $this->statusId = Status::STATUS_NEW;
            }

            return true;
        }

        return false;

    }
}
