<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $userId
 * @property string $username
 * @property string $email
 * @property integer $status
 * @property string $authKey
 * @property string $password
 * @property string $statusId
 *
 * @property Status $statusId0
 */
class User extends ActiveRecord implements IdentityInterface
{


    public $passwordConfirm = false;

    public static function tableName()
    {
        return 'user';
    }



    public function rules()
    {
        return [

            [['username', 'email', 'password'], 'required'],

            ['username', 'unique', 'message' => 'Sorry, this username already exists!'],

            ['email', 'unique', 'message' => 'Sorry, this email already exists!'],

            ['username', 'string', 'length' => [6, 32]],

            ['username', 'match', 'pattern' => '/^[a-zA-Z0-9_]+$/', 'message' => 'Please, use only letters, digits and underscore!'],

            ['email', 'email'],

            ['statusId', 'match', 'pattern' => '/(10|20|30)/'],

            [['username', 'authKey'], 'string', 'max' => 32],

            ['password', 'match', 'pattern' => '/^[!?@$%&*()_\-\#A-Za-z0-9]+$/', 'message' => 'Please, use only letters, digits and this special symbols: !, ?, @, $, %, &, *, (, ), _, -, # !'],

            ['password', 'match', 'pattern' => '/[a-zA-Z]/', 'message' => 'Please, use at least 1 letter!'],

            ['password', 'match', 'pattern' => '/[0-9]/', 'message' => 'Please, use at least 1 digit!'],

            ['password', 'match', 'pattern' => '/[!?@$%&*()_\-\#]/', 'message' => 'Please, use at least 1 special character (!, ?, @, $, %, &, *, (, ), _, -, #, \, /)!'],

            //need to fix a problem when a valid password was entered, passwordConfirm matches it, and than password was altered
            //passwordConfirm remains glowing green
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password'],

            ['authKey', 'safe'],


        ];
    }



    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'username' => 'Username',
            'email' => 'Email',
            'authKey' => 'Auth Key',
        ];
    }


    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['statusId' => 'statusId']);
    }

    public function getRole()
    {
        return $this->hasOne(AuthAssignment::className(), ['userId' => 'user_id']);
    }



    public static function findIdentity($id)
    {
        return static::findOne($id);
    }



    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }


    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function getPasswordHashByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }



    public function getId()
    {
        return $this->userId;
    }


    public function validatePassword($password)
    {
        return password_verify($password, $this->password);
    }



    public function getAuthKey()
    {
        return $this->authKey;
    }



    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->authKey = \Yii::$app->security->generateRandomString();
                if (!isset($this->statusId)) {
                    $this->statusId = Status::STATUS_ACTIVE;
                }
            }
            $this->password = password_hash($this->password, PASSWORD_DEFAULT);

            return true;
        }
        return false;

    }

    public function changeStatus($newStatus)
    {
        echo "<br><br><br><br>";
        var_dump($newStatus);
        $this->statusId = $newStatus;
    }
}
