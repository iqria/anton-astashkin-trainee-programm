<?php


class Contacts
{
    //WHERE userId=$_SESSION['id']
    public static function getContactsList($pageNumb, $sortingField = 'noSorting', $sortingOrder)
    {
        $db = Db::getConnection();

        if ($sortingField !== 'noSorting') {
            $sort = " ORDER BY $sortingField $sortingOrder";
        } else {
            $sort = "";
        }

        //admin with id = 0 sees all contacts
        if ($_SESSION['id'] == 1) {
            $userIdCheck = '';
        } else {
            $userIdCheck = " WHERE userId='" . $_SESSION['id'] . "'";
        }

        $query = "SELECT * FROM " . FULL_TABLE_NAME_CONTACTS . $userIdCheck . $sort . " LIMIT " . ($pageNumb - 1) * RECORDS_PER_PAGE . ", " . RECORDS_PER_PAGE;
        $result = $db->query($query);

        return Contacts::fetchResult($result);

    }

    //AND userId=$_SESSION['id']
    public static function getContactById($id)
    {
        $db = Db::getConnection();

        if ($_SESSION['id'] == 1) {
            $userIdCheck = '';
        } else {
            $userIdCheck = " AND userId='" . $_SESSION['id'] . "'";
        }

        $result = $db->query("SELECT * FROM " . FULL_TABLE_NAME_CONTACTS . " WHERE contactId = $id $userIdCheck;");


        if ($result !== false) {

            return Contacts::fetchResult($result)[0];

        } else {

            return false;
        }

    }

    //AND userId=$_SESSION['id']
    public static function getNumberOfRows()
    {
        $db = Db::getConnection();
        $numb = $db->query("SELECT COUNT(*) FROM " . FULL_TABLE_NAME_CONTACTS . " WHERE userId='" . $_SESSION['id'] . "';")->fetch()[0];
        
        return ($numb);
    }

    //userId=$_SESSION['id']
    public static function addContact($userData)
	{

		$db = Db::getConnection();

        $query = "INSERT INTO " . FULL_TABLE_NAME_CONTACTS . " VALUES (NULL, " . $_SESSION['id'] .", ";

        foreach ($userData as $column=>$value) {
            $query .= "'$value', ";
        }

        //need to cut last coma
        $query = substr($query, 0, strrpos($query, ','));
        $query .= ");";

		$db->query($query);

    }

    //AND userId=$_SESSION['id']
    public static function getContactsSearch($fieldName, $fieldValue, $pageNumb, $sortingField, $sortingOrder)
    {
        if ($sortingField !== null) {
            $sort = " ORDER BY $sortingField $sortingOrder";
        } else {
            $sort = "";
        }

        $db = Db::getConnection();
        $result = $db->query("SELECT * FROM " . FULL_TABLE_NAME_CONTACTS . " WHERE $fieldName='$fieldValue' AND userId='" . $_SESSION['id'] ."' $sort LIMIT " . ($pageNumb - 1) * RECORDS_PER_PAGE . ", " . RECORDS_PER_PAGE . ";");
        $contactsList = Contacts::fetchResult($result);

        return ['data' => $contactsList, 'rowsNumber'  => count($contactsList)];
    }

    //AND userId=$_SESSION['id']
	public static function editContactById($id, $data)
	{

		unset($data['id']);

		if ($data['avatar'] == '/img/avatars/default.jpeg') {
			unset($data['avatar']);
		}

		$query = "UPDATE " . FULL_TABLE_NAME_CONTACTS . " SET ";

		foreach ($data as $column=>$value) {
			$query .= "$column ='$value', ";
		}

		//need to cut last coma
		$query = substr($query, 0, strrpos($query, ','));

		$query .= " WHERE contactId = $id AND userId='" . $_SESSION['id'] ."'";

        $db = Db::getConnection();
        $db->query($query);
	}

    //AND userId=$_SESSION['id']
    public static function deleteContactById($id)
    {
        $db = Db::getConnection();
        $db->query("DELETE FROM " . FULL_TABLE_NAME_CONTACTS . " WHERE contactId = $id AND userId='" . $_SESSION['id'] ."';");

    }



	public static function fetchResult($result)
	{

		$contactsList = [];

        $i = 0;

        if ($row = $result->fetch()) {

            do {

                $contactsList[$i]['id'] = $row['contactId'];
                $contactsList[$i]['name'] = $row['firstName'] . ' ' . $row['lastName'];
                $contactsList[$i]['firstName'] = $row['firstName'];
                $contactsList[$i]['lastName'] = $row['lastName'];
                $contactsList[$i]['email'] = $row['email'];
                $contactsList[$i]['mobPhone'] = $row['mobPhone'];
                $contactsList[$i]['homePhone'] = $row['homePhone'];
                $contactsList[$i]['workPhone'] = $row['workPhone'];
                $contactsList[$i]['address'] = $row['address'];
                $contactsList[$i]['city'] = $row['city'];
                $contactsList[$i]['state'] = $row['state'];
                $contactsList[$i]['zip'] = $row['zip'];
                $contactsList[$i]['birthday'] = $row['birthday'];
                $contactsList[$i]['avatar'] = $row['avatar'];
                $i++;

            } while ($row = $result->fetch());

        } else {

            $contactsList[$i]['id'] =  '-';
            $contactsList[$i]['name'] = '-';
            $contactsList[$i]['firstName'] = '-';
            $contactsList[$i]['lastName'] = '-';
            $contactsList[$i]['email'] =  '-';
            $contactsList[$i]['mobPhone'] =  '-';
            $contactsList[$i]['homePhone'] =  '-';
            $contactsList[$i]['workPhone'] =  '-';
            $contactsList[$i]['address'] =  '-';
            $contactsList[$i]['city'] =  '-';
            $contactsList[$i]['state'] =  '-';
            $contactsList[$i]['zip'] = '-';
            $contactsList[$i]['birthday'] =  '-';
            $contactsList[$i]['avatar'] =  '/img/avatars/notFound.jpg';

        }

		return $contactsList;
	}
}
