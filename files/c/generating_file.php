<!doctype HTML>
<html>
<head>
</head>
<body>

<?php
require_once('generating_file_functions.php');

$filePath = 'file.txt';
$file = fopen($filePath, 'w+');

generateFileContent($file);
echo "Generated<br>";

$a = nl2br(file_get_contents('file.txt'));
echo $a. "<br>";

bubbleFile($file);
echo "Bubbled<br>";

$a = nl2br(file_get_contents('file.txt'));
echo $a;

fclose($file);

?>

</body>
</html>
