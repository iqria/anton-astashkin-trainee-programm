<?php

define('ALLOWED_FIELDS_CONTACTS', '(firstName|lastName|email|mobPhone|homePhone|workPhone|state|zip)');
define('ALLOWED_FIELDS_USERS', '(login|email)');
define('SORTING_ORDER', '(asc|desc)');
define('CONTROLLER_NAME_TO_WORK_WITH_DATA', '(contacts|admin)');
return [

	//view
	'^$' => 'contacts/index',

    '^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '$' => '$1/index',

    '^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '/([0-9]+)$' => '$1/index/$2',

	'^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '/([0-9]+)/' . ALLOWED_FIELDS_CONTACTS .'/' . SORTING_ORDER .'$' => '$1/index/$2/$3/$4',

	//add
	'^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '/add$' => '$1/add',

	//edit
	'^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '/edit$' => '$1/edit/null',

	'^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '/edit/([0-9]+)$' => '$1/edit/$2',

	//delete
	'^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '/delete/([0-9]+)$' => '$1/delete/$2',

	'^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '/delete/([0-9]+)/(true|false)$' => '$1/delete/$2/$3',


	//search
	'^' . CONTROLLER_NAME_TO_WORK_WITH_DATA . '/search$' => '$1/search',

	//need to replace regex for search field and search value
	'^contacts/search/' . ALLOWED_FIELDS_CONTACTS . '/([a-zA-Z0-9\-@%.]+)$' => 'contacts/search/$1/$2',

	'^admin/search/' . ALLOWED_FIELDS_USERS . '/([a-zA-Z0-9\-@%.]+)$' => 'admin/search/$1/$2',

	'^contacts/search/' . ALLOWED_FIELDS_CONTACTS . '/([a-zA-Z0-9\-@%.]+)/([0-9]+)$' => 'contacts/search/$1/$2/$3',

	'^admin/search/' . ALLOWED_FIELDS_USERS . '/([a-zA-Z0-9\-@%.]+)/([0-9]+)$' => 'admin/search/$1/$2/$3',

	'^contacts/search/' . ALLOWED_FIELDS_CONTACTS . '/([a-zA-Z0-9\-@%.]+)/([0-9]+)/' . ALLOWED_FIELDS_CONTACTS .'/' . SORTING_ORDER .'$' => 'contacts/search/$1/$2/$3/$4/$5',

    '^admin/search/' . ALLOWED_FIELDS_USERS . '/([a-zA-Z0-9\-@%.]+)/([0-9]+)/' . ALLOWED_FIELDS_USERS .'/' . SORTING_ORDER .'$' => 'admin/search/$1/$2/$3/$4/$5',




	'^users/login$' => 'users/login',

	'^users/login/([A-Za-z0-9_]+)$' => 'users/login/$1',

	'^users/register$' => 'users/register',

	'^users/logout$' => 'users/logout',

	//errors
	'^error404$' => 'error404/index',

	'(.*)' => 'index',

];