
<?php

function printStructure($data, $spaces = "")
{

    $output ="";

    if (!(is_array($data))) {

        return $data . "<br>";

    } else {
        $output .= "[</br>";
        foreach ($data as $key=>$element) {
            $output .= $spaces . "$key => " . printStructure($element, $spaces . "    ");
        }
        $output .= "$spaces]"."<br>";
        return $output;

    }
}

function renderMenu($data)
{
    echo "<pre>" . printStructure($data) . "</pre>";
}