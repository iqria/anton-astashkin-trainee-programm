-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 04, 2016 at 06:06 PM
-- Server version: 5.6.28-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoices`
--
CREATE DATABASE IF NOT EXISTS `invoices` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `invoices`;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `addressId` int(10) unsigned NOT NULL,
  `addressLine1` varchar(255) NOT NULL,
  `addressLine2` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`addressId`, `addressLine1`, `addressLine2`, `country`, `state`, `city`, `zip`) VALUES
(1, 'qwe', 'qwe', 'qwe', 'qwe', 'qw', '324'),
(4, 'qwe', 'qwe', 'qwe', 'qwe', 'qw', '324'),
(5, 'qeqwr', 'qwrqwrq', 'rqwrqwr', 'qwrqwr', 'wrqwrqw', 'qwr'),
(6, 'qwrqwrqw', 'rqwrqwr', 'qwrqwrqwr', 'qwrqwrqwr', 'qwrqwr', 'qwrqwr'),
(7, 'qwrqwrqw', 'rqwrqwr', 'qwrqwrqwr', 'qwrqwrqwr', 'qwrqwr', 'qwrqwr'),
(8, 'qwrqwrqw', 'rqwrqwr', 'qwrqwrqwr', 'qwrqwrqwr', 'qwrqwr', 'qwrqwr'),
(9, 'qwrqwrqw', 'rqwrqwr', 'qwrqwrqwr', 'qwrqwrqwr', 'qwrqwr', 'qwrqwr1'),
(10, 'qwrqwrqw', 'rqwrqwr', 'qwrqwrqwr', 'qwrqwr22qwr', 'qwrqwr', 'qwrqwr1'),
(11, 'qwrqwrqw', 'rqwrqwr', 'qwrqwrqwr', 'qwrqwr22qwr', 'qwrqwr123', 'qwrqwr1'),
(12, 'qwrqwrqw', 'rqwrqwr', 'qwrqwrqwr', 'qwrqwr22qwr', 'qwrqwr', 'qwrqwr');

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '19', 1454587668),
('user', '10', 1454508022),
('user', '11', 1454508339),
('user', '13', 1454509875),
('user', '14', 1454587148),
('user', '15', 1454587176),
('user', '16', 1454587208),
('user', '17', 1454587212),
('user', '18', 1454587590),
('user', '2', 1454503576),
('user', '3', 1454503769),
('user', '4', 1454503887),
('user', '5', 1454503974),
('user', '6', 1454504207),
('user', '7', 1454504263),
('user', '8', 1454504467),
('user', '9', 1454504807);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('accessAllUsers', 2, 'Admin can access users data', NULL, NULL, 1454502518, 1454502518),
('accessClients', 2, 'User can access his/her clients data', NULL, NULL, 1454502518, 1454502518),
('admin', 1, NULL, NULL, NULL, 1454502518, 1454502518),
('editUser', 2, 'User can edit own profile', NULL, NULL, 1454502518, 1454502518),
('user', 1, NULL, NULL, NULL, 1454502518, 1454502518);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'accessAllUsers'),
('user', 'accessClients'),
('user', 'editUser'),
('admin', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `clientId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `organization` varchar(255) NOT NULL,
  `addressId` int(10) unsigned NOT NULL,
  `lastInvoice` date NOT NULL,
  `totalSpentCount` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`clientId`, `userId`, `firstName`, `lastName`, `phone`, `email`, `organization`, `addressId`, `lastInvoice`, `totalSpentCount`) VALUES
(3, 19, 'rtyrty', 'rtyrty', '234234', 'ryrty@qwe.qwe', 'qwe', 3, '0000-00-00', 0),
(4, 19, 'rtyrty', 'rtyrty', '234234', 'ryrty@qwe.qwe', 'qwe', 4, '0000-00-00', 0),
(5, 19, 'qweqwr', 'qwrwqr', '234234', 'qwrqwr@qwe.qwe', 'wrqwrqwr', 5, '0000-00-00', 0),
(6, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 6, '0000-00-00', 0),
(7, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 7, '0000-00-00', 0),
(8, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 8, '0000-00-00', 0),
(9, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 6, '0000-00-00', 0),
(10, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 6, '0000-00-00', 0),
(11, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 9, '0000-00-00', 0),
(12, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 9, '0000-00-00', 0),
(13, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 10, '0000-00-00', 0),
(14, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 11, '0000-00-00', 0),
(15, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 10, '0000-00-00', 0),
(16, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 12, '0000-00-00', 0),
(17, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 10, '0000-00-00', 0),
(18, 19, 'tgujrtujrtu', 'rturturt', '345345', 'urturtu@wqr.fgydfg', 'erterte', 10, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1454501886),
('m140506_102106_rbac_init', 1454502296);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userId` int(10) unsigned NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `authKey` varchar(64) DEFAULT NULL,
  `statusId` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `avatar`, `firstName`, `lastName`, `phone`, `email`, `password`, `authKey`, `statusId`) VALUES
(6, 'img/avatar/default.png', 'rtuyrtu', 'eruyreyt', '', 'qwe123@wqe.qwe', '$2y$10$EDvX7B.iXnWZD..lQvW3DOWMmk18enx..K5xmuX8iYkPN.g2zMxri', 'xpzL1kTTmq-ryNyGoY-ctQONKT2pxSuD', 10),
(7, 'img/avatar/default.png', 'qweqwe', 'qweqwe', '', 'qwqwee123@wqe.qwe', '$2y$10$Fnj4kTqbUMnL9dRigKm54eUYKO21Eth4SVMPWOVOwHV7oWmeH2/7y', 'pqIt16p_yrK7wrVeEjJB6ZBq5S1gywbN', 10),
(8, 'img/avatar/856e9c1d39a6f2a7b509499e93e65897.jpg', 'tyrtyqeqwetryrtytry', 'ertyetyer', '234234', 'wer@fgjf.ghfg', '$2y$10$KRD13Xgw.Cx.IHtj7Fhf0euowiPwWIPEScgWYCWH0Mk3hfNdgoILO', 'xTve-UMaHyfgcCi4TUJQ9GhvGJUFGuIn', 10),
(9, 'img/avatar/f2fb18fe224844b8c0c37dcfd4eb0c9a.jpeg', 'lolkin', 'pypkin', '5345345', 'qwe123@wqe.qwe1', '$2y$10$fsilX8U2gulcrO1pfpIFMuuSc1VRld2NHFV6S/c0hcdOHx4Vh3Ar2', 'cUXEjoMr26YcdhztW_kYyXKQMwRcM8YJ', 10),
(10, 'img/avatar/dfc28a4d6d5314273be5200e664e3584.jpg', 'ertwrtye', 'ryweryery', '', 'eryery@qtqet.qt', '$2y$10$4l0yS3.loBsCbdHETr9PL.ydZFc9RjupHxWBiLDGIgj05acLofwi6', 'sogHk1I12sI4mE_qywTrjHTVLWQLDrZ1', 10),
(11, 'img/avatar/a2f3d1c146b2d4f070dc882f7df4f905.jpg', 'qweqwe', 'qweqwe', '', 'qwe@qwe.qwe', '$2y$10$YS5tY30tzURxRqT6rxXyIO2jG0sWKaUWex07JmmWI7.OB58RWIv4G', 'Rkpm91dQjnzEVVYZt3-cC5tMTz2_GmyF', 10),
(13, 'img/avatar/856e9c1d39a6f2a7b509499e93e65897.jpg', 'werwer', 'werwer', '', 'werwer@qweqwe.qweqw', '$2y$10$979XSShUdPFQ.x0aoslgDuQA5gjhDKPs6X6rdWEmVufvL8G5VDRYa', 'm5dBzFZiXaqx9yt9nxIniEF2jW5EWOUt', 10),
(19, 'img/avatar/856e9c1d39a6f2a7b509499e93e65897.jpg', 'egdfgd', 'fhdhd', '', 'q@w.e', '$2y$10$bG8Uwafeepmn9aF4KxycyurdKxji10QqUtLlF3GAkIyy4zzrtv9dm', 'c5KpVyCVC-yFowrB0ec170wZrqvn7del', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`addressId`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `addressId` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `clientId` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
