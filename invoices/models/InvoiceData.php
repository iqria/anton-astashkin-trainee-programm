<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "invoiceLines".
 *
 * @property string $invoiceLineId
 * @property string $invoiceId
 * @property string $name
 * @property string $description
 * @property double $price
 * @property integer $quantity
 * @property double $tax
 * @property double $total
 */
class InvoiceData extends Model
{
    public $dataArray = [];
}
