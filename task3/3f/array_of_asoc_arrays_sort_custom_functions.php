<?php
define('ASCENDING', 'ascending');
define('DESCENDING', 'descending');
define('MIN_VALUE', 5);
define('MAX_VALUE', 15);
define('MIN_LENGTH', 5);
define('MAX_LENGTH', 15);

function generate()
{

    $array = [];
    for ($i = 0; $i < mt_rand(MIN_LENGTH, MAX_LENGTH); $i++) {
        $array[$i] = ['priority'=>mt_rand(MIN_VALUE, MAX_VALUE)];
    }

    return $array;
}

function customSort($array, $order = ASCENDING)
{
    if ($order == ASCENDING) {
        $offset1 = 0;
        $offset2 = 1;
    } else {
        $offset1 = 1;
        $offset2 = 0;
    }

    $length = count($array);

    if ($length > 1) {

        do{
            $swaps = 0;
            for ($i = 0; $i < $length - 1; $i++) {
                if ($array[$i + $offset1]['priority'] > $array[$i + $offset2]['priority']) {
                    $temp = $array[$i]['priority'];
                    $array[$i]['priority'] = $array[$i + 1]['priority'];
                    $array[$i + 1]['priority'] = $temp;
                    $swaps++;
                }
            }
        } while($swaps != 0);


    }

    return $array;
}
