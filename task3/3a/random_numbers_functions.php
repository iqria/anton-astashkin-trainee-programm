<?php
define('MIN_VALUE', 5);
define('MAX_VALUE', 15);
define('MIN_LENGTH', 5);
define('MAX_LENGTH', 15);

function generateArray(){

    $array = [];
    $length = mt_rand(MIN_LENGTH, MAX_LENGTH);

    for($i = 0; $i < $length; $i++){
        $array[] = mt_rand(MIN_VALUE, MAX_VALUE);
    }

    return $array;
}

