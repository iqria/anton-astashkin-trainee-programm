<?php

class Db
{
	public static function getConnection()
	{
		$parametersPath = ROOT . '/config/dbParameters.php';
		$parameters = include($parametersPath);


		try {

			$dbConnection = new PDO("mysql:host=" . $parameters['host'] . ";dbname=" . $parameters['dbname'], $parameters['user'], $parameters['password']);
			return $dbConnection;

		} catch (PDOException $e) {

			echo 'Connection failed: ' . $e->getMessage();
			return false;

		}

	}
}