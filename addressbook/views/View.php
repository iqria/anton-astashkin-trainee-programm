<?php
class View
{
    function render($viewFile, $data = null, $additionalInfo = null)
    {
        $viewPath = ROOT . '/views/' . $viewFile;
        include_once($viewPath);
    }
}