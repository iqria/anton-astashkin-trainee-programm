<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "clients".
 *
 * @property string $clientId
 * @property string $firstName
 * @property string $lastName
 * @property string $phone
 * @property string $email
 * @property string $organization
 * @property string $addressId
 * @property string $lastInvoice
 * @property double $totalSpentCount
 */
class Clients extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'phone', 'email', 'organization'], 'required'],

            ['addressId', 'integer'],

            [['lastInvoice'], 'safe'],

            [['totalSpentCount'], 'number'],

            [['firstName', 'lastName', 'email', 'organization'], 'string', 'max' => 255],

            ['email', 'email'],

            ['phone', 'match', 'pattern' => '/[0-9]/', 'message' => 'Invalid phone number!'],

            ['phone', 'match', 'pattern' => '/^[0-9\-\(\)]{6,32}$/', 'message' => 'Please, use only digits, dashes and parenthesis!'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'clientId' => 'Client ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'phone' => 'Phone',
            'email' => 'E-mail',
            'organization' => 'Organization',
            'addressId' => 'Address ID',
            'lastInvoice' => 'Last Invoice',
            'totalSpentCount' => 'Total Spent Count',
        ];
    }


    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['statusId' => 'statusId']);
    }

    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['addressId' => 'addressId']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {
                    $this->statusId = Status::STATUS_ACTIVE;
            }

            return true;
        }

        return false;

    }



    public static function checkClientExists($clientModel)
    {
        //if exact same address already exists -> give its id
        if ($client = Clients::findOne([
            'firstName' => $clientModel->firstName,
            'lastName' => $clientModel->lastName,
            'phone' => $clientModel->phone,
            'email' => $clientModel->email,
            'organization' => $clientModel->organization,
        ])) {
            return $client->clientId;
        }
    }


}
