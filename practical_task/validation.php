<?php
define('AVATAR_DIR', 'avatars/');
define('DEFAULT_AVATAR', 'avatars/default.jpeg');

function validate(&$data, $rules)
{
    $errors = [];

    foreach ($data as $key=>$value) {

        $rule = $rules[$key];

        if ($value == null) {

            if ($rule['required']) {
                $errors[$key] = $rule['messages']['requiredError'];
            }

        } else {

            switch ($rule['type']) {

                case 'regex':

                    if ($rule['parameters']['minLength'] !== null && $rule['parameters']['maxLength'] !== null) { //length is checked not with regex
                        if (!(strlen($value) >= $rule['parameters']['minLength'] && strlen($value) <= $rule['parameters']['maxLength'])) {
                            $errors[$key] = $rule['messages']['lengthError'];
                        }
                    }

                    if (!preg_match($rule['parameters']['pattern'], $value)) {
                        $errors[$key] = $rule['messages']['symbolsError'];
                    }

                    break;

                case 'regexBirthday':


                    foreach ($rule['parameters']['delimitersToChange'] as $delimiter) {
                        $value = str_replace($delimiter, '-', $value);
                    }

                    $data[$key] = $value;


                    if (!preg_match($rule['parameters']['pattern'], $value)) {
                        $errors[$key] = $rule['messages']['symbolsError'];
                    }

                    break;

                case 'filter_var':

                    if (filter_var($value, $rule['parameters']['filter']) === false) {
                        $errors[$key] = $rule['messages']['filterError'];
                    }

                    break;

                case 'imageCheck':

                    if ($data['avatar']['name'] == '' || $data['avatar'] == DEFAULT_AVATAR ) {

                        $data['avatar'] = DEFAULT_AVATAR;

                    } else {

                        $targetFile = AVATAR_DIR . basename($data['avatar']['name']);

                        $uploadOk = true;

                        $size = getimagesize($data['avatar']['tmp_name']);

                        if ($size === false) {

                            $errors[$key] = $rule['messages']['typeError'];
                            $uploadOk = false;

                        } else {

                            $imageFileType = pathinfo($targetFile, PATHINFO_EXTENSION);

                            //check for all available types
                            foreach ($rule['parameters'] as $type) {
                                if ($imageFileType === $type) {
                                    $typeIsOk = true;
                                    break;
                                }
                            }

                            //if there wasnt a type match - there is an error
                            if ($typeIsOk !== true) {
                                $errors[$key] = $rule['messages']['typeError'];
                                $uploadOk = false;
                            }

                        }


                        //2mb limit
                        if ($data['avatar']['size'] > 2 * pow(2, 20) && $uploadOk) {
                            $errors[$key] = $rule['messages']['sizeError'];
                            $uploadOk = false;
                        }


                        if ($uploadOk) {
                            if (move_uploaded_file($data['avatar']['tmp_name'], $targetFile)) {
                                $data['avatar'] = $targetFile;
                            } else {
                                $errors[$key] = $rule['messages']['uploadError'];
                            }
                        }

                    }

                    break;
            }
        }
    }

    return $errors;
}




