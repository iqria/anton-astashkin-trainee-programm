<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<h1>Contact to delete:</h1>
<table class="contactsTable">
    <tr>
        <td>Name</td>
        <td><?= Html::encode("$contact->firstName $contact->lastName");?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?= Html::encode($contact->email)?></td>
    </tr>
    <tr>

    <td>Mobile Phone</td>
        <td><?= Html::encode($contact->mobilePhone);?></td>
    </tr>
    <tr>

    <td>Home Phone</td>
        <td><?= Html::encode($contact->homePhone);?></td>
    </tr>
    <tr>

    <td>Work Phone</td>
        <td><?= Html::encode($contact->workPhone);?></td>
    </tr>
    <tr>

    <td>Address</td>
        <td><?= Html::encode($contact->address);?></td>
    </tr>
    <tr>

    <td>City</td>
        <td><?= Html::encode($contact->city);?></td>
    </tr>
    <tr>

    <td>State</td>
        <td><?= Html::encode($contact->state);?></td>
    </tr>
    <tr>

    <td>Zip</td>
        <td><?= Html::encode($contact->zip);?></td>
    </tr>
    <tr>

    <td>Birthday</td>
        <td><?= Html::encode($contact->birthday);?></td>
    </tr>

</table>

<br>

<div class="form-group">
    <?php $form = ActiveForm::begin(); ?>
    <?= Html::submitButton('Delete', ['class' => 'btn btn-primary']) ?>

    <?= Html::button('Cancel', ['class' => 'btn btn-primary', 'onclick' => "window.location.href='/'"]) ?>
    <?php ActiveForm::end(); ?>
</div>
