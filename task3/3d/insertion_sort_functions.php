<?php
define('ASCENDING', 'ascending');
define('DESCENDING', 'descending');
define('MIN_VALUE', 5);
define('MAX_VALUE', 15);
define('MIN_LENGTH', 5);
define('MAX_LENGTH', 15);

function insertion($array, $order = ASCENDING){

    $length = count($array);

    if ($length > 0) {

        if ($order == DESCENDING){
            $offset1 = 0;
            $offset2 = -1;
        } else {
            $offset1 = -1;
            $offset2 = 0;
        }

        for ($inserted = 1; $inserted < $length; $inserted++) {

            $i = $inserted + $offset1;
            $j = $inserted + $offset2;

            while ($array[$i] > $array[$j] && ($j > -1 && $i > -1)) {

                $temp = $array[$i];
                $array[$i] = $array[$j];
                $array[$j] = $temp;

                $i--;
                $j--;
            }
        }
    }

    return $array;


}

