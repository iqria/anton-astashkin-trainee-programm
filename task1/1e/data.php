<?php
$data = [
    'region1' => [
        'subregion1' => [
            'subsubregion1' => 400,
            'subsubregion2' => 500
        ],
        'subregion2' => [
            'subsubregion1' => 50,
            'subsubregion2' => 80
        ],
    ],
    'region2' => [
        'subregion1' => [
            'subsubregion1' => 400,
            'subsubregion2' => 500
        ],
        'subregion2' => [
            'subsubregion1' => 50,
            'subsubregion2' => [
                'subsubsubregion1' => 400,
                'subsubsubregion2' => 900,
                'subsubsubregion3' => 800,
            ]
        ],
    ]
];
