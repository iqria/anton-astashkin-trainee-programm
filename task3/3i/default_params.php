<!doctype HTML>
<html>
<head>
</head>
<body>

<?php
require_once('default_params_functions.php');

$defaultConfig = [
    'par1' => 'par1',
    'par2' => 'par2',
    'par3' => 'par3',
    'par4' => 'par4',
];

$userConfig = [
    'par4' => 'newPar4',
    'par5' => 'newPar5',
];


print_r(applyConfig($userConfig, $defaultConfig));
?>


</body>
</html>
