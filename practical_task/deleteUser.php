<!doctype HTML>

<html>
<head>
    <style>
        .error {
            color: red;
            font-size: 0.8em;
        }
    </style>
</head>
<body>

<?php
define('DATA_FILE_PATH', 'data/dataFile.csv');
require_once('functionsEdit.php');

$file = fopen(DATA_FILE_PATH, 'r+');

//getting to needed user
$userNumb = $_GET['user'];

//userNumb must be preserved
{
    $userNumbLoop = $userNumb;
    while ($userNumbLoop > 1) {
        fgets($file);
        $userNumbLoop--;
    }
}


//grab users data
$dataToDelete = fgetcsv($file, null, '|');
fclose($file);

if ($_GET['delete']) {

    changeDataInFile(DATA_FILE_PATH, $dataToDelete);
    header("Location: index.php");
    die();

}

?>

<p>
    Are You sure You want to delete this user?
<ul>

    <li><label>Full Name</label>: <?= $dataToDelete[1] ?>
    <li><label>Email</label>: <?= $dataToDelete[2] ?>
    <li><label>Mobile phone</label>: <?= $dataToDelete[3] ?>
    <li><label>Home phone</label>: <?= $dataToDelete[4] ?>
    <li><label>Work phone</label>: <?= $dataToDelete[5] ?>
    <li><label>Address</label>: <?= $dataToDelete[6] ?>
    <li><label>City</label>: <?= $dataToDelete[7] ?>
    <li><label>State</label>: <?= $dataToDelete[8] ?>
    <li><label>Zip code</label>: <?= $dataToDelete[9] ?>
    <li><label>Birthday (yyyy-mm-dd)</label>: <?= $dataToDelete[10] ?>

</ul>
</p>

<form>
    <input type="button" value="Delete User"
           onclick="window.location.href='deleteUser.php?user=<?= $userNumb ?>&delete=true'">
    <input type="button" value="Cancel" onclick="window.location.href='index.php'">
</form>

</body>
</html>
