<?php

function generateTasks($numTasks, $maxTime)
{
    $tasks = [];

    for ($i = 0; $i < $numTasks; $i++) {
        $tasks[] = [
            'id' => $i + 1,
            'priority' => mt_rand(0, 100),
            'time' => mt_rand(1, $maxTime),
            'retries' => 0
        ];
    }
    return $tasks;
}

function executeTask(&$task)
{
    if(mt_rand(0, 4) == 4){
        $task['retries']++;
        return false;
    } else {
        return $task['time'] * ($task['retries'] + 1);
    }
}

function execute(&$tasks)
{
    $startTime = 0;
    $lastSortTime = 0;

    echo  "<table>
           <tr>
           <td>ID</td>
           <td>Priority</td>
           <td>Start time</td>
           <td>End Time</td>
           <td>Total time</td>
           <td>Retries</td></tr>";

    while (count($tasks) > 0) {
        $task = &$tasks[0];
        $executedTime = executeTask($task);

        if ($executedTime === false) {
            $tasks[] = array_shift($tasks);
            getResults($task, $startTime, $executedTime);
        } else {
            getResults($task, $startTime, $executedTime);

            array_shift($tasks);
        }


        if (($startTime - $lastSortTime) > 4 && count($tasks) > 0) {

            $lastSortTime = $startTime;

            $sortTime = microtime();
            sortTasks($task);
            $sortTime = microtime() - $sortTime;

            $startTime += $sortTime;

            echo    "<tr>
                     <td>-</td>
                     <td>*sorting*</td>
                     <td>$lastSortTime</td>
                     <td>$startTime</td>
                     <td>" . number_format($sortTime, 5) . "</td>
                     <td>-</td>
                     </tr>";
        }

    }
    echo "</table>";
}

 function getResults(&$task, &$startTime, $executedTime)
 {
     $task['start_time'] = $startTime;
     $startTime += $task['time'];
     $task['total_time'] = $executedTime;
     $task['end_time'] = $task['start_time'] + $task['total_time'];

     echo    "<tr>
              <td>" . $task['id'] . "</td>
              <td>" . $task['priority'] ."</td>
              <td>" . $task['start_time'] . "</td>
              <td>" . $task['end_time'] . "</td>
              <td>";
     echo ($task['total_time'] === false) ? "FAILED" : $executedTime;
     echo    "</td>
              <td>" . $task['retries'] . "</td>
              </tr>";
}

function sortTasks(&$tasks)
{
    usort($tasks, "sortByPriority");
    return $tasks;
}

function sortByPriority($a, $b)
{
    return $a['priority'] - $b['priority'];
}
