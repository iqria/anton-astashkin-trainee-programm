<?php

namespace app\controllers;

use app\models\Status;
use Yii;
use app\models\User;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class AdminController extends Controller
{
    public function actionIndex()
    {
        if (Yii::$app->user->can('admin')) {

            $query = User::find()->joinWith(['role']);

            $provider = new ActiveDataProvider([
                'sort' => false,
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],

            ]);

            return $this->render('index', ['dataProvider' => $provider, 'pagination' => $provider->getPagination()]);

        } else {

            throw new ForbiddenHttpException('Log in as admin to proceed!');

        }

    }

    public function actionChangeStatus($userId = -1)
    {
        if (Yii::$app->user->can('admin')) {

            if (!$user = User::findOne(['userId' => $userId])) {

                throw new NotFoundHttpException('User not found!');

            }

            $user->scenario = User::SCENARIO_CHANGE_STATUS;

            if ($user->statusId == Status::STATUS_ACTIVE) {

                $user->statusId = Status::STATUS_INACTIVE;

            } else {

                $user->statusId = Status::STATUS_ACTIVE;

            }

            $user->save();

            $this->redirect(Yii::$app->request->referrer);

        } else {

            throw new ForbiddenHttpException('Log in as admin to proceed!');

        }


    }


}