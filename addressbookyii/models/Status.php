<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "status".
 *
 * @property integer $statusId
 * @property string $statusName
 * @property string $statusValue
 *
 * @property User[] $users
 */
class Status extends ActiveRecord
{



    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 20;
    const STATUS_PENDING = 30;

    public static function getStatusList()
    {
        return [
            Status::STATUS_ACTIVE => 'active',
            Status::STATUS_INACTIVE => 'inactive',
            Status::STATUS_PENDING => 'pending',
        ];

    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    public static function getStatusName($statusId)
    {
        return static::findOne(['statusId' => $statusId])->statusName;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statusName'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'statusId' => 'Status ID',
            'statusName' => 'Status Name',
            'statusValue' => 'Status Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['status' => 'statusId']);
    }
}
