<!doctype HTML>

<html>
<head>
<style>

.highlighted {
    background-color: yellow;
}

</style>
</head>
<body>

<?php

if ($_POST['submit']) {

    require_once('search_functions.php');

    $dataMissing = [];

    if (isset($_POST['textarea'])) {
        $input = $_POST['textarea'];
    } else {
        $dataMissing[] = $_POST['textarea'];
    }

    if (isset($_POST['search'])) {
        $search = $_POST['search'];
    } else {
        $dataMissing[] = $_POST['search'];
    }

    if (isset($_POST['case'])) {
        $case = $_POST['case'];
    }

    if (isset($_POST['dir'])) {
        $dir = $_POST['dir'];
    } else {
        $dataMissing[] = $_POST['dir'];
    }


    if (isset($_POST['occurrences'])) {
        $occurrences = intval($_POST['occurrences']);

        if ($occurrences == 0) {
            $occurrences = -1;
        }

    }

    if (empty($dataMissing)) {

        $output = search($input, $search, $case, $dir, $occurrences);
    }

}


?>


<form action="<?=$_SERVER['SCRIPT_NAME']?>" method="post">

<textarea name="textarea"><?=(($input) ? $input :  'Enter text here');?></textarea>

</br>
</br>

Look for: <input type="text" name="search" value="<?=$search?>">

</br>
</br>

Amount of occurrences: <input type="text" name="occurrences" value="<?= (($occurrences < 0) ? 0 : $occurrences)?>">

</br>
</br>

<input type="hidden" name="case" value="sens">
<input type="checkbox" name="case" value="insens" <?php if ($case == CASE_INSENS) echo "checked";?>> Case-insensitive

</br>
</br>

<?php

if (($dir != DIR_LTR && $dir != DIR_RTL) || $dir == DIR_LTR) {
    $ltrchecked = "checked";
} else {
    $rtlchecked = "checked";
}

?>

<input type="radio" name="dir" value="ltr" <?=$ltrchecked?>> Left-to-right
<input type="radio" name="dir" value="rtl" <?=$rtlchecked?>> Right-to-left

</br>
</br>


<input type="submit" name="submit" value="Search!"></br>

</form>

<?="<p>Output: $output</p>";?>

</body>
</html>
