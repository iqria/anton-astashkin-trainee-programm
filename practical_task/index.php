<!doctype HTML>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <style>
        .usersTable {
            border-collapse: collapse;
        }

        .usersTable, th, td {
            border: 1px solid black;
        }

        td {
            padding: 2px;
        }

        .avatar {
            width: 50px;
            height: 25px;
        }
        </style>

</head>
<body>

<?php

require_once('functions.php');

if (file_exists(DATA_FILE_PATH)) {
    $file = fopen(DATA_FILE_PATH, 'r');
} else {
    $file = fopen(DATA_FILE_PATH, 'w+');
}

if ($_POST['submit']) {

}

?>

<!--add new user button-->
<form>
    <input type="button" value="Add new User" onclick="window.location.href='addUser.php'">
</form>
<br>

<form action="<?=$_SERVER['SCRIPT_NAME']?>" method="post">

    <input type="submit" name="submit" value="Search for:">


    <br>

    <select name="search">

        <?php
        if($_POST['submit']){
            switch ($_POST['search']){
                case 'fullName':
                    $searchByFullName = "selected";
                    break;
                case 'email':
                    $searchByEmail = "selected";
                    break;
                case 'mobPhone':
                    $searchByMobPhone = "selected";
                    break;
                case 'homePhone':
                    $searchByHomePhone = "selected";
                    break;
                case 'workPhone':
                    $searchByWorkPhone = "selected";
                    break;
                case 'state':
                    $searchByState = "selected";
                    break;
                case 'zip':
                    $searchByZip = "selected";
                    break;

            }
        }
        ?>


        <option value="fullName"    <?=$searchByFullName?>>Name</option>
        <option value="email"       <?=$searchByEmail?>>Email</option>
        <option value="mobPhone"    <?=$searchByMobPhone?>>Mobile Phone</option>
        <option value="homePhone"   <?=$searchByHomePhone?>>Home Phone</option>
        <option value="workPhone"   <?=$searchByWorkPhone?>>Work Phone</option>
        <option value="state"       <?=$searchByState?>>State</option>
        <option value="zip"         <?=$searchByZip?>>Zip code</option>
    </select>

    <input type="text" name="searchValue" value="<?=$_POST['searchValue']?>">

    <br>


    <label for="sort">Sort by:</label>
    <select name="sort" id="sort">

        <?php
        if($_POST['submit']){
            switch ($_POST['sort']){
                case 'null':
                    $sortByNull = "selected";
                    break;
                case 'fullName':
                    $sortByFullName = "selected";
                    break;
                case 'email':
                    $sortByEmail = "selected";
                    break;
                case 'mobPhone':
                    $sortByMobPhone = "selected";
                    break;
                case 'homePhone':
                    $sortByHomePhone = "selected";
                    break;
                case 'workPhone':
                    $sortByWorkPhone = "selected";
                    break;
                case 'state':
                    $sortByState = "selected";
                    break;
                case 'zip':
                    $sortByZip = "selected";
                    break;

            }
        }
        ?>

        <option value="null"        <?=$sortByNull?>>-</option>
        <option value="fullName"    <?=$sortByFullName?>>Name</option>
        <option value="email"       <?=$sortByEmail?>>Email</option>
        <option value="mobPhone"    <?=$sortByMobPhone?>>Mobile Phone</option>
        <option value="homePhone"   <?=$sortByHomePhone?>>Home Phone</option>
        <option value="workPhone"   <?=$sortByWorkPhone?>>Work Phone</option>
        <option value="state"       <?=$sortByState?>>State</option>
        <option value="zip"         <?=$sortByZip?>>Zip code</option>
    </select>

    <input type="hidden" name="order" value="ascending">
    <label for="order">Descending order:</label>
    <input type="checkbox" name="order" id="order" value="descending"
    <?php
        if ($_POST['order'] == DESCENDING) {
            echo 'checked';
        }
    ?>
    >

</form>
<br>


<?php

if ($_POST['submit'] && $_POST['searchValue'] != null) {

    $output = searchByField($file, $_POST['select'], $_POST['searchValue']);
    echo implode('', $output);

} else {

    $output = drawTable($file);

}

if ($_POST['submit'] && $_POST['sort'] != 'null') {

    sortRecords($output, $_POST['sort'], $_POST['order']);

}

echo implode('', $output);

?>


</body>
</html>
