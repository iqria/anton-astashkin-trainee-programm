<?php



class Users
{

    public static function checkCredentials($credentials)
    {

        $db = Db::getConnection();

        $query = "SELECT userId, password FROM " . FULL_TABLE_NAME_USERS;

        if (isset($credentials['login'])) {

            $query .= " WHERE (login ='". $credentials['login'];

        } else {

            $query .= " WHERE (email ='". $credentials['email'];

        }

        $query .= "');";

        $storedData = $db->query($query)->fetch();

        if (password_verify($credentials['password'], $storedData['password'])) {

            return $storedData['userId'];

        } else {

            return null;

        }

    }

    //assume $userData will have all needed key=>value pairs
    public static function registerNewUser($userData)
    {


        $userData['password'] = password_hash($userData['password'], PASSWORD_DEFAULT);

        $db = Db::getConnection();

        $query = "INSERT INTO " . FULL_TABLE_NAME_USERS . " VALUES (NULL, ";

        foreach ($userData as $column=>$value) {
            $query .= "'$value', ";
        }

        //need to cut last coma
        $query = substr($query, 0, strrpos($query, ','));
        $query .= ");";

        $db->query($query);
    }

    public static function checkForUniqueness($data){

        $db = Db::getConnection();

        return $db->query("SELECT userId FROM " . FULL_TABLE_NAME_USERS . " WHERE login='$data' OR email='$data'")->fetch()[0];

    }

}
