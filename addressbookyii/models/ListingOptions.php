<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class ListingOptions extends Model
{
    public $searchField = '';
    public $searchFieldUsers = '';
    public $searchValue = '';
    public $sortField = '';
    public $sortFieldUsers = '';
    public $sortOrder = '';
    public $viewAll = '';


    public static function getContactsFieldsList()
    {
        return [
            'contactId' => 'None',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'email' => 'Email',
            'mobilePhone' => 'Mobile Phone',
            'homePhone' => 'Home Phone',
            'workPhone' => 'Work Phone',
            'city' => 'City',
            'state' => 'State',
            'zip' => 'Zip Code',
        ];
    }

    public static function getUsersFieldsList()
    {
        return [
            'userId' => 'User ID',
            'username' => 'Username',
            'email' => 'Email',
            //'role' => 'Role',
            'statusId' => 'Status',
        ];
    }

    public static function getSortOrderList()
    {
        return [
            'ascending' => 'Ascending',
            'descending' => 'Descending',
        ];

    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['searchField', 'sortField'], 'match', 'pattern' => '/(contactId|firstName|lastName|email|mobilePhone|homePhone|workPhone|city|state|zip)/'],

            [['searchFieldUsers', 'sortFieldUsers'], 'match', 'pattern' => '/(userId|username|email|role|status)/'],
            //will add needed symbols
            ['searchValue', 'match', 'pattern' => '/^[a-zA-Z0-9 \-\@\.]+$/'],

            ['sortOrder', 'match', 'pattern' => '/(ascending|descending)/'],

            ['viewAll', 'integer'],

        ];
    }

}
